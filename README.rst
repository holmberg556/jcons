
JCons - a build tool
====================

JCons_ is a build tool. Its main inspiration is Cons, a Perl-based
"make-replacement" developed in the late 1990s. Variants of JCons_
have been implemented in several programming languages (Perl, Ruby,
C++/Ruby, Python). The version in this archive is the Python version.

JCons_ uses the the same build engine as JCmds_.
With the command line option ``jcons --jcmds`` it even works like JCmds_.

Documentation
-------------

A `JCons Manual`_ is available.
It is generated from reStructuredText_ format source files.

Testing
-------

::

  git clone https://bitbucket.org/holmberg556/cmdtest.git
  git clone https://bitbucket.org/holmberg556/jcons.git
  cd jcons/python
  ../../cmdtest/bin/cmdtest.rb CMDTEST_jcons.rb


Installation
------------

No installation is needed to use JCons_. The file ``jcons.py`` can
be executed directly from where it is checked out or unpacked.
Use the following command::

  git clone https://bitbucket.org/holmberg556/jcons.git
  $PWD/jcons/python/jcons.py [...]


License
-------

JCons_ is released under the GNU General Public License version 3.
For details see the file ``COPYING.txt`` in the same directory as this file.

Author
------

JCons_ was created by Johan Holmberg <holmberg556 at gmail dot com>.


.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _JCons:            https://bitbucket.org/holmberg556/jcons
.. _JCmds:            https://bitbucket.org/holmberg556/jcmds

.. _`JCons Manual`:   https://holmberg556.bitbucket.io/jcons/doc/

#!/usr/bin/env python3
#----------------------------------------------------------------------
# jcons.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""jcons main module, with command line parsing"""

import argparse
import os
import re
import signal
import sys

import jconslib
import jconslib.script
from jconslib.commands import read_commands, change_conscript_dir

#----------------------------------------------------------------------

def _extract_overrides(args):
    remaining_args = []
    overrides = []
    for arg in args:
        m = re.match(r'([_A-Z][_A-Z0-9]*)=(.*)$', arg)
        if m:
            overrides.append(m.groups())
        else:
            remaining_args.append(arg)
    return overrides, remaining_args

#----------------------------------------------------------------------

def _parse_arguments():
    parser = argparse.ArgumentParser('jcons')
    add = parser.add_argument

    add("--verbose", action="store_true",
        help="be more verbose")
    add("-v", "--version", action="store_true",
        help="show version")
    add("-q", "--quiet", action="store_true",
        help="be more quiet")
    add("-j", "--parallel", metavar='N', type=int, default=1,
        help="build in parallel")
    add("-k", "--keep-going", action="store_true",
        help="continue after errors")
    add("-n", "--dry-run", action="store_true",
        help="just print commands")
    add("-B", "--always-make", action="store_true",
        help="always build targets")
    add("--cache-dir", action="store",
        help="name of cache directory")
    add("--cache-force", action="store",
        help="copy existing files into cache")
    add("-r", "--remove", action="store_true",
        help="remove targets")
    add("-p", "--list-targets", action="store_true",
        help="list known targets")
    add("--accept-existing-target", action="store_true",
        help="make updating an existing target a nop")
    add("--dont-trust-mtime", action="store_false",
        dest='trust_mtime',
        help="always consult files for content digest")
    add("--case-sensitive", action="store_true",
        help="force case sensitive file lookup")
    add("--case-insensitive", action="store_true",
        help="force case insensitive file lookup")
    add("--states", action="store_true",
        help="show state transition log")
    add("arg", nargs="*",
        help="target or NAME=VALUE")

    add("-f", "--file", dest='construct_files', action="append",
        help="name of construct.py / *.cmds files")
    add("--cmds", action="store_true",
        help="default to Cons style input file")

    opts = parser.parse_args()
    overrides, args = _extract_overrides(opts.arg)

    if not opts.construct_files:
        if not opts.cmds:
            opts.construct_files = ['construct.py']

    return opts, overrides, args

#----------------------------------------------------------------------

class Main:
    def main(self):
        opts, overrides, args = _parse_arguments()
        jconslib.init(vars(opts))

        def ctrl_c_handler(dummy_signum, dummy_frame):
            print('jcons: got ctrl-c ...')
            jconslib.got_sigint()

        signal.signal(signal.SIGINT, ctrl_c_handler)
        signal.siginterrupt(signal.SIGINT, False)

        self.cons_cache_by_dir = {} # TODO: nested?!

        if not opts.construct_files:
            if "." not in self.cons_cache_by_dir:
                self.cons_cache_by_dir["."] = {}
            read_commands(sys.stdin, self.cons_cache_by_dir["."], ".")
        else:
            for fname in opts.construct_files:
                if fname.endswith('.txt') or fname.endswith('.cmds'):
                    self.read_commands_file(opts, fname)
                else:
                    jconslib.script.read_construct(fname, overrides)

        nerrors = self.update_top_files(args)
        return 1 if nerrors > 0 else 0

    def read_commands_file(self, opts, fname):
        with open(fname, "r") as f:
            if opts.verbose:
                print("+++ reading command file '", fname, "'")
            dname = os.path.dirname(fname)
            with change_conscript_dir(dname):
                if dname not in self.cons_cache_by_dir:
                    self.cons_cache_by_dir[dname] = {}
                read_commands(f, self.cons_cache_by_dir[dname], dname)


    def update_top_files(self, args):
        return jconslib.update_top_files(args)

if __name__ == '__main__':
    exit(Main().main())

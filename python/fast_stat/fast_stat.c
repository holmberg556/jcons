
#include <Python.h>
#include <sys/stat.h>

int gcd(int x, int y)
{
  int g;
  g = y;
  while (x > 0) {
    g = x;
    x = y % x;
    y = g;
  }
  return g;
}

/*----------------------------------------------------------------------*/

static PyObject * py_gcd(PyObject *self, PyObject *args)
{
  int x,y,r;
  if (!PyArg_ParseTuple(args,"ii:gcd",&x,&y)) {
    return NULL;
  }
  r = gcd(x,y);
  return Py_BuildValue("i",r);
}

/*----------------------------------------------------------------------*/

static PyObject * py_isfile(PyObject *self, PyObject *args)
{
  int err;
  int res;
  struct stat st;
  const char * path;

  if (!PyArg_ParseTuple(args,"s",&path)) {
    return NULL;
  }

  Py_BEGIN_ALLOW_THREADS;
  err = stat(path, &st);
  Py_END_ALLOW_THREADS;

  if (err == 0) {
    res = (S_IFREG & st.st_mode) != 0;
  }
  else {
    res = 0;
  }
  return Py_BuildValue("i",res);
}

/*----------------------------------------------------------------------*/

static PyObject * py_classify_file(PyObject *self, PyObject *args)
{
  int err;
  int res;
  struct stat st;
  const char * path;

  if (!PyArg_ParseTuple(args,"s",&path)) {
    return NULL;
  }

  Py_BEGIN_ALLOW_THREADS;
  err = stat(path, &st);
  Py_END_ALLOW_THREADS;

  if (err == 0) {
    res = ((S_IFREG & st.st_mode) != 0 ? st.st_mtime :
           (S_IFDIR & st.st_mode) != 0 ? 2 :
           0);
  }
  else {
    res = 0;
  }
  return Py_BuildValue("i",res);
}

/*----------------------------------------------------------------------*/

static PyMethodDef fast_stat_methods[] = {
  {"isfile", py_isfile, METH_VARARGS, "isfile function ..."},
  {"classify_file", py_classify_file, METH_VARARGS, "classify_file function ..."},
  {"gcd",    py_gcd,    METH_VARARGS, "gcd function ..."},
  {NULL, 0, 0, 0}  /* Sentinel */
};

static struct PyModuleDef fast_stat_module = {
  PyModuleDef_HEAD_INIT,
  "spam",
  NULL, /* doc */
  -1,
  fast_stat_methods
};

PyMODINIT_FUNC
PyInit_fast_stat(void)
{
  PyObject * m;
  m = PyModule_Create(&fast_stat_module);
  if (m == NULL) return NULL;

  return m;
}

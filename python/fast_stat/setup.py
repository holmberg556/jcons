# setup.py

from distutils.core import setup, Extension

setup(name="jcons_clib",
      version="1.0",
      py_modules = [],
      ext_modules = [
        Extension("fast_stat", ["fast_stat.c"])
        ]
      )

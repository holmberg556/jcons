#!/usr/bin/env python3
#----------------------------------------------------------------------
# jcons_cmds_wrapper.py
#----------------------------------------------------------------------
# Copyright 2015 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

from os.path import dirname, normpath, join
import os
import sys

script_path = normpath(join(os.getcwd(), __file__))
script_dir  = dirname(script_path)
jcons_dir   = script_dir
sys.path.insert(0, jcons_dir)

from jcons_cmds_bridge import MyMain

if __name__ == '__main__':
    jcons_to_test = sys.argv[1]
    sys.argv[1:2] = []
    status = MyMain(jcons_to_test).main()
    exit(status)

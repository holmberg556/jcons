
__metaclass__ = type

import collections
import copy
import re
import sys
import os

sys.path.append(os.getcwd())

try:
    import builtins
except ImportError:
    import __builtin__ as builtins

import jcons.cons
import jcons.script
import jcons.filesystem


def _mapf(f, arg):
    if isinstance(arg, list):
        return [ _mapf(f, x) for x in arg ]
    elif isinstance(arg, dict):
        return { k : _mapf(f,v) for k, v in arg.items() }
    elif isinstance(arg, str):
        return f(arg)
    else:
        return arg

def _trans(arg):
    def to_percent(x):
        x = re.sub(r'^#', r'#/', x)
        x = re.sub(r'\$SOURCES\b', r'%INPUT', x)
        x = re.sub(r'\$SOURCE\b',  r'%INPUT', x)
        x = re.sub(r'\$TARGETS\b', r'%OUTPUT', x)
        x = re.sub(r'\$TARGET\b',  r'%OUTPUT', x)
        return re.sub(r'\$([A-Z_][A-Z0-9_]*)',r'%\1', x)
    res = _mapf(to_percent, arg)
    #print ["_trans ====================", arg, res]
    return res

def _trans_dict(dict1):
    if "CPPDEFINES" in dict1:
        cppdefines = dict1["CPPDEFINES"]
        cppdefines = cppdefines if isinstance(cppdefines, list) else [cppdefines]
        dict1["CCFLAGS"] = ' '.join(cppdefines)
    return _trans(dict1)

#----------------------------------------------------------------------

class Environment1:
    def __init__(self, **kwargs):
        kwargs.pop("ENV", None)
        kwargs.pop("tools", None)
        self.cons = jcons.cons.Cons(**_trans_dict(kwargs))

    def Clone(self, **kwargs):
        new = copy.copy(self)
        new.cons = new.cons.clone(**_trans_dict(kwargs))
        return new

    def Replace(self, **kwargs):
        self.cons._apply_kwargs(**_trans(kwargs))

    def Command(self, tgts, srcs, command):
        self.cons.command(_trans(tgts), _trans(srcs), _trans(command))

    def Object(self, source, target):
        self.cons.object(_trans(target), _trans(source))

    def Program(self, prog, sources):
        self.cons.program(_trans(prog), _trans(sources))

    def StaticLibrary(self, lib, sources):
        self.cons.static_library(_trans(lib), _trans(sources))

#----------------------------------------------------------------------

def AddOption(*args, **kwargs):
    pass
def GetOption(*args, **kwargs):
    # assume "Native" question
    return True

exported_stack = [dict()]

def Export(names):
    names = names if isinstance(names, list) else [names]
    globals_ = jcons.script._globals_stack[-1]
    exported = exported_stack[-1]
    for name in names:
        exported[name] = globals_[name]
        #print ["export", name, globals_[name]]

def Import(names):
    names = names if isinstance(names, list) else [names]
    globals_ = jcons.script._globals_stack[-1]
    exported = exported_stack[-1]
    for name in names:
        #print ["import", name]
        globals_[name] = exported[name]

def SConscript(files):
    for fname in files:
        exported_stack.append( dict(exported_stack[-1]) )
        #print ["SConscript", fname]
        jcons.script.Cons.include(fname)
        exported_stack.pop()


class FakePath:
    pass

def File(path):
    x = FakePath()
    #x.path = jcons.filesystem.Rdir.curr_dir.find_file(path).path()
    x.path = jcons.cons.Cons.conscript_dir.find_file(path).path
    return x

builtins.AddOption = AddOption
builtins.GetOption = GetOption
builtins.Environment = Environment1
builtins.Export = Export
builtins.Import = Import
builtins.SConscript = SConscript
builtins.File = File

#jcons.cons.Cons.Clone = jcons.cons.Cons.clone



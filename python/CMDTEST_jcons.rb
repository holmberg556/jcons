
require 'fileutils'
require 'rbconfig'

# TODO: use array arguments to Jcons.command
# TODO: test expansion of %FOO in construct.rb

CWD = Dir.pwd
SRC_DIR = File.dirname(File.expand_path(__FILE__))

PYTHON = true


JCMDS_TO_TEST = ENV["JCMDS_TO_TEST"]

if JCMDS_TO_TEST
    JCONS_TO_TEST = File.join(SRC_DIR, "jcons_cmds_wrapper.py") + " " + '"' + JCMDS_TO_TEST + '"'
else
    JCONS_TO_TEST = ENV["JCONS_TO_TEST"] || "python3 #{CWD}/jcons.py"
end

#----------------------------------------------------------------------

class TC_jcons < Cmdtest::Testcase

    def setup
        # prepend_local_path "bin"
        ignore_file ".jcons/"
        prepend_path File.join(SRC_DIR, "scripts")
    end

    def is_windows
        false
    end

    def jcons
        JCONS_TO_TEST
    end

    #--------------------------------------------------
    # Simple error situations

    def test_00_simple_errors
        cmd "#{jcons}" do
            comment "no args and no construct.rb"
            stderr_equal /^jcons: error: failed to read construct\.(py|rb)/
            exit_nonzero
        end

        #----------
        create_file "construct.py", [
            "# empty construct file",
        ]

        cmd "#{jcons}" do
            comment "no args and a construct.rb"
            stdout_equal [
                "jcons: up-to-date: ."
            ]
        end

        #----------
        create_file "construct.py", [
            "x = y +        # with syntax error",
        ]

        cmd "#{jcons}" do
            comment "syntax error in construct.rb"
            stderr_equal /^jcons: error: syntax error reading construct\.(py|rb)/
            # and traceback probably
            exit_nonzero
        end
    end

    #--------------------------------------------------
    # -f <construct_file>

    def test_00_option_f
        create_file "construct-aaa", [
            "print('aaa')",
        ]

        create_file "construct-bbb", [
            "print('bbb')",
        ]

        cmd "#{jcons} -f construct-aaa" do
            comment "-f construct-aaa"
            stdout_equal [
                "aaa",
                "jcons: up-to-date: .",
            ]
        end

        cmd "#{jcons} -f construct-bbb" do
            comment "-f construct-bbb"
            stdout_equal [
                "bbb",
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------------------------------------
    # -h

    def test_00_option_h
        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb.txt', 'aaa.txt', 'cp %INPUT %OUTPUT')",
        ]

        cmd "#{jcons} -h" do
            comment "with -h option"
            stdout_equal /usage: jcons /
            stdout_equal /-h.*show this help message/
        end
    end

    #--------------------------------------------------
    # -q

    def test_00_option_q
        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb.txt', 'aaa.txt', 'cp %INPUT %OUTPUT')",
            "e.command('ccc.txt', 'bbb.txt', 'cp %INPUT %OUTPUT')",
        ]

        create_file "aaa.txt", [
            "this is aaa.txt",
        ]

        cmd "#{jcons} -q ccc.txt" do
            comment "with -q option"
            stdout_equal [
                /^(\[1\]|Build) bbb.txt$/,
                /^(\[2\]|Build) ccc.txt$/,
            ]
            written_files "bbb.txt", "ccc.txt"
        end

        #----------
        create_file "aaa.txt", [
            "this is aaa.txt -- changed",
        ]

        cmd "#{jcons} ccc.txt" do
            comment "without -q option"
            stdout_equal [
                "cp aaa.txt bbb.txt",
                "cp bbb.txt ccc.txt",
            ]
            written_files "bbb.txt", "ccc.txt"
        end
    end

    #--------------------------------------------------
    # -B

    def test_00_option_B
        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb.txt', 'aaa.txt', 'cp %INPUT %OUTPUT')",
            "e.command('ccc.txt', 'bbb.txt', 'cp %INPUT %OUTPUT')",
        ]

        create_file "aaa.txt", [
            "this is aaa.txt",
        ]

        cmd "#{jcons} -B ccc.txt" do
            comment "first build"
            stdout_equal [
                "cp aaa.txt bbb.txt",
                "cp bbb.txt ccc.txt",
            ]
            written_files "bbb.txt", "ccc.txt"
        end

        cmd "#{jcons} ccc.txt" do
            comment "up-to-date without -B option"
            stdout_equal [
                "jcons: already up-to-date: 'ccc.txt'"
            ]
        end

        cmd "#{jcons} -B ccc.txt" do
            comment "full rebuild with -B option"
            stdout_equal [
                "cp aaa.txt bbb.txt",
                "cp bbb.txt ccc.txt",
            ]
            written_files "bbb.txt", "ccc.txt"
        end

    end

    #--------------------------------------------------
    # -j

    def test_00_option_j
        create_file "construct.py", [
            "e = Cons()",
            "e.command('a2.txt', 'a1.txt', 'process.rb %INPUT -- %OUTPUT -- SLEEP=1')",
            "e.command('a3.txt', 'a2.txt', 'process.rb %INPUT -- %OUTPUT')",

            "e.command('b2.txt', 'b1.txt', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
            "e.command('b3.txt', 'b2.txt', 'process.rb %INPUT -- %OUTPUT')",

            "e.command('ab3.txt', ['a3.txt', 'b3.txt'], 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        create_file "a1.txt", [ "this is a1.txt" ]
        create_file "b1.txt", [ "this is b1.txt" ]

        cmd "#{jcons} ab3.txt" do
            comment "without -j2"
            stdout_equal [
                "process.rb a1.txt -- a2.txt -- SLEEP=1",
                "process.rb a2.txt -- a3.txt",
                "process.rb b1.txt -- b2.txt -- SLEEP=2",
                "process.rb b2.txt -- b3.txt",
                "process.rb a3.txt b3.txt -- ab3.txt",
            ]
            written_files "a2.txt", "a3.txt", "b2.txt", "b3.txt", "ab3.txt"
        end

        #----------
        create_file "a1.txt", [ "this is a1.txt -- changed" ]
        create_file "b1.txt", [ "this is b1.txt -- changed" ]

        cmd "#{jcons} -j2 ab3.txt" do
            comment "with -j2"
            stdout_equal [
                "process.rb a1.txt -- a2.txt -- SLEEP=1",
                "process.rb b1.txt -- b2.txt -- SLEEP=2",
                "process.rb a2.txt -- a3.txt",
                "process.rb b2.txt -- b3.txt",
                "process.rb a3.txt b3.txt -- ab3.txt",
            ]
            written_files "a2.txt", "a3.txt", "b2.txt", "b3.txt", "ab3.txt"
        end
    end

    #--------------------------------------------------
    # -k

    def test_00_option_k
        create_file "construct.py", [
            "e = Cons()",
            "e.command('a2.txt', 'a1.txt', 'process.rb %INPUT -- %OUTPUT -- EXIT=1')",
            "e.command('b2.txt', 'b1.txt', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('ab2.txt', ['a2.txt', 'b2.txt'], 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        create_file "a1.txt", [ "this is a1.txt" ]
        create_file "b1.txt", [ "this is b1.txt" ]

        cmd "#{jcons} ab2.txt" do
            comment "without -k"
            exit_nonzero
            stdout_equal [
                "process.rb a1.txt -- a2.txt -- EXIT=1",
                "jcons: error: error building 'a2.txt'",
            ]
            written_files "a2.txt"
        end

        #----------
        create_file "a1.txt", [ "this is a1.txt -- changed" ]
        create_file "b1.txt", [ "this is b1.txt -- changed" ]

        cmd "#{jcons} -k ab2.txt" do
            comment "with -k"
            exit_nonzero
            stdout_equal [
                "process.rb a1.txt -- a2.txt -- EXIT=1",
                "jcons: error: error building 'a2.txt'",
                "process.rb b1.txt -- b2.txt",
            ]
            written_files "a2.txt", "b2.txt"
        end
    end

    #--------------------------------------------------

    # regression test: when a file was updated via two "dependecy
    # paths", the second one did not propagate the 'status_ok'.
    # this caused a crash when -k was used too.

    def test_01_regress_option_k_status_ok
        create_file "aaa.txt", ["a line"]

        create_file "construct.py", [
            'e = Cons()',
            'e.command("bbb.txt", "aaa.txt", "false")',
            'e.command("ccc.txt", "bbb.txt", "cp %INPUT %OUTPUT")',
        ]

        cmd "#{jcons} -k bbb.txt ccc.txt" do
            comment "failing intermediate file"
            stdout_equal [
                "false",
                "jcons: error: error building 'bbb.txt'",
            ]
            exit_nonzero
        end
    end

    #--------------------------------------------------

    # regression test: after inlining tests of .updated
    # and setting .status_ok directly, the setting in call_state()
    # overwrites the earlier setting.
    # this caused a crash when -k was used too.

    def test_01_regress_status_ok_crash
        create_file "x0", []
        create_file "y0", []

        create_file "construct.py", [
            "e = Cons()",
            "e.command('x1', 'x0', 'false')",
            "e.command('y1', 'y0', 'process.rb y0 -- y1 -- SLEEP=2')",
            "e.command('x2', ['x1', 'y1'], 'process.rb x1 y1 -- x2')",
        ]

        cmd "#{jcons} -k x1 x2" do
            stdout_equal [
                "false",
                "jcons: error: error building 'x1'",
                "process.rb y0 -- y1 -- SLEEP=2",
            ]
            created_files "y1"
            exit_nonzero
        end
    end

    #--------------------------------------------------
    # the file "prog.h" will include two different versions of "prog12.h"
    # depending on the current Cons environment.
    # jcons should not reuse the same dependecy info in both cases.

    def test_01_command_regress_include
        create_file "src/prog1.c", [
            "#include <prog.h>",
            "int main() { return RETURN_VALUE; }",
        ]
        create_file "src/prog2.c", [
            "#include <prog.h>",
            "int main() { return RETURN_VALUE; }",
        ]
        create_file "src/prog.h", [
            "#include <prog12.h>",
        ]
        create_file "include1/prog12.h", [
            "#define RETURN_VALUE 111",
        ]
        create_file "include2/prog12.h", [
            "#define RETURN_VALUE 222",
        ]
        create_file "construct.py", [
            'e1 = Cons(CPPPATH=["src", "include1"])',
            'e2 = Cons(CPPPATH=["src", "include2"])',
            'e1.program("prog1", "src/prog1.c")',
            'e2.program("prog2", "src/prog2.c")',
        ]

        cmd "#{jcons} prog1 prog2" do
            comment "update prog1 & prog2"
            stdout_equal [
                "gcc -I src -I include1 -c src/prog1.c -o src/prog1.o",
                "gcc -o prog1 src/prog1.o",
                "gcc -I src -I include2 -c src/prog2.c -o src/prog2.o",
                "gcc -o prog2 src/prog2.o",
            ]
            written_files "prog1", "src/prog1.o", "prog2", "src/prog2.o"
        end

        # change one include directory

        create_file "include2/prog12.h", [
            "#define RETURN_VALUE 333",
        ]

        cmd "#{jcons} prog1 prog2" do
            comment "update prog2"
            stdout_equal [
                "jcons: already up-to-date: 'prog1'",
                "gcc -I src -I include2 -c src/prog2.c -o src/prog2.o",
                "gcc -o prog2 src/prog2.o",
            ]
            written_files "prog2", "src/prog2.o"
        end

        # change the other include directory

        create_file "include1/prog12.h", [
            "#define RETURN_VALUE 333",
        ]

        cmd "#{jcons} prog1 prog2" do
            comment "update prog1"
            stdout_equal [
                "gcc -I src -I include1 -c src/prog1.c -o src/prog1.o",
                "gcc -o prog1 src/prog1.o",
                "jcons: already up-to-date: 'prog2'",
            ]
            written_files "prog1", "src/prog1.o"
        end
    end

    #--------------------------------------------------
    # Simple errors.

    def test_01_command_error
        create_file "1.txt", "qwerty\n"

        create_file "construct.py", [
            'e = Cons()',
            'e.command("2.txt", "1.txt", "process.rb %INPUT -- %OUTPUT")',
            'e.command("3.txt", "2.txt", "process.rb %INPUT -- %OUTPUT -- EXIT=1")',
            'e.command("4.txt", "3.txt", "process.rb %INPUT -- %OUTPUT")',
        ]

        cmd "#{jcons}" do
            comment "one ok + one exit=1"
            stdout_equal [
                "process.rb 1.txt -- 2.txt",
                "process.rb 2.txt -- 3.txt -- EXIT=1",
                "jcons: error: error building '3.txt'",
            ]
            created_files "2.txt", "3.txt"
            exit_nonzero
        end

        cmd "#{jcons}" do
            comment "first one exit=1"
            stdout_equal [
                "process.rb 2.txt -- 3.txt -- EXIT=1",
                "jcons: error: error building '3.txt'",
            ]
            written_files "3.txt"
            exit_nonzero
        end
    end

    #--------------------------------------------------
    # Simple dependeny tests, using "command".

    def test_01_command
        create_file "construct.py", [
            'e = Cons()',
            'e.command("out.txt", "in.txt", "cp %INPUT %OUTPUT")',
        ]

        cmd "#{jcons} unknown-file" do
            comment "unknown file"
            stdout_equal [
                "jcons: error: don't know how to build 'unknown-file'",
            ]
            exit_nonzero
        end

        cmd "#{jcons} unknown-file1 unknown-file2" do
            comment "2 unknown files"
            stdout_equal [
                "jcons: error: don't know how to build 'unknown-file1'",
                "jcons: error: don't know how to build 'unknown-file2'",
            ]
            exit_nonzero
        end

        #----------
        cmd "#{jcons} out.txt" do
            comment "src doesn't exist"
            stdout_equal [
                "jcons: error: don't know how to build 'in.txt'"
            ]
            exit_nonzero
        end

        #----------
        create_file "in.txt", [
            "This is in.txt",
        ]

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt'"
            stdout_equal [
                "cp in.txt out.txt"
            ]
            created_files "out.txt"
        end

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'out.txt'"
            ]
        end

        #----------
        create_file "in.txt", [
            "This is in.txt after change",
        ]

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt' after changed 'in.txt'"
            stdout_equal [
                "cp in.txt out.txt"
            ]
            changed_files "out.txt"
        end

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'out.txt'"
            ]
        end

        #----------
        # just updating the timestamp
        touch_file "in.txt"

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt' after touched 'in.txt'"
            stdout_equal [
                "jcons: already up-to-date: 'out.txt'"
            ]
        end

        #----------
        remove_file_tree ".jcons"

        cmd "#{jcons} out.txt" do
            comment "removed .jcons/"
            stdout_equal [
                "cp in.txt out.txt"
            ]
            changed_files "out.txt"
        end

        cmd "#{jcons} out.txt" do
            comment "building 'out.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'out.txt'"
            ]
        end
    end

    #--------------------------------------------------
    # Chain of dependencies

    def test_01_command_chain
        create_file "construct.py", [
            'e = Cons()',
            'e.command("b.txt", "a.txt", "cp %INPUT %OUTPUT")',
            'e.command("c.txt", "b.txt", "cp %INPUT %OUTPUT")',
        ]

        #----------
        create_file "a.txt", [
            "This is a.txt",
        ]

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                "cp a.txt b.txt",
                "cp b.txt c.txt"
            ]
            created_files "b.txt", "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'"
            ]
        end

        #----------
        remove_file "c.txt"

        cmd "#{jcons} c.txt" do
            comment "after removed c.txt"
            stdout_equal [
                "cp b.txt c.txt"
            ]
            created_files "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'"
            ]
        end

        #----------
        remove_file "b.txt"

        cmd "#{jcons} c.txt" do
            comment "after removed b.txt"
            stdout_equal [
                "cp a.txt b.txt",
                "jcons: already up-to-date: 'c.txt'",
            ]
            created_files "b.txt"
        end

        #----------
        # just updating timestamp
        for file in ["a.txt", "b.txt", "c.txt"]
            touch_file file

            cmd "#{jcons} c.txt" do
                comment "after touching #{file}"
                stdout_equal [
                    "jcons: already up-to-date: 'c.txt'",
                ]
            end
        end

        #----------
        # changing content too
        create_file "a.txt", [
            "This is a.txt changed",
        ]

        cmd "#{jcons} c.txt" do
            comment "after changing a.txt"
            stdout_equal [
                "cp a.txt b.txt",
                "cp b.txt c.txt",
            ]
            changed_files "b.txt", "c.txt"
        end

        #----------
        create_file "b.txt", [
            "*** garbage ***",
        ]

        cmd "#{jcons} c.txt" do
            comment "after changing b.txt"
            stdout_equal [
                "cp a.txt b.txt",
                "jcons: already up-to-date: 'c.txt'",
            ]
            changed_files "b.txt"
        end

        #----------
        create_file "c.txt", [
            "*** garbage ***",
        ]

        cmd "#{jcons} c.txt" do
            comment "after changing b.txt"
            stdout_equal [
                "cp b.txt c.txt",
            ]
            changed_files "c.txt"
        end
    end

    #--------------------------------------------------
    # overlapping with 'test_02_two_output_2'

    def test_02_two_output_1
        create_file "construct.py", [
            "e = Cons()",
            "e.command(['b1.txt', 'b2.txt'], 'a.txt', 'process.rb a.txt -- b1.txt b2.txt')",
        ]

        #----------
        create_file "a.txt", [
            "This is a.txt",
        ]

        cmd "#{jcons} b1.txt" do
            comment "building 'b1.txt'"
            stdout_equal [
                "process.rb a.txt -- b1.txt b2.txt",
            ]
            created_files "b1.txt", "b2.txt"
        end

        cmd "#{jcons} b1.txt" do
            comment "'b1.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'b1.txt'",
            ]
        end

        cmd "#{jcons} b2.txt" do
            comment "'b2.txt' also up-to-date"
            stdout_equal [
                "jcons: already up-to-date: 'b2.txt'",
            ]
        end

        #----------
        remove_file "b1.txt"

        cmd "#{jcons} b2.txt" do
            comment "'b2.txt' after removing 'b1.txt'"
            stdout_equal [
                "process.rb a.txt -- b1.txt b2.txt",
            ]
            created_files "b1.txt"
            changed_files "b2.txt"
        end

        cmd "#{jcons} b2.txt" do
            comment "'b2.txt' up-to-date"
            stdout_equal [
                "jcons: already up-to-date: 'b2.txt'",
            ]
        end

        #----------
        remove_file "b2.txt"

        cmd "#{jcons} b1.txt" do
            comment "'b1.txt' after removing 'b2.txt'"
            stdout_equal [
                "process.rb a.txt -- b1.txt b2.txt",
            ]
            created_files "b2.txt"
            changed_files "b1.txt"
        end

        cmd "#{jcons} b1.txt" do
            comment "'b1.txt' up-to-date"
            stdout_equal [
                "jcons: already up-to-date: 'b1.txt'",
            ]
        end

    end

    #----------------------------------------
    # Two output files, more tests

    def test_02_two_output_2
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            'e = Cons()',
            "e.command(['b1.txt', 'b2.txt'], 'a.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        cmd "#{jcons} b1.txt" do
            comment "building 'b1.txt'"
            stdout_equal [
                'process.rb a.txt -- b1.txt b2.txt',
            ]
            created_files "b1.txt", "b2.txt"
        end

        cmd "#{jcons} b1.txt" do
            comment "building again"
            stdout_equal [
                "jcons: already up-to-date: 'b1.txt'",
            ]
        end

        cmd "#{jcons} b2.txt" do
            comment "building other target"
            stdout_equal [
                "jcons: already up-to-date: 'b2.txt'",
            ]
        end

        #----------
        create_file "b2.txt", [
            "This is b2.txt overwritten ...",
        ]

        cmd "#{jcons} b2.txt" do
            comment "building 'b2.txt' after change"
            stdout_equal [
                'process.rb a.txt -- b1.txt b2.txt',
            ]
            changed_files "b1.txt", "b2.txt"
        end

        #----------
        create_file "b1.txt", [
            "This is b1.txt overwritten ...",
        ]

        cmd "#{jcons} b2.txt" do
            comment "building 'b2.txt' after change in 'b1.txt'"
            stdout_equal [
                'process.rb a.txt -- b1.txt b2.txt',
            ]
            changed_files "b1.txt", "b2.txt"
        end
    end

    #----------------------------------------
    # Two input files

    def test_02_two_input
        create_file "b1.txt", [
            "This is b1.txt",
        ]
        create_file "b2.txt", [
            "This is b2.txt",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('c.txt', ['b1.txt', 'b2.txt'], 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                'process.rb b1.txt b2.txt -- c.txt',
            ]
            created_files "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end

        #----------
        create_file "b1.txt", [
            "This is b1.txt -- changed",
        ]

        cmd "#{jcons} c.txt" do
            comment "after changed b1.txt"
            stdout_equal [
                'process.rb b1.txt b2.txt -- c.txt',
            ]
            changed_files "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "build again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end

        #----------
        create_file "b2.txt", [
            "This is b2.txt -- changed",
        ]

        cmd "#{jcons} c.txt" do
            comment "after changed b2.txt"
            stdout_equal [
                'process.rb b1.txt b2.txt -- c.txt',
            ]
            changed_files "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "build again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end

        #----------
        remove_file "c.txt"

        cmd "#{jcons} c.txt" do
            comment "after removing c.txt"
            stdout_equal [
                'process.rb b1.txt b2.txt -- c.txt',
            ]
            created_files "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "build again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end
    end

    #----------------------------------------
    # Two output files, more tests

    def test_02_two_output_failing
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            'e = Cons()',
            "e.command(['b1.txt', 'b2.txt'], 'a.txt', 'process.rb %INPUT -- %OUTPUT -- EXIT=1')",
            "e.command('c1.txt', 'b1.txt', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c2.txt', 'b2.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        cmd "#{jcons} -k c1.txt c2.txt" do
            comment "two failing for same dependency"
            exit_nonzero
            stdout_equal [
                "process.rb a.txt -- b1.txt b2.txt -- EXIT=1",
                "jcons: error: error building 'b1.txt'",
            ]
            created_files "b1.txt", "b2.txt"
        end

    end

    #----------------------------------------

    def test_02_command_array_args
        create_file "construct.py", [
            "e = Cons()",
            "e.command(['b.txt'], ['a.txt'], 'cp %INPUT %OUTPUT')",
            "e.command('c.txt', 'b.txt', 'cp %INPUT %OUTPUT')",
        ]

        create_file "a.txt", [
            "This is a.txt",
        ]

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                "cp a.txt b.txt",
                "cp b.txt c.txt"
            ]
            created_files "b.txt", "c.txt"
        end
    end

    #----------------------------------------

    def test_02_spaces_in_filenames
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('b spaces.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c.txt', 'b spaces.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                'process.rb a.txt -- "b spaces.txt"',
                'process.rb "b spaces.txt" -- c.txt',
            ]
            created_files "b spaces.txt", "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end
    end

    #----------------------------------------

    def test_03_clone
        create_file "construct.py", [
            'e1 = Cons(A = "1a", B = "1b")',
            'e2 = e1.clone(C = "2c", B = "2b")',
            'e1.command("%A-%B-%C.txt", "src1.txt", "cp %INPUT %OUTPUT")',
            'e2.command("%A-%B-%C.txt", "src2.txt", "cp %INPUT %OUTPUT")',
        ]

        create_file "src1.txt", ["this is src1.txt"]
        create_file "src2.txt", ["this is src2.txt"]

        cmd "#{jcons} " do
            comment "building with clone too"
            stdout_equal [
                "cp src1.txt 1a-1b-.txt",
                "cp src2.txt 1a-2b-2c.txt",
            ]
            created_files "1a-1b-.txt", "1a-2b-2c.txt"
        end

        cmd "#{jcons} B=bbb" do
            comment "clone & cmdline override"
            stdout_equal [
                "cp src1.txt 1a-bbb-.txt",
                "cp src2.txt 1a-bbb-2c.txt",
            ]
            created_files "1a-bbb-.txt", "1a-bbb-2c.txt"
        end
    end

    #----------------------------------------

    def test_03_other_dir
        #----------
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('subdir/b.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c.txt', 'subdir/b.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                'process.rb a.txt -- subdir/b.txt',
                'process.rb subdir/b.txt -- c.txt',
            ]
            created_files "subdir/", "subdir/b.txt", "c.txt"
        end

        #----------
        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end
    end

    #----------------------------------------

    def test_03_exports
        create_file "construct.py", [
            "exports['E0'] = 'set in .'",
            "Cons.include('a1/conscript.py')",
            "Cons.include('b1/conscript.py')",
            "print(('. ', sorted(exports.keys())))"
        ]
        create_file "a1/conscript.py", [
            "exports['E1'] = 'set in a1'",
            "Cons.include('a2/conscript.py')",
            "print(('a1', sorted(exports.keys())))"
        ]
        create_file "a1/a2/conscript.py", [
            "exports['E2'] = 'set in a2'",
            "print(('a2', sorted(exports.keys())))"
        ]
        create_file "b1/conscript.py", [
            "exports['E3'] = 'set in b1'",
            "print(('b1', sorted(exports.keys())))"
        ]

        cmd "#{jcons}" do
            stdout_equal [
                "('a2', ['E0', 'E1', 'E2'])",
                "('a1', ['E0', 'E1'])",
                "('b1', ['E0', 'E3'])",
                "('. ', ['E0'])",
                "jcons: up-to-date: .",
            ]
        end
    end

    #----------------------------------------

    def test_31_cpppath_include_cmp_OLD
        #----------
        # setup common files

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1"',
        ]
        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2"',
        ]
        create_file "dir1/prog_include_cmp.c", [
            '#include <foo.h>',
            '#include <stdio.h>',
            'int main () {',
            '    printf("FOO_DIR = %s\n", FOO_DIR);',
            '    return 0;',
            '}',
        ]
        create_file "dir1/prog_include_quote.c", [
            '#include "foo.h"',
            '#include <stdio.h>',
            'int main () {',
            '    printf("FOO_DIR = %s\n", FOO_DIR);',
            '    return 0;',
            '}',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir2' )",
            "e.program('prog_include_quote', 'dir1/prog_include_quote.c')",
            "e.program('prog_include_cmp',   'dir1/prog_include_cmp.c')",
        ]

        #----------
        # test #include <...>

        cmd "#{jcons} prog_include_cmp" do
            comment 'with #include <...>'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include_cmp.c -o dir1/prog_include_cmp.o",
                "gcc -o prog_include_cmp dir1/prog_include_cmp.o",
            ]
            created_files "prog_include_cmp" , "dir1/prog_include_cmp.o"
        end

        cmd "./prog_include_cmp" do
            comment 'running with #include <...>'
            stdout_equal [
                'FOO_DIR = dir2',
            ]
        end

        #---
        # changing in 'dir1' has no effect

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1-changed"',
        ]

        cmd "#{jcons} prog_include_cmp" do
            comment 'with #include <...>'
            stdout_equal [
                "jcons: already up-to-date: 'prog_include_cmp'",
            ]
        end

        cmd "./prog_include_cmp" do
            comment 'running with #include <...>'
            stdout_equal [
                'FOO_DIR = dir2',
            ]
        end

        #---
        # changing in 'dir2' triggers rebuild

        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2-changed"',
        ]

        cmd "#{jcons} prog_include_cmp" do
            comment 'with #include <...>'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include_cmp.c -o dir1/prog_include_cmp.o",
                "gcc -o prog_include_cmp dir1/prog_include_cmp.o",
            ]
            changed_files "prog_include_cmp" , "dir1/prog_include_cmp.o"
        end

        cmd "./prog_include_cmp" do
            comment 'running with #include <...>'
            stdout_equal [
                'FOO_DIR = dir2-changed',
            ]
        end

        #----------
        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1"',
        ]
        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2"',
        ]

        cmd "#{jcons} prog_include_quote" do
            comment 'with #include "..."'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include_quote.c -o dir1/prog_include_quote.o",
                "gcc -o prog_include_quote dir1/prog_include_quote.o",
            ]
            created_files "prog_include_quote" , "dir1/prog_include_quote.o"
        end

        cmd "./prog_include_quote" do
            comment 'running with #include "..."'
            stdout_equal [
                'FOO_DIR = dir1',
            ]
        end
    end

    #----------------------------------------

    def test_31_cpppath_include_quote
        #----------
        # setup common files

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1"',
        ]
        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2"',
        ]
        create_file "dir1/prog_include.c", [
            '#include "foo.h"',
            '#include <stdio.h>',
            'int main () {',
            '    printf("FOO_DIR = %s\n", FOO_DIR);',
            '    return 0;',
            '}',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir2' )",
            "e.program('prog_include', 'dir1/prog_include.c')",
        ]

        #----------
        # test #include "..."

        cmd "#{jcons} prog_include" do
            comment 'with #include "..."'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include.c -o dir1/prog_include.o",
                "gcc -o prog_include dir1/prog_include.o",
            ]
            created_files "prog_include" , "dir1/prog_include.o"
        end

        cmd "./prog_include" do
            comment 'running with #include "..."'
            stdout_equal [
                'FOO_DIR = dir1',
            ]
        end

        #---
        # changing in 'dir2' has no effect

        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2-changed"',
        ]

        cmd "#{jcons} prog_include" do
            comment 'with changed "dir2/foo.h"'
            stdout_equal [
                "jcons: already up-to-date: 'prog_include'",
            ]
        end

        cmd "./prog_include" do
            comment 'running with changed "dir2/foo.h"'
            stdout_equal [
                'FOO_DIR = dir1',
            ]
        end

        #---
        # changing in 'dir1' triggers rebuild

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1-changed"',
        ]

        cmd "#{jcons} prog_include" do
            comment 'with changed "dir1/foo.h"'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include.c -o dir1/prog_include.o",
                "gcc -o prog_include dir1/prog_include.o",
            ]
            changed_files "prog_include" , "dir1/prog_include.o"
        end

        cmd "./prog_include" do
            comment 'running with changed "dir1/foo.h"'
            stdout_equal [
                'FOO_DIR = dir1-changed',
            ]
        end
    end

    #----------------------------------------

    def test_31_cpppath_include_cmp
        #----------
        # setup common files

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1"',
        ]
        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2"',
        ]
        create_file "dir1/prog_include.c", [
            '#include <foo.h>',
            '#include <stdio.h>',
            'int main () {',
            '    printf("FOO_DIR = %s\n", FOO_DIR);',
            '    return 0;',
            '}',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir2' )",
            "e.program('prog_include', 'dir1/prog_include.c')",
        ]

        #----------
        # test #include <...>

        cmd "#{jcons} prog_include" do
            comment 'with #include <...>'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include.c -o dir1/prog_include.o",
                "gcc -o prog_include dir1/prog_include.o",
            ]
            created_files "prog_include" , "dir1/prog_include.o"
        end

        cmd "./prog_include" do
            comment 'running with #include <...>'
            stdout_equal [
                'FOO_DIR = dir2',
            ]
        end

        #---
        # changing in 'dir1' has no effect

        create_file "dir1/foo.h", [
            '#define FOO_DIR "dir1-changed"',
        ]

        cmd "#{jcons} prog_include" do
            comment 'with changed "dir1/foo.h"'
            stdout_equal [
                "jcons: already up-to-date: 'prog_include'",
            ]
        end

        cmd "./prog_include" do
            comment 'running with changed "dir1/foo.h"'
            stdout_equal [
                'FOO_DIR = dir2',
            ]
        end

        #---
        # changing in 'dir2' triggers rebuild

        create_file "dir2/foo.h", [
            '#define FOO_DIR "dir2-changed"',
        ]

        cmd "#{jcons} prog_include" do
            comment 'with changed "dir2/foo.h"'
            stdout_equal [
                "gcc -I dir2 -c dir1/prog_include.c -o dir1/prog_include.o",
                "gcc -o prog_include dir1/prog_include.o",
            ]
            changed_files "prog_include" , "dir1/prog_include.o"
        end

        cmd "./prog_include" do
            comment 'running with changed "dir2/foo.h"'
            stdout_equal [
                'FOO_DIR = dir2-changed',
            ]
        end
    end

    #----------------------------------------

    def test_31_cpppath
        #----------
        create_file "dir1/prog.h", [
            '#define PROG_DIR "dir1"',
        ]
        create_file "dir2/prog.h", [
            '#define PROG_DIR "dir2"',
        ]
        create_file "prog.c", [
            '#include "prog.h"',
            '#include <stdio.h>',
            'int main () {',
            '    printf("PROG_DIR = %s\n", PROG_DIR);',
            '    return 0;',
            '}',
        ]
        #----------
        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "without CPPPATH --> error"
            if is_windows && ! is_fake_windows
                stdout_equal /^jcons: error: error building 'prog.obj'/
                stdout_equal /Cannot open include file: 'prog.h': No such file or directory/
            else
                stdout_equal /^jcons: error: error building 'prog.o'/
                stderr_equal /(error: prog.h: No such file or directory|fatal error: 'prog.h' file not found)/
            end
            exit_nonzero
        end
        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir1' )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "with CPPPATH = dir1"
            stdout_equal [
                "gcc -I dir1 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            created_files "prog" , "prog.o"
        end

        cmd "./prog" do
            comment "running with CPPPATH = dir1"
            stdout_equal [
                'PROG_DIR = dir1',
            ]
        end

        cmd "#{jcons} prog" do
            comment "CPPPATH = dir1 again --> no action"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir2' )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "with CPPPATH = dir2"
            stdout_equal [
                "gcc -I dir2 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            changed_files "prog" , "prog.o"
        end

        cmd "./prog" do
            comment "running with CPPPATH = dir2"
            stdout_equal [
                'PROG_DIR = dir2',
            ]
        end

        cmd "#{jcons} prog" do
            comment "CPPPATH = dir2 again --> no action"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = ['dir2', 'dir1'] )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "CPPPATH = dir2,dir1: --> no change"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = ['dir1', 'dir2'] )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "CPPPATH = dir1,dir2: --> change"
            stdout_equal [
                "gcc -I dir1 -I dir2 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            changed_files "prog" , "prog.o"
        end

        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = ['dir1'] )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "CPPPATH = dir1: --> no change"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end
    end

    #----------------------------------------

    def test_31_cpppath_slash
        create_file "dir1/subdir/prog.h", [
            '#define PROG_DIR "dir1/subdir"',
        ]
        create_file "dir1/prog.h", [
            '#define PROG_DIR "dir1"',
        ]
        create_file "prog.c", [
            '#include "subdir/prog.h"',
            '#include <stdio.h>',
            'int main () {',
            '    printf("PROG_DIR = %s\n", PROG_DIR);',
            '    return 0;',
            '}',
        ]

        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = 'dir1' )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} prog" do
            comment "first build"
            stdout_equal [
                "gcc -I dir1 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            created_files "prog" , "prog.o"
        end

        cmd "./prog" do
            comment "running prog"
            stdout_equal [
                'PROG_DIR = dir1/subdir',
            ]
        end

        cmd "#{jcons} prog" do
            comment "second time -- up-to-date"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "dir1/prog.h", [
            '#define PROG_DIR "dir1 changed"',
        ]

        cmd "#{jcons} prog" do
            comment "changed dir1/prog.h -- no action"
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "dir1/subdir/prog.h", [
            '#define PROG_DIR "dir1/subdir changed"',
        ]

        cmd "#{jcons} prog" do
            comment "changed dir1/subdir/prog.h -- action"
            stdout_equal [
                "gcc -I dir1 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            written_files "prog" , "prog.o"
        end

        cmd "./prog" do
            comment "running prog"
            stdout_equal [
                'PROG_DIR = dir1/subdir changed',
            ]
        end

    end

    #----------------------------------------
    # Circular dependency

    def test_31_circular

        create_file "construct.py", [
            "e = Cons()",
            "e.command('c1', 'c2', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c2', 'c3', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c3', 'c1', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c0', 'c1', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} c0" do
            comment "cirular dependenies"
            stdout_equal [
                "jcons: error: circular dependency for 'c0,c1,c2,c3'",
            ]
            exit_nonzero
        end
    end

    #----------------------------------------
    # The database should be saved even after a circular dependency
    # has been detected. Otherwise an executed action will be repeated
    # the next time too.

    def test_31_circular_and_action

        create_file "y2", ["this is y2"]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('c1', 'c2', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c2', ['c3', 'y1'], 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c3', 'c1', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c0', 'c1', 'process.rb %INPUT -- %OUTPUT')",

            "e.command('y1', 'y2', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} c0" do
            comment "action + circular first time"
            stdout_equal [
                "process.rb y2 -- y1",
                "jcons: error: circular dependency for 'c0,c1,c2,c3'",
            ]
            created_files "y1"
            exit_nonzero
        end

        cmd "#{jcons} c0" do
            comment "only circular second time"
            stdout_equal [
                "jcons: error: circular dependency for 'c0,c1,c2,c3'",
            ]
            exit_nonzero
        end
    end

    #----------------------------------------
    # Regression test for "false circular dependecy" error.
    # Unwinding the "call stacks", need to take care of semaphores
    # in effect because of the parallel algorithm of the Engine.
    # Otherwise some nodes will be left and interpreted as a sign
    # of a circular dependeny.

    def test_32_circular_by_mistake

        create_file "x1", ""
        create_file "z1", ""

        create_file "construct.py", [
            "e = Cons()",

            "e.command('x15', 'x1', 'process.rb %INPUT -- %OUTPUT -- SLEEP=5')",
            "e.command('x2', 'x15', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('x3', 'x2', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('y1', 'x2','process.rb %INPUT -- %OUTPUT')",
            "e.command('x4', ['x3', 'y1', 'z2'], 'process.rb %INPUT -- %OUTPUT')",

            "e.command('z2', 'z1', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2 EXIT=1')",
        ]


        cmd "#{jcons} -j4 x4" do
            comment "not circular"
            stdout_equal [
                "process.rb x1 -- x15 -- SLEEP=5",
                "process.rb z1 -- z2 -- SLEEP=2 EXIT=1",
                "jcons: error: error building 'z2'",
                "jcons: *** waiting for commands to finish ...",
            ]
            created_files "x15", "z2"
            exit_nonzero
        end
    end

    #----------------------------------------
    # While a "long running" command is executing, as much as possible
    # other stuff should be done.  Even if the "run queue" is
    # temporarily empty while another command is also executing.

    def test_32_really_parallel

        create_file "x",  ["this is x"]
        create_file "y2", ["this is y2"]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('a', 'x', 'process.rb %INPUT -- %OUTPUT -- SLEEP=3 EXIT=1')",
            "e.command('c', ['a', 'b'], 'process.rb %INPUT -- %OUTPUT')",
            "e.command('b', 'y1', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('y1', 'y2', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} -j3 c" do
            comment "really parallel"
            stdout_equal [
                "process.rb x -- a -- SLEEP=3 EXIT=1",
                "process.rb y2 -- y1",
                "process.rb y1 -- b",
                "jcons: error: error building 'a'",
            ]
            created_files "a", "b", "y1"
            exit_nonzero
        end

        cmd "#{jcons} -j3 c" do
            comment "second time"
            stdout_equal [
                "process.rb x -- a -- SLEEP=3 EXIT=1",
                "jcons: error: error building 'a'",
            ]
            modified_files "a"
            exit_nonzero
        end
    end

    #----------------------------------------

    def test_32_second_incdir
        #----------
        create_file "dir1/dummy.h", [
            "This is a dummy file",
        ]
        #----------
        create_file "dir2/prog.h", [
            '#define PROG "prog string"',
        ]
        #----------
        create_file "prog.c", [
            '#include <prog.h>',
            'char * name = PROG;',
            'int main() { return 0; }',
        ]
        #----------
        create_file "construct.py", [
            "e = Cons( CPPPATH = ['dir1', 'dir2'] )",
            "e.program('prog', 'prog.c')",
        ]

        # +++++++++++++++++++++++

        cmd "#{jcons} prog" do
            comment 'first build'
            stdout_equal [
                "gcc -I dir1 -I dir2 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            created_files "prog" , "prog.o"
        end

        cmd "#{jcons} prog" do
            comment 'second build'
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end

        #----------
        create_file "dir2/prog.h", [
            '#define PROG "prog string -- changed"',
        ]

        cmd "#{jcons} prog" do
            comment 'after change in dir2'
            stdout_equal [
                "gcc -I dir1 -I dir2 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            changed_files "prog" , "prog.o"
        end

    end

    #----------------------------------------
    # Test changing the command line

    def test_32_change_command
        #----------
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('b.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #----------
        cmd "#{jcons} b.txt" do
            comment "building 'b.txt' before change"
            stdout_equal [
                'process.rb a.txt -- b.txt',
            ]
            created_files "b.txt"
        end

        #----------
        create_file "construct.py", [
            "e = Cons()",
            "e.command('b.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT -- COMMENT=cmdline2')",
        ]

        cmd "#{jcons} b.txt" do
            comment "building 'b.txt' with changed cmdline"
            stdout_equal [
                'process.rb a.txt -- b.txt -- COMMENT=cmdline2',
            ]
            changed_files "b.txt"
        end
        #----------
        cmd "#{jcons} b.txt" do
            comment "building 'b.txt' with changed cmdline again"
            stdout_equal [
                "jcons: already up-to-date: 'b.txt'"
            ]
        end

        #----------
        create_file "construct.py", [
            "e = Cons()",
            "e.command('b.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT -- COMMENT=cmdline3')",
        ]

        cmd "#{jcons} b.txt" do
            comment "building 'b.txt' with yet antoher cmdline"
            stdout_equal [
                'process.rb a.txt -- b.txt -- COMMENT=cmdline3',
            ]
            changed_files "b.txt"
        end
    end

    #----------------------------------------
    # Should *not* produce "error: circular dependency"

    def test_32_parallel_circular_not
        create_file "x3", ["a line in x3"]
        create_file "y4", ["a line in y4"]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('x1', 'x2',         'process.rb %INPUT -- %OUTPUT')",
            "e.command('x2', 'x3',         'process.rb %INPUT -- %OUTPUT -- SLEEP=3')",
            "e.command('y1', 'y2',         'process.rb %INPUT -- %OUTPUT')",
            "e.command('y2', ['x2', 'y3'], 'process.rb %INPUT -- %OUTPUT')",
            "e.command('y3', 'y4',         'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} -j2 x1 y1 x2" do
            stdout_equal [
                "process.rb x3 -- x2 -- SLEEP=3",
                "process.rb y4 -- y3",
                "process.rb x2 -- x1",
                "process.rb x2 y3 -- y2",
                "process.rb y2 -- y1",
            ]
            created_files "x1", "x2", "y1", "y2", "y3"
        end
    end

    def test_32_parallel_circular_not_2
        create_file "x3", ["a line in x3"]
        create_file "y4", ["a line in y4"]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('x1', 'x2',         'process.rb %INPUT -- %OUTPUT')",
            "e.command('x2', 'x3',         'process.rb %INPUT -- %OUTPUT -- SLEEP=3 EXIT=1')",
            "e.command('y1', 'y2',         'process.rb %INPUT -- %OUTPUT')",
            "e.command('y2', ['x2', 'y3'], 'process.rb %INPUT -- %OUTPUT')",
            "e.command('y3', 'y4',         'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} -j2 -k x1 y1 x2" do
            exit_nonzero
            stdout_equal [
                "process.rb x3 -- x2 -- SLEEP=3 EXIT=1",
                "process.rb y4 -- y3",
                "jcons: error: error building 'x2'",
            ]
            created_files "x2", "y3"
        end
    end

    #----------------------------------------

    def test_33_failed_include_gen
        create_file "a.c", [
            '#include "a.h"',
            'int main() { return EXIT_CODE; }',
        ]
        create_file "a.h", [
            '#include "a2.h"',
        ]
        create_file "a2.h.in", [
            '#define EXIT_CODE 23',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = '.')",
            "e.program('a', 'a.c')",
            "e.command('a2.h', 'a2.h.in', 'failing.rb cp %INPUT %OUTPUT')",
        ]

        #---
        cmd "#{jcons} a" do
            exit_nonzero
            stdout_equal [
                "failing.rb cp a2.h.in a2.h",
                "jcons: error: error building 'a2.h'",
            ]
            created_files "a2.h"
        end

        #---
        sleep 2 if is_windows
        cmd "#{jcons} -k a" do
            comment "-k ---> still error"
            exit_nonzero
            stdout_equal [
                "failing.rb cp a2.h.in a2.h",
                "jcons: error: error building 'a2.h'",
            ]
            changed_files "a2.h"
        end

        #---
        sleep 2 if is_windows
        cmd "#{jcons} -k a" do
            comment "-k ---> still error (II)"
            exit_nonzero
            stdout_equal [
                "failing.rb cp a2.h.in a2.h",
                "jcons: error: error building 'a2.h'",
            ]
            changed_files "a2.h"
        end
    end

    #----------------------------------------
    # The actual content of an include file "in the middle" of
    # an include-chain should matter.

    def test_32_include_several_levels
        create_file "a.c", [
            '#include "a1.h"',
            'int main() { return EXIT_CODE; }',
        ]
        create_file "a1.h", [
            '#include "a2.h"',
        ]
        create_file "a2.h", [
            '#include "a3.h"',
        ]
        create_file "a3.h", [
            '#include "a4.h"',
        ]
        create_file "a4.h", [
            '#define EXIT_CODE 91',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = '.')",
            "e.program('a', 'a.c')",
        ]

        #---
        cmd "#{jcons} a" do
            stdout_equal [
                "gcc -I . -c a.c -o a.o",
                "gcc -o a a.o",
            ]
            created_files "a.o", "a"
        end

        #---
        cmd "./a" do
            exit_status 91
        end

        #---
        create_file "a2.h", [
            '#include "a3.h"',
            '/* new comment */',
        ]

        #---
        cmd "#{jcons} a" do
            if is_windows && ! is_fake_windows
                # TODO: verify this is correct
                stdout_equal [
                    "cl /nologo -I . /EHsc /c a.c /Foa.obj",
                    "a.c",
                    "link /nologo /out:a.exe a.obj",
                ]
                changed_files "a.obj", "a.exe"
            else
                stdout_equal [
                    "gcc -I . -c a.c -o a.o",
                    "jcons: already up-to-date: 'a'",
                ]
                changed_files "a.o"
            end
        end

    end

    #----------------------------------------
    # Try to trigger error occuring if the dependency tree is not handled correctly.
    # The order of travering the dependency tree should not affect the result.

    def test_32_dependency_regression_1
        create_file "a1.c", [
            '/* file: a1.c */',
            '#include "a-before.h"',
            '#include "a-indirect-before.h"',
            '#include "a-direct.h"',
            '#include "a-indirect-after.h"',
            '#include "a-after.h"',
            'int main() { return 0; }',
        ]
        create_file "a2.c", [
            '/* file: a2.c */',
            '#include "a-after.h"',
            '#include "a-indirect-after.h"',
            '#include "a-direct.h"',
            '#include "a-indirect-before.h"',
            '#include "a-before.h"',
            'int main() { return 0; }',
        ]
        create_file "a-before.h", [
            '/* file: a-before.h */',
        ]
        create_file "a-indirect-before.h", [
            '/* file: a-indirect-before.h */',
            '#include "a-direct.h"',
        ]
        create_file "a-direct.h", [
            '/* file: a-direct.h */',
            '/* empty */',
        ]
        create_file "a-indirect-after.h", [
            '/* file: a-indirect-after.h */',
            '#include "a-direct.h"',
        ]
        create_file "a-after.h", [
            '/* file: a-after.h */',
            '/* empty */',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = '.')",
            "e.program('a1', 'a1.c')",
            "e.program('a2', 'a2.c')",
        ]

        #---
        cmd "#{jcons} a1 a2" do
            comment "first build"
            stdout_equal [
                "gcc -I . -c a1.c -o a1.o",
                "gcc -o a1 a1.o",
                "gcc -I . -c a2.c -o a2.o",
                "gcc -o a2 a2.o",
            ]
            created_files "a1.o", "a1", "a2.o", "a2"
        end
        #---
        cmd "#{jcons} a1 a2" do
            comment "second build"
            stdout_equal [
                "jcons: already up-to-date: 'a1'",
                "jcons: already up-to-date: 'a2'",
            ]
        end
        #---
        cmd "#{jcons} a2 a1" do
            comment "second build"
            stdout_equal [
                "jcons: already up-to-date: 'a2'",
                "jcons: already up-to-date: 'a1'",
            ]
        end
    end

    #----------------------------------------
    # Regression test for "INTERNAL ERROR - node_calc_md5 - a2.h".
    # Jcons did not handle self-#include properly.

    def test_32_circular_include_of_self
        create_file "prog.c", [
            '#include <stdio.h>',
            '#include "prog1.h"',
            'int main() { printf("%s\n", PROG2_VALUE); return 0; }',
        ]
        create_file "prog1.h", [
            '#include "prog2.h"',
        ]
        create_file "prog2.h", [
            '#ifdef PROG2',
            '#define PROG2_VALUE (PROG2 "second time")',
            '#else',
            '#define PROG2 "first time, "',
            '#include "prog2.h"',
            '#endif',
        ]
        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'prog.c')",
        ]

        #---

        cmd "#{jcons} " do
            comment "first build"
            stdout_equal [
                "gcc -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            created_files "prog.o", "prog"
        end

        cmd "#{jcons} " do
            comment "second build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end

        prepend_local_path "."
        cmd "prog" do
            comment "running program"
            stdout_equal [
                "first time, second time",
            ]
        end
    end

    #----------------------------------------
    # The actual content of an include file "in the middle" of
    # an include-chain should matter.

    def test_32_circular_include
        create_file "a.c", [
            '#include "a1.h"',
            'int main() { return EXIT_CODE + A2_N; }',
        ]
        create_file "a1.h", [
            '#include "a2.h"',
        ]
        create_file "a2.h", [
            '#ifndef A2_H',
            '#define A2_H',
            '#include "a3.h"',
            '#include "a2_x.h"',
            '#endif',
        ]
        create_file "a2_x.h", [
            '#include "a2_y.h"',
        ]
        create_file "a2_y.h", [
            '#include "a2.h"',
            '#define A2_N 2',
        ]
        create_file "a3.h", [
            '#include "a4.h"',
        ]
        create_file "a4.h", [
            '#define EXIT_CODE 91',
        ]

        create_file "construct.py", [
            "e = Cons( CPPPATH = '.')",
            "e.program('a', 'a.c')",
        ]

        #---
        cmd "#{jcons} a" do
            comment "first build"
            stdout_equal [
                "gcc -I . -c a.c -o a.o",
                "gcc -o a a.o",
            ]
            created_files "a.o", "a"
        end

        #---
        cmd "#{jcons} a" do
            comment "no change"
            stdout_equal [
                "jcons: already up-to-date: 'a'",
            ]
        end

        #---
        create_file "a2_y.h", [
            '#include "a2.h"',
            '#define A2_N (2 + 100)',
        ]

        cmd "#{jcons} a" do
            comment "circular h-file changed"
            stdout_equal [
                "gcc -I . -c a.c -o a.o",
                "gcc -o a a.o",
            ]
            changed_files "a.o", "a"
        end

        #---
        cmd "#{jcons} a" do
            comment "no change"
            stdout_equal [
                "jcons: already up-to-date: 'a'",
            ]
        end

    end

    #----------------------------------------
    # Helper functions for tests below ...

    @@abc_count = 0

    def abc_write_unique(file)
        @@abc_count += 1
        create_file file, [
            "This is #{file}: #{@@abc_count}",
        ]
    end

    def abc_setup
        abc_write_unique("aaa")
        remove_file "bbb"
        remove_file "ccc"

        #----------
        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('ccc', 'bbb', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} ccc" do
            comment "aaa -> bbb -> ccc"
            stdout_equal [
                'process.rb aaa -- bbb',
                'process.rb bbb -- ccc',
            ]
            created_files "bbb", "ccc"
        end
    end

    def with_ENV(name, value="1")
        #ENV[name] = value
        setenv(name, value)
        yield
    ensure
        #ENV.delete(name)
        unsetenv(name)
    end

    #----------------------------------------
    # A target is removed before executing the command

    def test_05_tgt_removed
        abc_setup

        abc_write_unique("bbb")
        with_ENV("JCONS_PROCESS_SHOW_EXISTING_FILES", "aaa bbb ccc") do
            cmd "#{jcons} bbb" do
                comment "tgt removed before cmd"
                stdout_equal [
                    'process.rb aaa -- bbb',
                    'process: existing files: aaa,ccc',
                ]
                changed_files "bbb"
            end
        end
    end

    #----------------------------------------
    # A command may return non-zero exit status, even if it produces
    # a new output file. This is still an error and such output files
    # should never be trusted.
    #
    def test_05_fail_at_end
        abc_setup

        abc_write_unique("aaa")
        with_ENV("JCONS_PROCESS_FAIL_AT_END") do
            cmd "#{jcons} ccc" do
                comment "fail at end"
                stdout_equal [
                    'process.rb aaa -- bbb',
                    "jcons: error: error building 'bbb'",
                ]
                exit_nonzero
                # output file is still created, and jcons does *not* remove it
                changed_files "bbb"
            end

            # the previous 'bbb' is not trusted
            cmd "#{jcons} ccc" do
                comment "fail at end again"
                stdout_equal [
                    'process.rb aaa -- bbb',
                    "jcons: error: error building 'bbb'",
                ]
                exit_nonzero
                # output file is still created, and jcons does *not* remove it
                changed_files "bbb"
            end
        end

    end

    #----------------------------------------
    # It is an error if a command fails to produce the file it should produce,
    # even if the command exits with zero exitstatus.
    #
    def test_05_no_tgt_written
        abc_setup

        abc_write_unique("bbb")
        with_ENV("JCONS_PROCESS_NO_TGT_WRITTEN") do
            cmd "#{jcons} bbb" do
                comment "no tgt written"
                stdout_equal [
                    'process.rb aaa -- bbb',
                    "jcons: error: tgts not created 'bbb'",
                ]
                exit_nonzero
                removed_files "bbb"
            end
        end

    end

    #----------------------------------------

    def test_05_lang_awareness
        #----------
        create_file "c-main.c", [
            "extern int bbb;",
            "int main() { return bbb; }",
        ]
        create_file "cxx-main.cpp", [
            "extern \"C\" int bbb;",
            "int main() { return bbb; }",
        ]
        create_file "c-module.c", [
            "int bbb = 123;",
        ]
        create_file "cxx-module.cpp", [
            'extern "C" { int bbb = 123; }',
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.program('c-c',     'c-main.c',     'c-module.c')",
            "e.program('c-cxx',   'c-main.c',     'cxx-module.cpp')",
            "e.program('cxx-cxx', 'cxx-main.cpp', 'cxx-module.cpp')",
            "e.program('cxx-c',   'cxx-main.cpp', 'c-module.c')",
        ]

        cmd "#{jcons} -B c-c" do
            comment "C --> gcc"
            stdout_equal [
                "gcc -c c-main.c -o c-main.o",
                "gcc -c c-module.c -o c-module.o",
                "gcc -o c-c c-main.o c-module.o",
            ]
            created_files "c-c", "c-main.o", "c-module.o"
        end

        cmd "#{jcons} -B cxx-cxx" do
            comment "C++ --> g++"
            stdout_equal [
                "g++ -c cxx-main.cpp -o cxx-main.o",
                "g++ -c cxx-module.cpp -o cxx-module.o",
                "g++ -o cxx-cxx cxx-main.o cxx-module.o",
            ]
            created_files "cxx-cxx", "cxx-main.o", "cxx-module.o"
        end

        cmd "#{jcons} -B c-cxx" do
            comment "C,C++ --> gcc,g++"
            stdout_equal [
                "gcc -c c-main.c -o c-main.o",
                "g++ -c cxx-module.cpp -o cxx-module.o",
                "g++ -o c-cxx c-main.o cxx-module.o",
            ]
            created_files "c-cxx"
            changed_files "c-main.o", "cxx-module.o"
        end

        cmd "#{jcons} -B cxx-c" do
            comment "C++,C --> g++,gcc"
            stdout_equal [
                "g++ -c cxx-main.cpp -o cxx-main.o",
                "gcc -c c-module.c -o c-module.o",
                "g++ -o cxx-c cxx-main.o c-module.o",
            ]
            created_files "cxx-c"
            changed_files "cxx-main.o", "c-module.o"
        end
    end

    #----------------------------------------

    def test_05_program_with_cons
        #----------
        create_file "m1.c", [
            "extern int m2;",
            "extern int m3;",
            "extern int m4;",
            "int main() { return m2 + m3 + m4; }",
        ]
        create_file "m2.c", [
            "int m2 = 2000;",
        ]
        create_file "m3.c", [
            "int m3 = 300;",
        ]
        create_file "m4.c", [
            "int m4 = 40;",
        ]
        #------
        create_file "construct.py", [
            "e = Cons(CFLAGS = '-DFOO=1')",
            "e.program('m', 'm1.c', 'm2.c', 'm3.c', 'm4.c')",
        ]

        cmd "#{jcons} -B m" do
            comment "flat"
            stdout_equal [
                "gcc -DFOO=1 -c m1.c -o m1.o",
                "gcc -DFOO=1 -c m2.c -o m2.o",
                "gcc -DFOO=1 -c m3.c -o m3.o",
                "gcc -DFOO=1 -c m4.c -o m4.o",
                "gcc -o m m1.o m2.o m3.o m4.o",
            ]
            created_files "m", "m1.o", "m2.o", "m3.o", "m4.o"
        end

        #------
        create_file "construct.py", [
            "e  = Cons(CFLAGS = '-DFOO=1')",
            "e2 = Cons(CFLAGS = '-DFOO=2')",
            "e.program('m', 'm1.c', e2, 'm2.c', 'm3.c', 'm4.c')",
        ]

        cmd "#{jcons} -B m" do
            comment "flat with e2"
            stdout_equal [
                "gcc -DFOO=1 -c m1.c -o m1.o",
                "gcc -DFOO=2 -c m2.c -o m2.o",
                "gcc -DFOO=2 -c m3.c -o m3.o",
                "gcc -DFOO=2 -c m4.c -o m4.o",
                "gcc -o m m1.o m2.o m3.o m4.o",
            ]
            changed_files "m", "m1.o", "m2.o", "m3.o", "m4.o"
        end

        #------
        create_file "construct.py", [
            "e  = Cons(CFLAGS = '-DFOO=1')",
            "e2 = Cons(CFLAGS = '-DFOO=2')",
            "e.program('m', 'm1.c', [e2, 'm2.c', 'm3.c'], 'm4.c')",
        ]

        cmd "#{jcons} -B m" do
            comment "nested with e2"
            stdout_equal [
                "gcc -DFOO=1 -c m1.c -o m1.o",
                "gcc -DFOO=2 -c m2.c -o m2.o",
                "gcc -DFOO=2 -c m3.c -o m3.o",
                "gcc -DFOO=1 -c m4.c -o m4.o",
                "gcc -o m m1.o m2.o m3.o m4.o",
            ]
            changed_files "m", "m1.o", "m2.o", "m3.o", "m4.o"
        end

        #------
        create_file "construct.py", [
            "e  = Cons(CFLAGS = '-DFOO=1')",
            "e2 = Cons(CFLAGS = '-DFOO=2')",
            "e3 = Cons(CFLAGS = '-DFOO=3')",
            "e.program('m', [e2, 'm1.c', [e3, 'm2.c'], 'm3.c'], 'm4.c')",
        ]

        cmd "#{jcons} -B m" do
            comment "nested with e2, e3"
            stdout_equal [
                "gcc -DFOO=2 -c m1.c -o m1.o",
                "gcc -DFOO=3 -c m2.c -o m2.o",
                "gcc -DFOO=2 -c m3.c -o m3.o",
                "gcc -DFOO=1 -c m4.c -o m4.o",
                "gcc -o m m1.o m2.o m3.o m4.o",
            ]
            changed_files "m", "m1.o", "m2.o", "m3.o", "m4.o"
        end

        #------
        create_file "construct.py", [
            "e  = Cons(BUILD_TOP = 'build1')",
            "e2 = Cons(BUILD_TOP = 'build2')",
            "e3 = Cons(BUILD_TOP = 'build3')",
            "e.program('m', [e2, 'm1.c', [e3, 'm2.c'], 'm3.c'], 'm4.c')",
        ]

        cmd "#{jcons} -B build1/m" do
            comment "BUILD_TOP nested with e2, e3"
            stdout_equal [
                "gcc -c m1.c -o build2/m1.o",
                "gcc -c m2.c -o build3/m2.o",
                "gcc -c m3.c -o build2/m3.o",
                "gcc -c m4.c -o build1/m4.o",
                "gcc -o build1/m build2/m1.o build3/m2.o build2/m3.o build1/m4.o",
            ]
            created_files("build1/m", "build2/m1.o", "build3/m2.o",
                          "build2/m3.o", "build1/m4.o",
                          "build1/", "build2/", "build3/")
        end

    end

    #----------------------------------------

    def test_05_mtime_now
        #----------
        create_file "aaa", [ "123" ]

        #------
        create_file "twice.rb", [
            "File.open('bbb', 'w') {|f| f.puts 111 }",
            "system '#{jcons} bbb' or raise 'error'",
            "File.open('bbb', 'w') {|f| f.puts 222 }",
            "system '#{jcons} bbb' or raise 'error'",
        ]
        #------
        create_file "construct.py", [
            "e = Cons(CFLAGS = '-O1')",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        cmd "ruby twice.rb" do
            comment "2*jcons with change between"
            stdout_equal [
                "cp aaa bbb",
                "cp aaa bbb",
            ]
            created_files "bbb"
        end
    end

    #----------------------------------------

    def test_07_cache_dir
        create_file "construct.py", [
            "Cons.global_cache_dir('JCONS-CACHE')",
            "e = Cons( CP = 'cp', OPT = '' )",
            "e.command('ccc', 'bbb', '%CP %OPT %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', '%CP %OPT %INPUT %OUTPUT')",
        ]

        _helper_cache_dir
    end

    #----------------------------------------

    def test_07_cache_dir_local
        create_file "construct.py", [
            "e = Cons( CP = 'cp', OPT = '' )",
            "e.cache_dir('JCONS-CACHE')",
            "e.command('ccc', 'bbb', '%CP %OPT %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', '%CP %OPT %INPUT %OUTPUT')",
        ]

        _helper_cache_dir
    end

    #----------------------------------------

    def test_07_cache_dir_both
        create_file "construct.py", [
            "Cons.global_cache_dir('UNUSED-CACHE')",
            "e = Cons( CP = 'cp', OPT = '' )",
            "e.cache_dir('JCONS-CACHE')",
            "e.command('ccc', 'bbb', '%CP %OPT %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', '%CP %OPT %INPUT %OUTPUT')",
        ]

        _helper_cache_dir
    end

    #----------------------------------------

    def _helper_cache_dir
        #----------
        create_file "aaa", [ "123" ]
        ignore_files /^JCONS-CACHE/

        #------
        cmd "#{jcons} ccc" do
            comment "first time"
            stdout_equal [
                "cp aaa bbb",
                "cp bbb ccc",
            ]
            created_files "bbb", "ccc"
        end

        #------
        cmd "#{jcons} ccc" do
            comment "second time"
            stdout_equal [
                "jcons: already up-to-date: 'ccc'",
            ]
        end

        remove_file "bbb"

        #------
        cmd "#{jcons} ccc" do
            comment "after removed 'bbb'"
            stdout_equal [
                "Updated from cache: bbb",
                "jcons: already up-to-date: 'ccc'",
            ]
            created_files "bbb"
        end

        #------
        # TODO: check why sleep is needed here
        sleep 2 if is_windows

        cmd "#{jcons} ccc OPT=-f" do
            comment "changed cp command"
            stdout_equal [
                "cp -f aaa bbb",
                "cp -f bbb ccc",
            ]
            changed_files "bbb", "ccc"
        end

        #------
        cmd "#{jcons} ccc OPT=-f" do
            comment "changed cp command (II)"
            stdout_equal [
                "jcons: already up-to-date: 'ccc'",
            ]
        end

        #------
        cmd "#{jcons} ccc" do
            comment "old cp command again"
            stdout_equal [
                "Updated from cache: bbb",
                "Updated from cache: ccc",
            ]
            changed_files "bbb", "ccc"
        end
    end

    #----------------------------------------

    def test_07_cache_dir_2
        create_file "construct.py", [
            "Cons.global_cache_dir('JCONS-CACHE')",
            "e = Cons()",
            "e.command('ccc', 'bbb', 'cp %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        _helper_cache_dir_2
    end

    #----------------------------------------

    def test_07_cache_dir_2_local
        create_file "construct.py", [
            "e = Cons()",
            "e.cache_dir('JCONS-CACHE')",
            "e.command('ccc', 'bbb', 'cp %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        _helper_cache_dir_2
    end

    #----------------------------------------

    def test_07_cache_dir_2_both
        create_file "construct.py", [
            "Cons.global_cache_dir('UNUSED-CACHE')",
            "e = Cons()",
            "e.cache_dir('JCONS-CACHE')",
            "e.command('ccc', 'bbb', 'cp %INPUT %OUTPUT')",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        _helper_cache_dir_2
    end

    #----------------------------------------

    def _helper_cache_dir_2
        #----------
        create_file "aaa", [ "123" ]
        ignore_files /^JCONS-CACHE/

        #------
        cmd "#{jcons} ccc" do
            comment "first time"
            stdout_equal [
                "cp aaa bbb",
                "cp bbb ccc",
            ]
            created_files "bbb", "ccc"
        end

        #------
        cmd "#{jcons} ccc" do
            comment "second time"
            stdout_equal [
                "jcons: already up-to-date: 'ccc'",
            ]
        end

        #------
        create_file "aaa", [ "123 changed" ]

        cmd "#{jcons} ccc" do
            comment "changed 'aaa'"
            stdout_equal [
                "cp aaa bbb",
                "cp bbb ccc",
            ]
            changed_files "bbb", "ccc"
        end

        #------
        cmd "#{jcons} ccc" do
            comment "still changed 'aaa'"
            stdout_equal [
                "jcons: already up-to-date: 'ccc'",
            ]
        end

        #------
        create_file "aaa", [ "123" ]

        cmd "#{jcons} ccc" do
            comment "old 'aaa' again"
            stdout_equal [
                "Updated from cache: bbb",
                "Updated from cache: ccc",
            ]
            changed_files "bbb", "ccc"
        end
    end

    #----------------------------------------

    def test_08_cpp_end
        #----------
        create_file "aaa.h", [ "int aaa = 1;" ]
        create_file "bbb.h", [ "int bbb = 20;" ]
        create_file "ccc.h", [ "int ccc = 300;" ]

        create_file "foo.c", [
            '#include "aaa.h"',
            '#ifdef NOT_DEFINED',
            '#include "bbb.h"',
            '#endif',
            '/*',
            '#end',
            '*/',
            '#include "ccc.h"',
            'int main() { return aaa + ccc + 1000; }',
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.program('foo', 'foo.c')",
        ]

        #------
        cmd "#{jcons} foo.o" do
            comment "first build"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
            ]
            created_files "foo.o"
        end

        #------
        create_file "aaa.h", [ "int aaa = 1; /* changed */" ]

        cmd "#{jcons} foo.o" do
            comment "aaa.h changed"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
            ]
            changed_files "foo.o"
        end

        #------
        create_file "bbb.h", [ "int bbb = 20; /* changed */" ]

        cmd "#{jcons} foo.o" do
            comment "bbb.h changed"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
            ]
            changed_files "foo.o"
        end

        #------
        create_file "ccc.h", [ "int ccc = 300; /* changed */" ]

        cmd "#{jcons} foo.o" do
            comment "ccc.h after #end"
            stdout_equal [
                "jcons: already up-to-date: 'foo.o'",
            ]
        end
    end

    #--------------------
    def test_08_depends_ORDER
        create_file "aaa", [ "111" ]

        numbers = (0..20).to_a
        numbers.each do |i|
            create_file "aaa#{i}", [ "i=#{i}" ]
        end

        create_file "construct.py", [
            "e = Cons()",
            'e.command("bbb", "aaa", "cp %INPUT %OUTPUT")',
            numbers.map do |i|
                "e.depends('bbb', 'aaa#{i}')\n"
            end.join
        ]

        cmd "#{jcons} bbb" do
            comment "with 10 * depends"
            stdout_equal [
                "cp aaa bbb",
            ]
            created_files "bbb"
        end

        cmd "#{jcons} bbb" do
            comment "with 10 * depends (again)"
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
            ]
        end

        # reversing the order of 'depends' calls should have no affect

        create_file "construct.py", [
            "e = Cons()",
            'e.command("bbb", "aaa", "cp %INPUT %OUTPUT")',
            numbers.reverse.map do |i|
                "e.depends('bbb', 'aaa#{i}')\n"
            end.join
        ]

        cmd "#{jcons} bbb" do
            comment "with 10 * depends (reverse)"
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
            ]
        end
    end

    #--------------------
    def test_08_depends
        create_file "aaa", [ "111" ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('ccc', 'aaa', 'cp %INPUT %OUTPUT')",
            "e.depends('ccc', 'bbb')",
        ]

        _helper_depends
    end

    #--------------------
    def test_08_depends_first
        create_file "aaa", [ "111" ]

        create_file "construct.py", [
            "e = Cons()",
            "e.depends('ccc', 'bbb')",
            "e.command('ccc', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        _helper_depends
    end

    #--------------------
    def _helper_depends
        cmd "#{jcons} ccc" do
            comment "without depends file"
            exit_nonzero
            stdout_equal [
                "jcons: error: don't know how to build 'bbb'",
            ]
        end

        create_file "bbb", [ "222" ]

        cmd "#{jcons} ccc" do
            comment "with depends file"
            stdout_equal [
                "cp aaa ccc",
            ]
            created_files "ccc"
        end

        cmd "#{jcons} ccc" do
            comment "with depends file (II)"
            stdout_equal [
                "jcons: already up-to-date: 'ccc'",
            ]
        end

        create_file "bbb", [ "222 changed" ]

        cmd "#{jcons} ccc" do
            comment "changed depends file"
            stdout_equal [
                "cp aaa ccc",
            ]
            changed_files "ccc"
        end

        create_file "aaa", [ "111 changed" ]

        cmd "#{jcons} ccc" do
            comment "changed source file"
            stdout_equal [
                "cp aaa ccc",
            ]
            changed_files "ccc"
        end

    end

    #--------------------
    def test_08_exe_depends
        prepend_local_path "."
        create_file "foo.rb", [
            '#!/usr/bin/ruby',
            'puts "This is foo.rb";',
            'exit(system("ruby bar.rb"));',
        ]
        file_chmod(0755, "foo.rb")
        create_file "bar.rb.in", [
            '#!/usr/bin/ruby',
            'puts "This is bar.rb";',
            'exit(system("cp aaa bbb"));',
        ]
        file_chmod(0755, "bar.rb.in")
        create_file "aaa", "some content";

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'foo.rb bla bla')",
            "e.exe_depends('foo.rb', 'bar.rb')",
            "e.command('bar.rb', 'bar.rb.in', 'cp %INPUT %OUTPUT')",
        ]

        cmd "#{jcons} bbb" do
            comment "needing exe_depends"
            stdout_equal [
                "cp bar.rb.in bar.rb",
                "foo.rb bla bla",
                "This is foo.rb",
                "This is bar.rb",
            ]
            written_files "bar.rb", "bbb"
        end

        cmd "#{jcons} bbb" do
            comment "no change"
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
            ]
        end

        create_file "bar.rb.in", [
            '#!/usr/bin/ruby',
            'puts "This is bar.rb again";',
            'exit(system("cp aaa bbb"));',
        ]

        cmd "#{jcons} bbb" do
            comment "change to bar.rb.in"
            stdout_equal [
                "cp bar.rb.in bar.rb",
                "foo.rb bla bla",
                "This is foo.rb",
                "This is bar.rb again",
            ]
            written_files "bar.rb", "bbb"
        end

    end

    #--------------------
    def test_08_install
        create_file "foo.txt", "123"

        create_file "construct.py", [
            "e = Cons()",
            "e.install('doc/subdir', 'foo.txt')",
        ]

        cmd "#{jcons} ." do
            comment "initial install"
            stdout_equal /./
            created_files "doc/", "doc/subdir/", "doc/subdir/foo.txt"
        end

        cmd "#{jcons} ." do
            comment "2nd time --- no action"
            stdout_equal [
                "jcons: up-to-date: ."
            ]
        end

        create_file "foo.txt", "123456"

        cmd "#{jcons} ." do
            comment "after change"
            stdout_equal /./
            changed_files "doc/subdir/foo.txt"
        end

    end

    #--------------------
    def test_08_install_as
        create_file "foo.txt", "123"

        create_file "construct.py", [
            "e = Cons()",
            "e.install_as('doc/subdir/bar.txt', 'foo.txt')",
        ]

        cmd "#{jcons} ." do
            comment "initial install"
            stdout_equal /./
            created_files "doc/", "doc/subdir/", "doc/subdir/bar.txt"
        end

        cmd "#{jcons} ." do
            comment "2nd time --- no action"
            stdout_equal [
                "jcons: up-to-date: ."
            ]
        end

        create_file "foo.txt", "123456"

        cmd "#{jcons} ." do
            comment "after change"
            stdout_equal /./
            changed_files "doc/subdir/bar.txt"
        end

    end

    #--------------------
    def test_09_command_cpp
        return if JCMDS_TO_TEST
        create_file "bbb", [
            '#include "bbb.h"',
        ]
        create_file "bbb.h", [
            '... some content ...',
        ]
        create_file "construct.py", [
            "e = Cons()",
            "e.command('aaa', 'bbb', 'cp %INPUT %OUTPUT', scanner = 'cpp')",
        ]

        cmd "#{jcons} aaa" do
            comment "first build"
            stdout_equal [
                "cp bbb aaa",
            ]
            created_files "aaa"
        end

        cmd "#{jcons} aaa" do
            comment "second build"
            stdout_equal [
                "jcons: already up-to-date: 'aaa'",
            ]
        end

        create_file "bbb.h", [
            '... some CHANGED content ...',
        ]

        cmd "#{jcons} aaa" do
            comment "changed bbb.h"
            stdout_equal [
                "cp bbb aaa",
            ]
            changed_files "aaa"
        end

    end

    #--------------------
    def test_09_expand_command
        create_file "ab", "content";

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.command('%B%C', '%A%B', 'cp %INPUT %OUTPUT')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "cp ab bc",
            ]
            created_files "bc"
        end
    end

    #--------------------
    def test_09_expand_depends
        create_file "ab", "ab content";
        create_file "ba", "ba content";

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.command('%B%C', '%A%B', 'cp %INPUT %OUTPUT')",
            "e.depends('%B%C', '%B%A')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "cp ab bc",
            ]
            created_files "bc"
        end

        create_file "ba", "ba content 2";

        cmd "#{jcons} ." do
            comment "after change of dependency"
            stdout_equal [
                "cp ab bc",
            ]
            changed_files "bc"
        end

    end

    #--------------------
    def test_09_expand_program
        create_file "ab.c", [
            "int main() { return 0; }"
        ]

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.program('prog_%B%C', '%A%B.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
                "gcc -o prog_bc ab.o",
            ]
            created_files "prog_bc", "ab.o"
        end

        #----------
        create_file "ab.c", [
            "extern int cd_int;",
            "int main() { return 0; }",
        ]
        create_file "cd.c", [
            "int cd_int = 17;",
        ]

        create_file "construct.py", [
            "e  = Cons(A = 'a', B = 'b', C = 'c')",
            "e2 = Cons(A = 'a', B = 'b', C = 'c', D = 'd', BUILD_TOP = 'build')",
            "e.program('prog_%B%C', '%A%B.c', e2, '%C%D.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
                "gcc -c cd.c -o build/cd.o",
                "gcc -o prog_bc ab.o build/cd.o",
            ]
            written_files "build/", "prog_bc", "ab.o", "build/cd.o"
        end
    end

    #--------------------
    def test_09_expand_static_library
        create_file "ab.c", [
            "int main() { return 0; }"
        ]

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.static_library('lib_%B%C', '%A%B.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
                "ar rc lib_bc.a ab.o",
            ]
            created_files "lib_bc.a", "ab.o"
        end

        #----------
        create_file "ab.c", [
            "extern int cd_int;",
            "int main() { return 0; }",
        ]
        create_file "cd.c", [
            "int cd_int = 17;",
        ]

        create_file "construct.py", [
            "e  = Cons(A = 'a', B = 'b', C = 'c')",
            "e2 = Cons(A = 'a', B = 'b', C = 'c', D = 'd', BUILD_TOP = 'build')",
            "e.static_library('lib_%B%C', '%A%B.c', e2, '%C%D.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
                "gcc -c cd.c -o build/cd.o",
                "ar rc lib_bc.a ab.o build/cd.o",
            ]
            written_files "build/", "lib_bc.a", "ab.o", "build/cd.o"
        end
    end

    #--------------------
    def test_09_expand_objects
        create_file "ab.c", [
            "int main() { return 0; }"
        ]

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.objects('%A%B.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
            ]
            created_files "ab.o"
        end

        #----------
        create_file "ab.c", [
            "extern int cd_int;",
            "int main() { return 0; }",
        ]
        create_file "cd.c", [
            "int cd_int = 17;",
        ]

        create_file "construct.py", [
            "e  = Cons(A = 'a', B = 'b', C = 'c')",
            "e2 = Cons(A = 'a', B = 'b', C = 'c', D = 'd', BUILD_TOP = 'build')",
            "e.objects('%A%B.c', e2, '%C%D.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o ab.o",
                "gcc -c cd.c -o build/cd.o",
            ]
            written_files "build/", "ab.o", "build/cd.o"
        end
    end

    #--------------------
    def test_09_expand_object
        create_file "ab.c", [
            "int main() { return 0; }"
        ]

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.object('%B%C.o', '%A%B.c')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                "gcc -c ab.c -o bc.o",
            ]
            created_files "bc.o"
        end
    end

    #--------------------
    def test_09_expand_install
        create_file "ab.txt", "content"

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.install('%B%C', '%A%B.txt')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                /./,
            ]
            created_files "bc/", "bc/ab.txt"
        end
    end

    #--------------------
    def test_09_expand_install_as
        create_file "ab.txt", "content"

        create_file "construct.py", [
            "e = Cons(A = 'a', B = 'b', C = 'c')",
            "e.install_as('%B%C.txt', '%A%B.txt')",
        ]

        cmd "#{jcons} ." do
            stdout_equal [
                /./,
            ]
            created_files "bc.txt"
        end
    end

    #--------------------
    # Use --cache-dir option to set cache dir globally.
    # Verify that the cache is used in the right place.

    def test_30_cache_dir_option
        ignore_files "CACHE/"
        # TODO: import_file "process.rb", "process.rb"
        create_file "aaa.txt", "this is aaa.txt\n"

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb.txt', 'aaa.txt', 'process.rb %INPUT -- %OUTPUT')",
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/ccc.txt", "this is subdir/ccc.txt\n"

        create_file "subdir/conscript.py", [
            "e_subdir = Cons()",
            "e_subdir.command('ddd.txt', 'ccc.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #---

        cmd "#{jcons} --cache-dir CACHE -B ." do
            comment "create CACHE/ at top"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            created_files [
                "bbb.txt",
                "subdir/ddd.txt",
            ]
        end

        cmd "#{jcons} --cache-dir CACHE -B ." do
            comment "update from cache"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end

        # remove cache
        remove_file_tree "CACHE/"

        cmd "#{jcons} --cache-dir CACHE -B ." do
            comment "without any cache"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            changed_files [
                "bbb.txt",
                "subdir/ddd.txt",
            ]
        end

        cmd "#{jcons} --cache-dir CACHE -B ." do
            comment "update from cache (again)"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end
    end

    #--------------------
    # Use Cons.cache_dir to set cache dir globally.
    # Verify that the cache is used in the right place.

    def test_30_cache_dir_globally
        ignore_files "CACHE/"
        # import_file "process.rb", "process.rb" # TODO
        create_file "aaa.txt", "this is aaa.txt\n"

        create_file "construct.py", [
            "Cons.global_cache_dir('CACHE')",
            "e = Cons()",
            "e.command('bbb.txt', 'aaa.txt', 'process.rb %INPUT -- %OUTPUT')",
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/ccc.txt", "this is subdir/ccc.txt\n"

        create_file "subdir/conscript.py", [
            "e2 = Cons()",
            "e2.command('ddd.txt', 'ccc.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #---

        cmd "#{jcons} -B ." do
            comment "create CACHE/ at top"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            created_files [
                "bbb.txt",
                "subdir/ddd.txt",
            ]
        end

        cmd "#{jcons} -B ." do
            comment "update from cache"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end

        # remove cache
        remove_file_tree "CACHE/"

        cmd "#{jcons} -B ." do
            comment "without any cache"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            changed_files [
                "bbb.txt",
                "subdir/ddd.txt",
            ]
        end

        cmd "#{jcons} -B ." do
            comment "update from cache (again)"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end
    end

    #--------------------
    # Use Cons#cache_dir to set cache dir per ojbect.
    # Verify that the different caches are used in the right place.

    def test_30_cache_dir_per_object
        ignore_files "CACHE1/", "subdir/CACHE2/"
        # import_file "process.rb", "process.rb" # TODO
        create_file "aaa.txt", "this is aaa.txt\n"

        create_file "construct.py", [
            "e = Cons()",
            "e.cache_dir('CACHE1')",
            "e.command('bbb.txt', 'aaa.txt', 'process.rb %INPUT -- %OUTPUT')",
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/ccc.txt", "this is subdir/ccc.txt\n"

        create_file "subdir/conscript.py", [
            "e2 = Cons()",
            "e2.cache_dir('CACHE2')",
            "e2.command('ddd.txt', 'ccc.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        #---

        cmd "#{jcons} -B ." do
            comment "CACHE1/ at top, CACHE2/ in subdir"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            created_files [
                "bbb.txt",
                "subdir/ddd.txt",
            ]
        end

        cmd "#{jcons} -B ." do
            comment "update from existing caches"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end

        # remove one of the caches
        remove_file_tree "CACHE1"

        cmd "#{jcons} -B ." do
            comment "update from one existing cache"
            stdout_equal [
                "process.rb aaa.txt -- bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            else
                changed_files "bbb.txt"
            end
        end

        # remove other of the caches
        remove_file_tree "subdir/CACHE2"

        cmd "#{jcons} -B ." do
            comment "update from other existing cache"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "process.rb subdir/ccc.txt -- subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            else
                changed_files "subdir/ddd.txt"
            end
        end

        cmd "#{jcons} -B ." do
            comment "update from existing caches (again)"
            stdout_equal [
                "Updated from cache: bbb.txt",
                "Updated from cache: subdir/ddd.txt",
            ]
            if is_windows
                # copy instead of link
                changed_files "bbb.txt", "subdir/ddd.txt"
            end
        end
    end

    #--------------------
    # EXE_DIR

    def test_99_EXE_DIR
        create_file "construct.py", [
            "e = Cons( EXE_DIR = '#/build/bin' )",
            "e.program('prog_top', 'prog_top.c')",
            "exports['E'] = e",
            "Cons.include('dir1/conscript.py')",
            ]

        create_file "dir1/conscript.py", [
            "e = exports['E']",
            "e.program('prog_dir1', 'prog_dir1.c')",
        ]

        create_file "prog_top.c", ["int main() { return 11; }"]
        create_file "dir1/prog_dir1.c", ["int main() { return 22; }"]

        cmd "#{jcons} " do
            stdout_equal [
                "gcc -c dir1/prog_dir1.c -o dir1/prog_dir1.o",
                "gcc -o build/bin/prog_dir1 dir1/prog_dir1.o",
                "gcc -c prog_top.c -o prog_top.o",
                "gcc -o build/bin/prog_top prog_top.o",
            ]
            created_files [
                "build/", "build/bin/",
                "build/bin/prog_top", "build/bin/prog_dir1",
                "prog_top.o", "dir1/prog_dir1.o",
            ]
        end
    end

    #--------------------
    # LIB_DIR

    def test_99_LIB_DIR
        create_file "construct.py", [
            "e = Cons( LIB_DIR = '#/build/lib' )",
            "e.static_library('foo_top', 'foo_top.c')",
            "exports['E'] = e",
            "Cons.include('dir1/conscript.py')",
        ]

        create_file "dir1/conscript.py", [
            "e = exports['E']",
            "e.static_library('foo_dir1', 'foo_dir1.c')",
        ]

        create_file "foo_top.c", ["int not_main() { return 11; }"]
        create_file "dir1/foo_dir1.c", ["int not_main() { return 22; }"]

        cmd "#{jcons} " do
            stdout_equal [
                "gcc -c dir1/foo_dir1.c -o dir1/foo_dir1.o",
                "ar rc build/lib/foo_dir1.a dir1/foo_dir1.o",
                "gcc -c foo_top.c -o foo_top.o",
                "ar rc build/lib/foo_top.a foo_top.o",
            ]
            created_files [
                "build/", "build/lib/",
                "build/lib/foo_top.a", "build/lib/foo_dir1.a",
                "foo_top.o", "dir1/foo_dir1.o",
            ]
        end
    end

    #--------------------
    # BUILD_TOP

    def test_99_BUILD_TOP
        create_file "construct.py", [
            "e = Cons( BUILD_TOP = '#/build1' )",
            "exports['E'] = e",
            "Cons.include('dir1/conscript')",
        ]

        create_file "dir1/conscript", [
            "e1 = exports['E']",
            "e1.objects('foo.c')",
        ]

        create_file "dir1/foo.c", ["int iii = 123;"]

        cmd "#{jcons} " do
            comment "object file in 'build1'"
            stdout_equal [
                "gcc -c dir1/foo.c -o build1/dir1/foo.o"
            ]
            created_files "build1/", "build1/dir1/", "build1/dir1/foo.o"
        end

        create_file "construct.py", [
            "e = Cons(BUILD_TOP = '#/build2/subdir')",
            "exports['E'] = e",
            "Cons.include('dir1/conscript')",
        ]

        cmd "#{jcons} " do
            comment "object file in 'build2/subdir'"
            stdout_equal [
                "gcc -c dir1/foo.c -o build2/subdir/dir1/foo.o"
            ]
            created_files "build2/", "build2/subdir/", "build2/subdir/dir1/", "build2/subdir/dir1/foo.o"
        end
    end

    #--------------------
    # BUILD_SUBDIR

    def test_99_BUILD_SUBDIR
        create_file "construct.py", [
            "e = Cons( BUILD_SUBDIR = 'dir1' )",
            "e.objects('foo.c', 'lib/bar.c')",
        ]

        create_file "foo.c", ["int iii = 123;"]
        create_file "lib/bar.c", ["int jjj = 456;"]

        cmd "#{jcons} " do
            comment "subdir at top level"
            stdout_equal [
                "gcc -c foo.c -o dir1/foo.o",
                "gcc -c lib/bar.c -o lib/dir1/bar.o",
            ]
            created_files "dir1/", "dir1/foo.o", "lib/dir1/", "lib/dir1/bar.o"
        end

        create_file "construct.py", [
            "e = Cons(BUILD_SUBDIR = 'dir1' )",
            "exports['E'] = e",
            "Cons.include('subdir1/conscript.py')",
        ]

        create_file "subdir1/conscript.py", [
            "e1 = exports['E']",
            "e1.objects('bar2.c', 'lib2/foo2.c')",
        ]

        create_file "subdir1/bar2.c", ["int jjj = 456;"]
        create_file "subdir1/lib2/foo2.c", ["int kkk = 789;"]

        cmd "#{jcons} " do
            comment "subdir at lower level"
            stdout_equal [
                "gcc -c subdir1/bar2.c -o subdir1/dir1/bar2.o",
                "gcc -c subdir1/lib2/foo2.c -o subdir1/lib2/dir1/foo2.o",
            ]
            created_files("subdir1/dir1/", "subdir1/dir1/bar2.o",
                           "subdir1/lib2/dir1/", "subdir1/lib2/dir1/foo2.o")
        end
    end

    #--------------------
    # BUILD_SUFFIX

    def test_99_BUILD_SUFFIX
        create_file "construct.py", [
            "e = Cons( BUILD_SUFFIX = 'suf1' )",
            "e.objects('foo.c')",
        ]

        create_file "foo.c", ["int iii = 123;"]

        cmd "#{jcons} " do
            comment "suffix at top level"
            stdout_equal [
                "gcc -c foo.c -o foo-suf1.o"
            ]
            created_files "foo-suf1.o"
        end

        create_file "construct.py", [
            "e = Cons( BUILD_SUFFIX = 'suf1' )",
            "exports['E'] = e",
            "Cons.include('subdir1/conscript.py')",
        ]

        create_file "subdir1/conscript.py", [
            "e1 = exports['E']",
            "e1.objects('bar.c')",
        ]

        create_file "subdir1/bar.c", ["int jjj = 456;"]

        cmd "#{jcons} " do
            comment "suffix at lower level"
            stdout_equal [
                "gcc -c subdir1/bar.c -o subdir1/bar-suf1.o"
            ]
            created_files "subdir1/bar-suf1.o"
        end
    end

    #--------------------
    # test "scoping" between construct.rb and conscript.rb

    def test_22_conscript_scope
        return if PYTHON        # TODO
        create_file "construct.rb", [
            "CONST1 = 'CONST1 in construct.rb'",
            "var = 'var in construct.rb'",
            "def fun ; 'fun in construct.rb' ; end",
            "Cons.include 'subdir/conscript.rb'",
            "puts var",
            "puts fun",
            "puts CONST2",
        ]

        create_file "subdir/conscript.rb", [
            "puts CONST1",
            "CONST2 = 'CONST2 in conscript.rb'",
            "var = 'var in conscript.rb'",
            "def fun ; 'fun in conscript.rb' ; end",
        ]

        cmd2 "jcons ." do
            stdout_equal [
                "CONST1 in construct.rb",
                "var in construct.rb",
                "fun in conscript.rb",
                "CONST2 in conscript.rb",
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------
    # make sure Cons#program gets file paths right in subdirectory

    def test_22_conscript_program
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons( CPPPATH = ['inc'] )",
            "e.program('prog', 'prog.c')",
        ]

        create_file "subdir/inc/prog.h", [
            "#define FOO 135",
        ]
        create_file "subdir/prog.c", [
            "#include <prog.h>",
            "int main() { return FOO; }",
        ]

        cmd "#{jcons} ." do
            comment "build of subdir/prog"
            stdout_equal [
                "gcc -I subdir/inc -c subdir/prog.c -o subdir/prog.o",
                "gcc -o subdir/prog subdir/prog.o",
            ]
            created_files "subdir/prog", "subdir/prog.o"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end

        cmd "subdir/prog" do
            comment "run built program"
            exit_status 135
        end

        # change header file
        create_file "subdir/inc/prog.h", [
            "#define FOO 246",
        ]

        cmd "#{jcons} ." do
            comment "rebuild after prog.h change"
            stdout_equal [
                "gcc -I subdir/inc -c subdir/prog.c -o subdir/prog.o",
                "gcc -o subdir/prog subdir/prog.o",
            ]
            changed_files "subdir/prog", "subdir/prog.o"
        end

        cmd "subdir/prog" do
            comment "run built program"
            exit_status 246
        end

        # CPPPATH with "#"
        create_file "subdir/conscript.py", [
            "e = Cons( CPPPATH = ['#/inc'] )",
            "e.program('prog', 'prog.c')",
        ]

        create_file "inc/prog.h", [
            "#define FOO 99",
        ]

        cmd "#{jcons} ." do
            comment "rebuild after CPPPATH change"
            stdout_equal [
                "gcc -I inc -c subdir/prog.c -o subdir/prog.o",
                "gcc -o subdir/prog subdir/prog.o",
            ]
            changed_files "subdir/prog", "subdir/prog.o"
        end

        cmd "subdir/prog" do
            comment "run built program"
            exit_status 99
        end

    end

    #--------------------
    # make sure Cons#static_library gets file paths right in subdirectory

    def test_22_conscript_static_library
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.static_library('libfoo.a', 'foo.c')",
        ]

        create_file "subdir/foo.c", [
            "int foo() { return 0; }",
        ]

        cmd "#{jcons} ." do
            comment "build of subdir/libfoo.a"
            stdout_equal [
                "gcc -c subdir/foo.c -o subdir/foo.o",
                "ar rc subdir/libfoo.a subdir/foo.o",
            ]
            created_files "subdir/libfoo.a", "subdir/foo.o"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------
    # make sure Cons#command gets file paths right in subdirectory

    def test_22_conscript_command
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.command(['out1', 'out2'], ['in1', 'in2'], 'process.rb %INPUT -- %OUTPUT')",
        ]

        create_file "subdir/in1", [ "this is in1" ]
        create_file "subdir/in2", [ "this is in2" ]

        cmd "#{jcons} ." do
            comment "command in subdir"
            stdout_equal [
                "process.rb subdir/in1 subdir/in2 -- subdir/out1 subdir/out2",
            ]
            created_files "subdir/out1", "subdir/out2"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------
    # make sure Cons#command gets file paths right in subdirectory

    def test_22_conscript_depends
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.command(['out1', 'out2'], ['in1', 'in2'], 'process.rb %INPUT -- %OUTPUT')",
            "e.depends('out2', 'not-in')",
        ]

        create_file "subdir/in1", [ "this is in1" ]
        create_file "subdir/in2", [ "this is in2" ]
        create_file "subdir/not-in", [ "this is not-in" ]

        cmd "#{jcons} ." do
            comment "command in subdir"
            stdout_equal [
                "process.rb subdir/in1 subdir/in2 -- subdir/out1 subdir/out2",
            ]
            created_files "subdir/out1", "subdir/out2"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end

        create_file "subdir/not-in", [ "this is not-in CHANGED" ]

        cmd "#{jcons} ." do
            comment "depends in subdir"
            stdout_equal [
                "process.rb subdir/in1 subdir/in2 -- subdir/out1 subdir/out2",
            ]
            changed_files "subdir/out1", "subdir/out2"
        end

    end

    #--------------------
    # make sure Cons#objects gets file paths right in subdirectory

    def test_21_conscript_objects
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.objects('foo.c')",
        ]

        create_file "subdir/foo.c", [
            "int foo() { return 0; }",
        ]

        cmd "#{jcons} ." do
            comment "objects in subdir"
            stdout_equal [
                "gcc -c subdir/foo.c -o subdir/foo.o",
            ]
            created_files "subdir/foo.o"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------
    # make sure Cons#object gets file paths right in subdirectory

    def test_21_conscript_object
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.object('foo-renamed.o', 'foo.c')",
        ]

        create_file "subdir/foo.c", [
            "int foo() { return 0; }",
        ]

        cmd "#{jcons} ." do
            comment "objects in subdir"
            stdout_equal [
                "gcc -c subdir/foo.c -o subdir/foo-renamed.o",
            ]
            created_files "subdir/foo-renamed.o"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end
    end

    #--------------------
    # make sure the error "expected directory" is handled correctly,
    # especially with/without -k option

    def test_21_not_a_dir_error
        create_file "not-a-dir", []
        create_file "aaa", []

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
            "e.command('not-a-dir/ccc', 'aaa', 'cp %INPUT %OUTPUT')",
            "e.command('ddd', 'bbb', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
        ]

        cmd "#{jcons} -B -j2 bbb not-a-dir/ccc ddd" do
            comment "printing 'waiting for ...'"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "jcons: error: expected directory 'not-a-dir'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "bbb"
        end

        cmd "#{jcons} -j2 bbb not-a-dir/ccc ddd" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "jcons: error: expected directory 'not-a-dir'",
            ]
        end

        #----------

        cmd "#{jcons} -B -k -j2 bbb not-a-dir/ccc ddd" do
            comment "-k ---> not 'waiting ...' + last command done"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "jcons: error: expected directory 'not-a-dir'",
                "process.rb bbb -- ddd -- SLEEP=2",
            ]
            written_files "bbb", "ddd"
        end

        cmd "#{jcons} -k -j2 bbb not-a-dir/ccc ddd" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "jcons: error: expected directory 'not-a-dir'",
                "jcons: already up-to-date: 'ddd'",
            ]
        end

    end

    #--------------------
    # make sure the error "don't know how to build ..." is handled correctly,
    # especially with/without -k option

    def test_21_dont_know_error
        create_file "not-a-dir", []
        create_file "aaa", []
        create_file "eee", []

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
            "e.command('ddd', 'non-existing', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('fff', 'eee', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
        ]

        cmd "#{jcons} -B -j2 bbb ddd fff" do
            comment "printing 'waiting for ...'"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "jcons: error: don't know how to build 'non-existing'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "bbb"
        end

        cmd "#{jcons} -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "jcons: error: don't know how to build 'non-existing'",
            ]
        end

        #----------

        cmd "#{jcons} -B -k -j2 bbb ddd fff" do
            comment "-k ---> not 'waiting ...' + last command done"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "jcons: error: don't know how to build 'non-existing'",
                "process.rb eee -- fff -- SLEEP=2",
            ]
            written_files "bbb", "fff"
        end

        cmd "#{jcons} -k -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "jcons: error: don't know how to build 'non-existing'",
                "jcons: already up-to-date: 'fff'",
            ]
        end

    end

    #--------------------
    # make sure the error "error building ..." is handled correctly,
    # especially with/without -k option

    def test_21_error_building
        create_file "aaa", []
        create_file "ccc", []
        create_file "eee", []

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
            "e.command('ddd', 'ccc', 'process.rb %INPUT -- %OUTPUT -- SLEEP=1 EXIT=1')",
            "e.command('fff', 'eee', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
        ]

        cmd "#{jcons} -B -j2 bbb ddd fff" do
            comment "printing 'waiting for ...'"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "process.rb ccc -- ddd -- SLEEP=1 EXIT=1",
                "jcons: error: error building 'ddd'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "bbb", "ddd"
        end

        cmd "#{jcons} -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "process.rb ccc -- ddd -- SLEEP=1 EXIT=1",
                "process.rb eee -- fff -- SLEEP=2",
                "jcons: error: error building 'ddd'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "ddd", "fff"
        end

        #----------

        cmd "#{jcons} -B -k -j2 bbb ddd fff" do
            comment "-k ---> not 'waiting ...' + last command done"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "process.rb ccc -- ddd -- SLEEP=1 EXIT=1",
                "jcons: error: error building 'ddd'",
                "process.rb eee -- fff -- SLEEP=2",
            ]
            written_files "bbb", "ddd", "fff"
        end

        cmd "#{jcons} -k -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "process.rb ccc -- ddd -- SLEEP=1 EXIT=1",
                "jcons: already up-to-date: 'fff'",
                "jcons: error: error building 'ddd'",
            ]
            written_files "ddd"
        end

    end

    #--------------------
    # make sure the error "tgts not created ..." is handled correctly,
    # especially with/without -k option

    def test_21_tgts_not_created
        create_file "aaa", []
        create_file "ccc", []
        create_file "eee", []

        create_file "construct.py", [
            "e = Cons()",
            "e.command('bbb', 'aaa', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
            "e.command('ddd', 'ccc', 'process.rb --no-tgt %INPUT -- %OUTPUT -- SLEEP=1')",
            "e.command('fff', 'eee', 'process.rb %INPUT -- %OUTPUT -- SLEEP=2')",
        ]

        cmd "#{jcons} -B -j2 bbb ddd fff" do
            comment "printing 'waiting for ...'"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "process.rb --no-tgt ccc -- ddd -- SLEEP=1",
                "jcons: error: tgts not created 'ddd'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "bbb"
        end

        cmd "#{jcons} -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "process.rb --no-tgt ccc -- ddd -- SLEEP=1",
                "process.rb eee -- fff -- SLEEP=2",
                "jcons: error: tgts not created 'ddd'",
                "jcons: *** waiting for commands to finish ...",
            ]
            written_files "fff"
        end

        #----------

        cmd "#{jcons} -B -k -j2 bbb ddd fff" do
            comment "-k ---> not 'waiting ...' + last command done"
            exit_nonzero
            stdout_equal [
                "process.rb aaa -- bbb -- SLEEP=2",
                "process.rb --no-tgt ccc -- ddd -- SLEEP=1",
                "jcons: error: tgts not created 'ddd'",
                "process.rb eee -- fff -- SLEEP=2",
            ]
            written_files "bbb", "fff"
        end

        cmd "#{jcons} -k -j2 bbb ddd fff" do
            comment "second update"
            exit_nonzero
            stdout_equal [
                "jcons: already up-to-date: 'bbb'",
                "process.rb --no-tgt ccc -- ddd -- SLEEP=1",
                "jcons: already up-to-date: 'fff'",
                "jcons: error: tgts not created 'ddd'",
            ]
        end

    end

    #--------------------

    def test_21_updated_from_cache
        create_file "construct.py", [
            "Cons.global_cache_dir('CACHE')",
            "e = Cons()",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
        ]

        ignore_files /^CACHE/

        for i in [0, 1, 2, 8000, 9000, 1_000_000]
            str = ""
            i.times {|i2| str << (i%256) }
            create_file "aaa", str
            cmd "#{jcons} bbb" do
                comment "cp ... #{i} ..."
                stdout_equal [
                    "cp aaa bbb",
                ]
                written_files "bbb"
            end

            remove_file "bbb"

            cmd "#{jcons} bbb" do
                comment "updated from cache ..."
                stdout_equal [
                    "Updated from cache: bbb",
                ]
                written_files "bbb"
            end

            cmd "cmp aaa bbb" do
            end
        end

    end

    #--------------------
    # make sure path beginning with "#" are interpreted

    def test_11_hashpath
        create_file "construct.py", [
            "Cons.include('subdir/conscript.py')",
        ]

        dir_mkdir "subdir"
        create_file "subdir/conscript.py", [
            "e = Cons()",
            "e.command(['#/out1', '#/dir/out2'], 'in', 'process.rb %INPUT -- %OUTPUT')",
        ]

        create_file "subdir/in", [ "this is in" ]

        cmd "#{jcons} ." do
            comment "with '#' in path"
            stdout_equal [
                "process.rb subdir/in -- out1 dir/out2",
            ]
            created_files "out1", "dir/", "dir/out2"
        end

        cmd "#{jcons} ." do
            comment "2nd build"
            stdout_equal [
                "jcons: up-to-date: .",
            ]
        end
    end

    #----------------------------------------------------------------------

    def test_11_static_library
        create_file "foo.c", [
            "int foo() { return 0; }",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.static_library('libfoo.a', 'foo.c')",
        ]

        cmd "#{jcons} -B ." do
            comment "libfoo.a --> libfoo.a"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "ar rc libfoo.a foo.o",
            ]
            written_files "libfoo.a", "foo.o"
        end

        create_file "construct.py", [
            "e = Cons()",
            "e.static_library('foo.a', 'foo.c')",
        ]

        cmd "#{jcons} -B ." do
            comment "foo.a --> foo.a"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "ar rc foo.a foo.o",
            ]
            written_files "foo.a", "foo.o"
        end

        create_file "construct.py", [
            "e = Cons()",
            "e.static_library('foo', 'foo.c')",
        ]

        cmd "#{jcons} -B ." do
            comment "foo --> foo.a"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "ar rc foo.a foo.o",
            ]
            written_files "foo.a", "foo.o"
        end

    end

    #----------------------------------------------------------------------

    def test_11_program
        create_file "prog.c", [
            "int main() { return 0; }",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} -B ." do
            comment "prog.exe --> prog.exe"
            stdout_equal [
                "gcc -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            written_files "prog", "prog.o"
        end

        # TODO: compare with previous revision

        cmd "#{jcons} -B ." do
            comment "prog --> prog.exe"
            stdout_equal [
                "gcc -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            written_files "prog", "prog.o"
        end

    end

    #----------------------------------------------------------------------

    def test_20_clone__constructor_bug
        create_file "prog_foo.cpp", [
            "int main() { return 0; }",
        ]
        create_file "prog_bar.cpp", [
            "int main() { return 0; }",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog_foo', ['prog_foo.cpp'])",
            "e2 = e.clone()",
            "e2.program('prog_bar', ['prog_bar.cpp'])",
        ]

        cmd "#{jcons} prog_foo prog_bar" do
            comment "use clone after using original"
            stdout_equal [
                "g++ -c prog_foo.cpp -o prog_foo.o",
                "g++ -o prog_foo prog_foo.o",
                "g++ -c prog_bar.cpp -o prog_bar.o",
                "g++ -o prog_bar prog_bar.o",
            ]
            written_files("prog_foo", "prog_foo.o",
                           "prog_bar", "prog_bar.o")
        end

    end

    #----------------------------------------------------------------------

    def test_20_cons_include
        if PYTHON
            create_file "construct.py", [
                "print('--> construct.py')",
                "Cons.include('construct-sub1.py')",
                "print('<-- construct.py')",
                "exit(0)",
            ]
            create_file "construct-sub1.py", [
                "print('    --> construct-sub1.py')",
                "Cons.include('construct-sub2.py')",
                "print('    <-- construct-sub1.py')",
            ]
            create_file "construct-sub2.py", [
                "print('        --> construct-sub2.py')",
                "print('        <-- construct-sub2.py')",
            ]

            cmd "#{jcons}" do
                comment "two levels of include"
                stdout_equal [
                    "--> construct.py",
                    "    --> construct-sub1.py",
                    "        --> construct-sub2.py",
                    "        <-- construct-sub2.py",
                    "    <-- construct-sub1.py",
                    "<-- construct.py",
                ]
            end
        else
            create_file "construct.rb", [
                "puts '--> construct.rb'",
                "Cons.include 'construct-sub1.rb'",
                "puts '<-- construct.rb'",
                "STDOUT.flush",
                "exit! 0",
            ]
            create_file "construct-sub1.rb", [
                "puts '    --> construct-sub1.rb'",
                "Cons.include 'construct-sub2.rb'",
                "puts '    <-- construct-sub1.rb'",
            ]
            create_file "construct-sub2.rb", [
                "puts '        --> construct-sub2.rb'",
                "puts '        <-- construct-sub2.rb'",
            ]

            cmd "#{jcons}" do
                comment "two levels of include"
                stdout_equal [
                    "--> construct.rb",
                    "    --> construct-sub1.rb",
                    "        --> construct-sub2.rb",
                    "        <-- construct-sub2.rb",
                    "    <-- construct-sub1.rb",
                    "<-- construct.rb",
                ]
            end
        end
    end

    #----------------------------------------------------------------------

    def test_20_cons_include_subdirs
        if PYTHON
            create_file "construct.py", [
                "print('--> construct.py')",
                "Cons.include('sub/dir1/construct-sub1.py')",
                "print('<-- construct.py')",
                "exit(0)",
            ]
            create_file "sub/dir1/construct-sub1.py", [
                "print('    --> construct-sub1.py')",
                "Cons.include('../../sub/dir2/construct-sub2.py')",
                "print('    <-- construct-sub1.py')",
            ]
            create_file "sub/dir2/construct-sub2.py", [
                "print('        --> construct-sub2.py')",
                "print('        <-- construct-sub2.py')",
            ]

            cmd "#{jcons}" do
                comment "two levels of include"
                stdout_equal [
                    "--> construct.py",
                    "    --> construct-sub1.py",
                    "        --> construct-sub2.py",
                    "        <-- construct-sub2.py",
                    "    <-- construct-sub1.py",
                    "<-- construct.py",
                ]
            end
        else
            create_file "construct.rb", [
                "puts '--> construct.rb'",
                "Cons.include 'sub/dir1/construct-sub1.rb'",
                "puts '<-- construct.rb'",
                "STDOUT.flush",
                "exit! 0",
            ]
            create_file "sub/dir1/construct-sub1.rb", [
                "puts '    --> construct-sub1.rb'",
                "Cons.include '../../sub/dir2/construct-sub2.rb'",
                "puts '    <-- construct-sub1.rb'",
            ]
            create_file "sub/dir2/construct-sub2.rb", [
                "puts '        --> construct-sub2.rb'",
                "puts '        <-- construct-sub2.rb'",
            ]

            cmd "#{jcons}" do
                comment "two levels of include"
                stdout_equal [
                    "--> construct.rb",
                    "    --> construct-sub1.rb",
                    "        --> construct-sub2.rb",
                    "        <-- construct-sub2.rb",
                    "    <-- construct-sub1.rb",
                    "<-- construct.rb",
                ]
            end
        end
    end

    #----------------------------------------------------------------------

    def test_20_cons_array_value
        #----------
        create_file "prog.c", [
            "int main() { return 0; }",
        ]

        #------
        create_file "construct.py", [
            "e = Cons( CFLAGS = '-DFOO=1' )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} -B" do
            comment "CFLAGS simple string"
            stdout_equal [
                "gcc -DFOO=1 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            written_files "prog", "prog.o"
        end

        #------
        create_file "construct.py", [
            "e = Cons( CFLAGS = ['-DFOO=1', '-DBAR=2'] )",
            "e.program('prog', 'prog.c')",
        ]

        cmd "#{jcons} -B" do
            comment "CFLAGS array value"
            stdout_equal [
                "gcc -DFOO=1 -DBAR=2 -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            written_files "prog", "prog.o"
        end
    end

    #----------------------------------------------------------------------
    # -r

    def test_10_option_r
        create_file "main.c", [
            "extern int mod1_fun();",
            "int main() { return 10 + mod1_fun(); }",
        ]

        create_file "mod1.c", [
            "int mod1_fun() { return 20; }",
        ]

        #----------
        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'main.c', 'mod1.c')",
        ]

        cmd "#{jcons} -B" do
            comment "initial build"
            stdout_equal [
                "gcc -c main.c -o main.o",
                "gcc -c mod1.c -o mod1.o",
                "gcc -o prog main.o mod1.o",
            ]
            written_files "prog", "main.o", "mod1.o"
        end

        #----------
        cmd "#{jcons} -r" do
            comment "remove object files"
            stdout_equal [
                "Removed main.o",
                "Removed mod1.o",
                "Removed prog",
            ]
            removed_files "prog", "main.o", "mod1.o"
        end

        #----------
        cmd "#{jcons} -r" do
            comment "remove object files (again)"
            stdout_equal [
                "jcons: already removed: .",
            ]
        end

    end

    #----------------------------------------------------------------------
    # -p

    def test_10_option_p
        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'main.c', 'mod1.c')",
            "e.command('bbb', 'aaa', 'cp %INPUT %OUTPUT')",
            "e.install('opt/local/bin', 'my-emacs')",
        ]

        cmd "#{jcons} -p" do
            comment "list targets"
            stdout_equal [
                "bbb",
                "main.o",
                "mod1.o",
                "opt/local/bin/my-emacs",
                "prog",
            ]
        end

        cmd "#{jcons} -p opt" do
            comment "with dir argument"
            stdout_equal [
                "opt/local/bin/my-emacs",
            ]
        end
    end

    #----------------------------------------------------------------------
    # The detection SCC:s of the "include tree" was flawed for a long time.
    # The "finishing times" of the DFS traversal was not correct for
    # parallel builds, where some header files where generated by commands
    # taking "non-zero" time.
    # This test triggered that bug, by having two "paths"
    # in the graph leading to the same file, and building in parallel.

    def test_20_include_dag_bug
        create_file "prog.c", [
            '#include "b.h"',
            '#include "a.h"',
            '',
            'int main() { return 0; }',
        ]

        create_file "a.h", [
            '/* a.h */',
            '#include "a2.h"',
        ]
        create_file "a2.h", [
            '/* a2.h */',
            '#include "a3.h"',
        ]
        create_file "a3.h.in", [
            '/* a3.h.in */',
        ]

        create_file "b.h", [
            '/* b.h */',
            '#include "b2.h"',
        ]
        create_file "b2.h.in", [
            '/* b2.h.in */',
            '#include "a2.h"',
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.program('prog', 'prog.c')",
            "e.command('a3.h', 'a3.h.in', 'sleep 3 && cp %INPUT %OUTPUT')",
            "e.command('b2.h', 'b2.h.in', 'sleep 1 && cp %INPUT %OUTPUT')",
        ]

        cmd "#{jcons} -j2 prog" do
            stdout_equal [
                "sleep 1 && cp b2.h.in b2.h",
                "sleep 3 && cp a3.h.in a3.h",
                "gcc -c prog.c -o prog.o",
                "gcc -o prog prog.o",
            ]
            created_files "a3.h", "b2.h", "prog", "prog.o"
        end

        cmd "#{jcons} -j2 prog" do
            stdout_equal [
                "jcons: already up-to-date: 'prog'",
            ]
        end
    end

    #----------------------------------------------------------------------

    def test_20_prog_dep_bug
        create_file "foo.in", [
            '/* foo.in */',
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('cp-copy', '/bin/cp', 'cp %INPUT %OUTPUT && false')",
            "e.command('foo.out', 'foo.in', './cp-copy %INPUT %OUTPUT')",
        ]

        cmd "#{jcons} -k foo.out" do
            exit_nonzero
            stdout_equal [
                "cp /bin/cp cp-copy && false",
                "jcons: error: error building 'cp-copy'",
            ]
            created_files "cp-copy"
        end

        cmd "#{jcons} -k foo.out" do
            exit_nonzero
            stdout_equal [
                "cp /bin/cp cp-copy && false",
                "jcons: error: error building 'cp-copy'",
            ]
            written_files "cp-copy"
        end
    end

    #----------------------------------------------------------------------

    def test_20_up_to_date_bug
        create_file "a.in", [
            '/* a.in */',
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command(['a.out1','a.out2'], 'a.in', 'process.rb a.in -- a.out1 a.out2')",
        ]

        cmd "#{jcons} a.out1 a.out2" do
            stdout_equal [
                "process.rb a.in -- a.out1 a.out2",
            ]
            created_files "a.out1", "a.out2"
        end

        # both files mentioned

        cmd "#{jcons} a.out1 a.out2" do
            stdout_equal [
                "jcons: already up-to-date: 'a.out1'",
                "jcons: already up-to-date: 'a.out2'",
            ]
        end

        # files in different order does not affect STDOUT

        cmd "#{jcons} a.out2 a.out1" do
            stdout_equal [
                "jcons: already up-to-date: 'a.out1'",
                "jcons: already up-to-date: 'a.out2'",
            ]
        end
    end

    #----------------------------------------------------------------------

    def test_20_program_LIBS
        return if JCMDS_TO_TEST
        create_file "prog.c", [
            'extern int foo_var;',
            'int main() { return 10 + foo_var; }',
        ]
        create_file "foo.c", [
            'int foo_var = 34;',
        ]

        create_file "construct.py", [
            "e = Cons( LIBS = 'foo', LIBDIRS = '.' )",
            "e.program('prog', 'prog.c')",
            "e.static_library('libfoo', 'foo.c')",
            "e.install_as('foo.c.copy', 'foo.c')",
        ]

        cmd "#{jcons} prog" do
            comment "initial build (library too)"
            stdout_equal [
                "gcc -c prog.c -o prog.o",
                "gcc -c foo.c -o foo.o",
                "ar rc libfoo.a foo.o",
                "gcc -o prog prog.o -L. -lfoo",
            ]
            created_files "prog", "prog.o", "foo.o", "libfoo.a"
        end

        cmd "./prog" do
            comment "correct exit code"
            exit_status 44
        end

        #--------------------

        create_file "foo.c", [
            'int foo_var = 5;',
        ]

        cmd "#{jcons} prog" do
            comment "rebuild caused by library change"
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "ar rc libfoo.a foo.o",
                "gcc -o prog prog.o -L. -lfoo",
            ]
            modified_files "prog", "foo.o", "libfoo.a"
        end

        cmd "./prog" do
            comment "changed exit code"
            exit_status 15
        end

        #--------------------

        create_file "foo.c", [
            '#error "syntax error in file"',
        ]

        cmd "#{jcons} prog" do
            comment "failed rebuild of library"
            exit_nonzero
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "jcons: error: error building 'foo.o'",
            ]
            stderr_equal /syntax error/
            removed_files "foo.o"
        end

        cmd "#{jcons} -k prog" do
            comment "-k should propagate error"
            exit_nonzero
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "jcons: error: error building 'foo.o'",
            ]
            stderr_equal /syntax error/
        end

        cmd "#{jcons} -k prog foo.c.copy" do
            comment "-k does not affect independent target"
            exit_nonzero
            stdout_equal [
                "gcc -c foo.c -o foo.o",
                "jcons: error: error building 'foo.o'",
                "cp foo.c foo.c.copy",
            ]
            stderr_equal /syntax error/
            created_files "foo.c.copy"
        end

    end

    #----------------------------------------------------------------------
    # Each target file of a command should have unique signature,
    # otherwise the cache will not work as intended.
    #
    # Put the two output files of a command, in the cache.
    # Restore the files from the cache, and verify that they are different.
    # If the digests were the same, we would only have one file in the cache.

    def test_20_unique_signature
        ignore_files "CACHE/"

        create_file "a.in", [
            'this is a.in',
        ]

        # both output file have the same basename, to verify that something more
        # than the basename is included in the digest
        create_file "construct.py", [
            "e = Cons()",
            "e.command(['1/a.out', '2/a.out'], 'a.in', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} --cache-dir CACHE 1/a.out 2/a.out" do
            comment "put files in cache"
            stdout_equal [
                'process.rb a.in -- 1/a.out 2/a.out',
            ]
            created_files "1/", "1/a.out", "2/", "2/a.out"
        end

        remove_file "1/a.out"
        remove_file "2/a.out"

        cmd "#{jcons} --cache-dir CACHE 1/a.out 2/a.out" do
            comment "get files from cache (two different ones)"
            stdout_equal [
                'Updated from cache: 1/a.out',
                'Updated from cache: 2/a.out',
            ]
            created_files "1/a.out", "2/a.out"

            assert file_read('1/a.out') != file_read('2/a.out'), "failed test '1/a.out' != '2/a.out'"
        end
    end

    #----------------------------------------------------------------------
    # Each target file should have unique signature, otherwise the
    # cache will not work as intended.
    #
    # Build 'prog' from indentical source files, with same basename,
    # in two different directories. Make sure they don't use each others
    # cached files. This means they have different digest.

    def test_33_unique_signature_2
        ignore_files "CACHE/"
        prepend_local_path "."

        content = [
            '#include <stdio.h>',
            'int main() { printf("this is %s\n", __FILE__); return 0; }',
        ]
        create_file "111/foo.c", content
        create_file "222/foo.c", content

        create_file "construct.py", [
            "e = Cons()",
            "e.object('prog.o', '111/foo.c')",
            "e.program('prog', 'prog.o')",
        ]

        cmd "#{jcons} --cache-dir CACHE prog" do
            comment "put files in cache"
            stdout_equal [
                "gcc -c 111/foo.c -o prog.o",
                "g++ -o prog prog.o",
            ]
            created_files "prog.o", "prog"
        end

        remove_file "prog.o"
        remove_file "prog"

        #--------------------

        cmd "#{jcons} --cache-dir CACHE prog" do
            comment "get files from cache"
            stdout_equal [
                "Updated from cache: prog.o",
                "Updated from cache: prog",
            ]
            created_files "prog.o", "prog"
        end

        cmd "prog" do
            comment "correct prog output"
            stdout_equal [
                /^this is .*111\/foo\.c$/
            ]
        end

        #--------------------

        remove_file "prog.o"
        remove_file "prog"

        create_file "construct.py", [
            "e = Cons()",
            "e.object('prog.o', '222/foo.c')",
            "e.program('prog', 'prog.o')",
        ]

        cmd "#{jcons} --cache-dir CACHE prog" do
            comment "do not get files from cache (different source)"
            stdout_equal [
                "gcc -c 222/foo.c -o prog.o",
                "g++ -o prog prog.o",
            ]
            created_files "prog.o", "prog"
        end

        cmd "prog" do
            comment "correct prog output"
            stdout_equal [
                /^this is .*222\/foo\.c$/
            ]
        end

    end

    #----------------------------------------------------------------------

end

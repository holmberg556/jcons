
require 'fileutils'
require 'rbconfig'

# TODO: use array arguments to Jcons.command
# TODO: test expansion of %FOO in construct.rb

CWD = Dir.pwd

PYTHON = true

#----------------------------------------------------------------------

class TC_jcons < Cmdtest::Testcase

    def setup
        # prepend_local_path "bin"
        ignore_file ".jcons/"
        prepend_path "scripts"
    end

    def is_windows
        false
    end

    def jcons
        ENV["JCONS_TO_TEST"] || "python3 #{CWD}/jcons.py"
    end

    #----------------------------------------

    def test_02_unicode_in_filenames
        create_file "a.txt", [
            "This is a.txt",
        ]

        create_file "construct.py", [
            "e = Cons()",
            "e.command('räksmörgås.txt', 'a.txt', 'process.rb %INPUT -- %OUTPUT')",
            "e.command('c.txt', 'räksmörgås.txt', 'process.rb %INPUT -- %OUTPUT')",
        ]

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt'"
            stdout_equal [
                'process.rb a.txt -- räksmörgås.txt',
                'process.rb räksmörgås.txt -- c.txt',
            ]
            created_files "räksmörgås.txt", "c.txt"
        end

        cmd "#{jcons} c.txt" do
            comment "building 'c.txt' again"
            stdout_equal [
                "jcons: already up-to-date: 'c.txt'",
            ]
        end
    end

end

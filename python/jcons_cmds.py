#!/usr/bin/env python3
#----------------------------------------------------------------------
# jcons_cmds.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

import argparse
import os
import os.path
import signal
import sys

import jconslib
from jconslib.commands import read_commands, change_conscript_dir

#----------------------------------------------------------------------

def _parse_arguments():
    parser = argparse.ArgumentParser("jcons_cmds")
    add = parser.add_argument

    add("--verbose", action="store_true",
        help="be more verbose")
    add("-v", "--version", action="store_true",
        help="show version")
    add("-q", "--quiet", action="store_true",
        help="be more quiet")
    add("-j", "--parallel", metavar='N', type=int, default=1,
        help="build in parallel")
    add("-k", "--keep-going", action="store_true",
        help="continue after errors")
    add("-n", "--dry-run", action="store_true",
        help="just print commands")
    add("-B", "--always-make", action="store_true",
        help="always build targets")
    add("--cache-dir", action="store",
        help="name of cache directory")
    add("--cache-force", action="store",
        help="copy existing files into cache")
    add("-r", "--remove", action="store_true",
        help="remove targets")
    add("-p", "--list-targets", action="store_true",
        help="list known targets")
    add("--accept-existing-target", action="store_true",
        help="make updating an existing target a nop")
    add("--dont-trust-mtime", action="store_false",
        dest='trust_mtime',
        help="always consult files for content digest")
    add("--case-sensitive", action="store_true",
        help="force case sensitive file lookup")
    add("--case-insensitive", action="store_true",
        help="force case insensitive file lookup")
    add("--states", action="store_true",
        help="show state transition log")
    add("arg", nargs="*",
        help="target or NAME=VALUE")

    add("-f", "--file", action='append', help="name of *.cmds file")

    opts = parser.parse_args()
    return opts

#----------------------------------------------------------------------

def main():
    cmds_files = None

    opt_version = None
    opt_verbose = None
    opt_list_targets = None
    opt_list_commands = None
    opt_args = None

    opts = _parse_arguments()

    if opts.version:
        print("jcons_cmds version 0.20")
        print()
        print("Copyright 2002-2017 Johan Holmberg <holmberg556@gmail.com>.")
        print("This is non-free software. There is NO warranty; not even")
        print("for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.")
        exit(0)

    jconslib.init(vars(opts))
    if opts.cache_dir:
        set_cachedir(opts.cache_dir)

    def ctrl_c_handler(dummy_signum, dummy_frame):
        print('jcons: got ctrl-c ...')
        jconslib.got_sigint()

    signal.signal(signal.SIGINT, ctrl_c_handler)
    signal.siginterrupt(signal.SIGINT, False)

    cons_cache_by_dir2 = {} # TODO: nested?!

    if not opts.file:
        if "." not in cons_cache_by_dir2:
            cons_cache_by_dir2["."] = {}
        read_commands(sys.stdin, cons_cache_by_dir2["."], ".")
    else:
        for fname in opts.file:
            with open(fname, "r") as f:
                if opts.verbose:
                    print("+++ reading command file '", fname, "'")
                dir = os.path.dirname(fname)
                with change_conscript_dir(dir):
                    if dir not in cons_cache_by_dir2:
                        cons_cache_by_dir2[dir] = {}
                    read_commands(f, cons_cache_by_dir2[dir], dir)

    if opts.list_targets and False:
        for cmd in Cmd.all:
            print([tgt.path for tgt in cmd.tgts])
            print("    ", [src.path for src in cmd.srcs])
            print("        ", cmd.get_cmdline2())
            print()
        exit(0)

    nerrors = jconslib.update_top_files(opts.arg)
    return 0 if nerrors == 0 else 1

if __name__ == '__main__':
    status = main()
    exit(status)

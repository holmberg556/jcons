#!/usr/bin/env python3

from collections import namedtuple, defaultdict
from contextlib import contextmanager
import os
import unittest

from jconslib.filesystem import Dir, File, NotFileError, NotDirError, ConsignEntry


@contextmanager
def empty_tree():
    Dir.init()
    yield

#----------------------------------------------------------------------

class TestConsign(unittest.TestCase):

    def setUp(self):
        self.consign = defaultdict(ConsignEntry)
        self.f1 = self.consign["f1.txt"]
        self.f2 = self.consign["f2.txt"]
        self.f3 = self.consign["f3.txt"]

    def tearDown(self):
        pass

    def test_basic(self):
        self.assertIsInstance( self.f1, ConsignEntry )
        self.assertEqual( len(self.consign), 3 )

    def test_remove(self):
        consign = self.consign

        self.assertIs( consign.get("f2.txt"), self.f2 )
        self.assertIs( consign.get("f3.txt"), self.f3 )
        self.assertIsNot( consign.get("f2.txt"), consign.get("f3.txt") )
        self.assertEqual( len(consign), 3 )

        del consign["f2.txt"]
        self.assertEqual( len(consign), 2 )

    def xxx_test_write_to_file(self):
        consign = self.consign
        consign.get("f4.txt")
        consign.write_to_file(123456, "tmp.consign")

        c2 = Consign.read_from_file("tmp.consign")
        self.assertEqual( len(c2.entries), 4 )
        self.assertEqual( sorted(c2.entries),
                          ['f1.txt', 'f2.txt', 'f3.txt', 'f4.txt'] )


#----------------------------------------------------------------------

class TestFilesystem(unittest.TestCase):

    def setUp(self):
        Dir.init()

    def tearDown(self):
        pass

    def test_basic(self):
        d = Dir.curr_dir
        self.assertEqual(".", d.path)

    def test_lookup_entry(self):
        # simple case
        file1 = Dir.curr_dir.find_file("file1")
        file2 = Dir.curr_dir.find_file("file2")

        self.assertEqual( file1, Dir.curr_dir.lookup_entry("file1") )
        self.assertEqual( file2, Dir.curr_dir.lookup_entry("file2") )
        self.assertEqual( None, Dir.curr_dir.lookup_entry("file3") )

        # file or dir
        foo = Dir.curr_dir.find_file("dir1/subdir1/foo")

        x = Dir.curr_dir.lookup_entry("dir1")
        self.assertTrue( isinstance(x, Dir) )
        self.assertEqual( foo.parent.parent, x )

        x = Dir.curr_dir.lookup_entry("dir1/subdir1")
        self.assertTrue( isinstance(x, Dir) )
        self.assertEqual( foo.parent, x )

        x = Dir.curr_dir.lookup_entry("dir1/subdir1/foo")
        self.assertTrue( isinstance(x, File) )
        self.assertEqual( foo, x )

        # file or dir (absolute)
        foo = Dir.curr_dir.find_file("/dir1/subdir1/foo")

        x = Dir.curr_dir.lookup_entry("/dir1")
        self.assertTrue( isinstance(x, Dir) )
        self.assertEqual( foo.parent.parent, x )

        x = Dir.curr_dir.lookup_entry("/dir1/subdir1")
        self.assertTrue( isinstance(x, Dir) )
        self.assertEqual( foo.parent, x )

        x = Dir.curr_dir.lookup_entry("/dir1/subdir1/foo")
        self.assertTrue( isinstance(x, File) )
        self.assertEqual( foo, x )

        # top too
        e = Dir.curr_dir.lookup_entry("/")
        self.assertTrue( isinstance(e, Dir) )


    def test_lookup_or_fs_dir(self):
        null_dir = None

        ## in top directory

        # existing fs dir
        self.assertNotEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("a-dir") )
        self.assertNotEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("/usr/bin") )

        # non-existing fs dir
        self.assertEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("not-a-dir") )
        self.assertEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("/usr/not-a-dir") )

        # existing dir
        d = Dir.curr_dir.find_dir("a-dir-node")
        self.assertNotEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("a-dir-node") )
        self.assertEqual( d,        Dir.curr_dir.lookup_or_fs_dir("a-dir-node") )

        d2 = Dir.curr_dir.find_dir("/usr/a-dir-node")
        self.assertNotEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("/usr/a-dir-node") )
        self.assertEqual( d2,       Dir.curr_dir.lookup_or_fs_dir("/usr/a-dir-node") )

        # non-existing dir
        self.assertEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("not-a-dir-node") )
        self.assertEqual( null_dir, Dir.curr_dir.lookup_or_fs_dir("/usr/not-a-dir-node") )

        ## in sub-directory
        a_dir = Dir.curr_dir.find_dir("a-dir")

        # existing fs dir
        self.assertNotEqual( null_dir, a_dir.lookup_or_fs_dir("a-subdir") )
        self.assertNotEqual( null_dir, a_dir.lookup_or_fs_dir("/usr/bin") )

        # non-existing fs dir
        self.assertEqual( null_dir, a_dir.lookup_or_fs_dir("not-a-subdir") )
        self.assertEqual( null_dir, a_dir.lookup_or_fs_dir("/usr/not-a-subdir") )

        # existing dir
        d = a_dir.find_dir("a-subdir-node")
        self.assertNotEqual( null_dir, a_dir.lookup_or_fs_dir("a-subdir-node") )
        self.assertEqual( d,        a_dir.lookup_or_fs_dir("a-subdir-node") )

        d2 = a_dir.find_dir("/usr/a-subdir-node")
        self.assertNotEqual( null_dir, a_dir.lookup_or_fs_dir("/usr/a-subdir-node") )
        self.assertEqual( d2,       a_dir.lookup_or_fs_dir("/usr/a-subdir-node") )

        # non-existing dir
        self.assertEqual( null_dir, a_dir.lookup_or_fs_dir("not-a-subdir-node") )
        self.assertEqual( null_dir, a_dir.lookup_or_fs_dir("/usr/not-a-subdir-node") )

        # existing fs file - not dir
        with empty_tree():
            self.assertRaises( NotDirError, Dir.curr_dir.lookup_or_fs_dir, "a-dir/a-dir-file/a-subdir-file" )

        # existing fs file - not dir - in path
        with empty_tree():
            self.assertNotEqual( None, Dir.curr_dir.lookup_or_fs_file("a-dir/a-dir-file") )
            self.assertRaises( NotDirError, Dir.curr_dir.lookup_or_fs_dir, "a-dir/a-dir-file" )


    def test_lookup_or_fs_file(self):
        null_file = None

        ## in top directory

        # existing fs file
        self.assertNotEqual( null_file, Dir.curr_dir.lookup_or_fs_file("a-file") )
        self.assertNotEqual( null_file, Dir.curr_dir.lookup_or_fs_file("/etc/passwd") )

        # non-existing fs file
        self.assertEqual( null_file, Dir.curr_dir.lookup_or_fs_file("not-a-file") )
        self.assertEqual( null_file, Dir.curr_dir.lookup_or_fs_file("/etc/not-a-file") )

        # existing file
        d = Dir.curr_dir.find_file("a-file-node")
        self.assertNotEqual( null_file, Dir.curr_dir.lookup_or_fs_file("a-file-node") )
        self.assertEqual( d,         Dir.curr_dir.lookup_or_fs_file("a-file-node") )

        d2 = Dir.curr_dir.find_file("/etc/a-file-node")
        self.assertNotEqual( null_file, Dir.curr_dir.lookup_or_fs_file("/etc/a-file-node") )
        self.assertEqual( d2,        Dir.curr_dir.lookup_or_fs_file("/etc/a-file-node") )

        # non-existing file
        self.assertEqual( null_file, Dir.curr_dir.lookup_or_fs_file("not-a-file-node") )
        self.assertEqual( null_file, Dir.curr_dir.lookup_or_fs_file("/etc/not-a-file-node") )

        ## in sub-directory
        a_dir = Dir.curr_dir.find_dir("a-dir")

        # existing fs file
        self.assertNotEqual( null_file, a_dir.lookup_or_fs_file("a-dir-file") )
        self.assertNotEqual( null_file, a_dir.lookup_or_fs_file("/etc/passwd") )

        # non-existing fs file
        self.assertEqual( null_file, a_dir.lookup_or_fs_file("not-a-dir-file") )
        self.assertEqual( null_file, a_dir.lookup_or_fs_file("/etc/not-a-dir-file") )

        # existing file
        d = a_dir.find_file("a-dir-file-node")
        self.assertNotEqual( null_file, a_dir.lookup_or_fs_file("a-dir-file-node") )
        self.assertEqual( d,        a_dir.lookup_or_fs_file("a-dir-file-node") )

        d2 = a_dir.find_file("/etc/a-dir-file-node")
        self.assertNotEqual( null_file, a_dir.lookup_or_fs_file("/etc/a-dir-file-node") )
        self.assertEqual( d2,        a_dir.lookup_or_fs_file("/etc/a-dir-file-node") )

        # non-existing dir
        self.assertEqual( null_file, a_dir.lookup_or_fs_file("not-a-subdir-node") )
        self.assertEqual( null_file, a_dir.lookup_or_fs_file("/etc/not-a-subdir-node") )

    def test_lookup_or_fs_file_AFTER_FIND(self):
        # find + lookup in non-existing dir
        Dir.curr_dir.find_dir("non-existing-dir")
        self.assertEqual( None, Dir.curr_dir.lookup_or_fs_file("non-existing-dir/a-file") )

        # find dir + lookup file
        Dir.curr_dir.find_dir("dir1/dir2")
        self.assertRaises(NotFileError, Dir.curr_dir.lookup_or_fs_file, "dir1/dir2")

        # entry but non-existing
        self.assertEqual( None, Dir.curr_dir.lookup_or_fs_file("a-dir/link-nonexisting-file") )

    def test_lookup_or_fs_file_EXCEPTIONS(self):

        self.assertRaises(NotFileError, Dir.curr_dir.lookup_or_fs_file, "a-dir")
        self.assertRaises(NotDirError,  Dir.curr_dir.lookup_or_fs_file, "a-file/another-file")

        # second time looking ofr 'a-file' as dir
        self.assertRaises(NotDirError,  Dir.curr_dir.lookup_or_fs_file, "a-file/another-file")

        # second time, cached result (looking for file)
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_file("a-dir/nonexisting"))
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_file("a-dir/nonexisting"))

        # second time, cached result (looking for dir)
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_file("nonexisting-dir/file"))
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_file("nonexisting-dir/file"))

    def test_lookup_or_fs_dir_EXCEPTIONS(self):

        self.assertRaises(NotDirError, Dir.curr_dir.lookup_or_fs_dir, "a-file")
        self.assertRaises(NotDirError, Dir.curr_dir.lookup_or_fs_dir, "a-file/another-file")

        # second time, cached result (looking for file)
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_dir("a-dir/nonexisting"))
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_dir("a-dir/nonexisting"))

        # second time, cached result (looking for dir)
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_dir("nonexisting-dir/dir"))
        self.assertEqual(None, Dir.curr_dir.lookup_or_fs_dir("nonexisting-dir/dir"))

    #----------------------------------------------------------------------
    # previously test of "lookup_file", now "lookup_entry"
    # TODO: merge with other method testing "lookup_entry"

    def test_lookup_entry_MORE(self):
        # known vs. unknown
        known      = Dir.curr_dir.find_file("known")
        known2     = Dir.curr_dir.find_file("sub1/known2")
        known3     = Dir.curr_dir.find_file("sub1/sub2/known3")

        self.assertEqual(None,   Dir.curr_dir.lookup_entry("unknown"))
        self.assertEqual(known,  Dir.curr_dir.lookup_entry("known"))

        self.assertEqual(None,   Dir.curr_dir.lookup_entry("sub1/unknown2"))
        self.assertEqual(known2, Dir.curr_dir.lookup_entry("sub1/known2"))

        self.assertEqual(None,   Dir.curr_dir.lookup_entry("sub1/sub2/unknown3"))
        self.assertEqual(known3, Dir.curr_dir.lookup_entry("sub1/sub2/known3"))

        # known vs. unknown for absolute paths
        abs_known      = Dir.curr_dir.find_file("/known")
        abs_known2     = Dir.curr_dir.find_file("/sub1/known2")
        abs_known3     = Dir.curr_dir.find_file("/sub1/sub2/known3")

        self.assertEqual(None,       Dir.curr_dir.lookup_entry("/unknown"))
        self.assertEqual(abs_known,  Dir.curr_dir.lookup_entry("/known"))

        self.assertEqual(None,       Dir.curr_dir.lookup_entry("/sub1/unknown2"))
        self.assertEqual(abs_known2, Dir.curr_dir.lookup_entry("/sub1/known2"))

        self.assertEqual(None,       Dir.curr_dir.lookup_entry("/sub1/sub2/unknown3"))
        self.assertEqual(abs_known3, Dir.curr_dir.lookup_entry("/sub1/sub2/known3"))

        # same name in other dir
        file1_dir1 = Dir.curr_dir.find_file("some/dir1/file1")
        file1_dir2 = Dir.curr_dir.find_file("some/dir2/file1")

        file1_dir1_lookup = Dir.curr_dir.lookup_entry("some/dir1/file1")
        file1_dir2_lookup = Dir.curr_dir.lookup_entry("some/dir2/file1")

        self.assertNotEqual(file1_dir1_lookup, file1_dir2_lookup)
        self.assertEqual(file1_dir1, file1_dir1_lookup)
        self.assertEqual(file1_dir2, file1_dir2_lookup)

        # NotDirError: seeing File when expecting Dir
        Dir.curr_dir.find_file("some/should_be_dir")
        self.assertRaises(NotDirError, Dir.curr_dir.lookup_entry, "some/should_be_dir/file1")
        self.assertRaises(NotDirError, Dir.curr_dir.lookup_entry, "some/should_be_dir/subdir/file1")

    #----------------------------------------------------------------------

    def test_find_file(self):
        # simple case
        file1 = Dir.curr_dir.find_file("file1")
        file2 = Dir.curr_dir.find_file("file2")
        self.assertEqual("file1", file1.path)
        self.assertEqual("file2", file2.path)
        self.assertNotEqual(file1, file2)

        file1_again = Dir.curr_dir.find_file("file1")
        self.assertEqual(file1, file1_again)

        # simple case in a subdirectory
        file1 = Dir.curr_dir.find_file("some/path1/file1")
        file2 = Dir.curr_dir.find_file("some/path1/file2")
        self.assertEqual("some/path1/file1", file1.path)
        self.assertEqual("some/path1/file2", file2.path)
        self.assertNotEqual(file1, file2)

        file1_again = Dir.curr_dir.find_file("some/path1/file1")
        self.assertEqual(file1, file1_again)

        # same name in other dir
        path2_file1 = Dir.curr_dir.find_file("some/path2/file1")
        self.assertNotEqual(file1, path2_file1)

        some_x = file1.parent.parent
        some_y = path2_file1.parent.parent
        self.assertEqual(some_x, some_y)

        #
        g1 = Dir.curr_dir.find_file("d1/g1")
        g1_again = Dir.curr_dir.find_file("d1/g1")
        g2 = Dir.curr_dir.find_file("d1/g2")
        h1 = Dir.curr_dir.find_file("d2/h1")
        d1 = Dir.curr_dir.find_dir("d1")
        self.assertEqual(g1, g1_again)
        self.assertEqual(d1, g1.parent)
        self.assertEqual(d1, g2.parent)
        self.assertNotEqual(d1, h1.parent)

        #
        self.assertEqual( "x/y/z", Dir.curr_dir.find_file("x/y/z").path )
        self.assertEqual( "x/y",   Dir.curr_dir.find_dir("x/y").path )
        self.assertEqual( "x",     Dir.curr_dir.find_dir("x").path )

        ### ASSERT_THROW(Dir.curr_dir.find_file("."), NotFileError)

        # TODO: decide if using exception or hard exit(2)
        Dir.curr_dir.find_file("some_dir/a_file")
        ### ASSERT_THROW(Dir.curr_dir.find_file("some_dir"), NotFileError)

        Dir.curr_dir.find_file("some_file")
        ### ASSERT_THROW(Dir.curr_dir.find_file("some_file/another_file"), NotDirError)

    #----------------------------------------------------------------------
    # find_file__sharp_prefix

    def test_find_file__sharp_prefix(self):
        dir1 = Dir.curr_dir.find_dir("aaa/bbb/ccc")

        f1 = Dir.curr_dir.find_file("file1")
        f2 = dir1.find_file("#/file1")
        self.assertEqual( f1.path, f2.path )
        self.assertEqual( f1, f2 )

        f2 = dir1.find_file("#/bbb/file2")
        f1 = Dir.curr_dir.find_file("bbb/file2")
        self.assertEqual( f1.path, f2.path )
        self.assertEqual( f1, f2 )

        d2 = dir1.find_dir("#/bbb")
        d1 = Dir.curr_dir.find_dir("bbb")
        self.assertEqual( d1.path, d2.path )
        self.assertEqual( d1, d2 )

# #----------------------------------------------------------------------
# # find_file__drive_letter

# TEST_F(TestDir, find_file__drive_letter)
# {
#   dir

#   f1 = Dir.curr_dir.find_file("c:/aaa/bbb/ccc/file1")
#   self.assertEqual( "c:/aaa/bbb/ccc/file1", f1.path )

#   {
#     # walk up to root
#     dir = f1.parent
#     self.assertEqual( "c:/aaa/bbb/ccc", dir.path )

#     dir = dir.parent
#     self.assertEqual( "c:/aaa/bbb", dir.path )

#     dir = dir.parent
#     self.assertEqual( "c:/aaa", dir.path )

#     dir = dir.parent
#     self.assertEqual( "c:/", dir.path )

#     dir = dir.parent
#     self.assertEqual( "c:/", dir.path )
#   }

#   {
#     # in two steps
#     f2 = Dir.curr_dir.find_dir("c:/").find_file("aaa/bbb/ccc/file1")
#     self.assertEqual( f1, f2 )

#     f2 = Dir.curr_dir.find_dir("c:/aaa").find_file("bbb/ccc/file1")
#     self.assertEqual( f1, f2 )
#   }

#   {
#     # small diffs
#     f2 = Dir.curr_dir.find_file("c:/aaa/bbb/ccc/file2")
#     self.assertNotEqual( f1, f2 )

#     f2 = Dir.curr_dir.find_file("d:/aaa/bbb/ccc/file1")
#     self.assertNotEqual( f1, f2 )

#   }
# }

# #----------------------------------------------------------------------
# # find_file__unc_path

# TEST_F(TestDir, find_file__unc_path)
# {
#   dir

#   f1 = Dir.curr_dir.find_file("#server/share/aaa/bbb/ccc/file1")
#   self.assertEqual( "#server/share/aaa/bbb/ccc/file1", f1.path )

#   {
#     # walk up to root
#     dir = f1.parent
#     self.assertEqual( "#server/share/aaa/bbb/ccc", dir.path )

#     dir = dir.parent
#     self.assertEqual( "#server/share/aaa/bbb", dir.path )

#     dir = dir.parent
#     self.assertEqual( "#server/share/aaa", dir.path )

#     dir = dir.parent
#     self.assertEqual( "#server/share", dir.path )

#     dir = dir.parent
#     self.assertEqual( "#server/share", dir.path )
#   }

#   {
#     # in two steps
#     f2 = Dir.curr_dir.find_dir("#server/share").find_file("aaa/bbb/ccc/file1")
#     self.assertEqual( f1, f2 )

#     f2 = Dir.curr_dir.find_dir("#server/share/aaa").find_file("bbb/ccc/file1")
#     self.assertEqual( f1, f2 )
#   }

#   {
#     # small diffs
#     f2 = Dir.curr_dir.find_file("#server/share/aaa/bbb/ccc/file2")
#     self.assertNotEqual( f1, f2 )

#     f2 = Dir.curr_dir.find_file("#server2/share/aaa/bbb/ccc/file1")
#     self.assertNotEqual( f1, f2 )

#     f2 = Dir.curr_dir.find_file("#server/share2/aaa/bbb/ccc/file1")
#     self.assertNotEqual( f1, f2 )


#   }
# }

    #----------------------------------------------------------------------

    def test_find_dir(self):
        # simple case
        dir1 = Dir.curr_dir.find_dir("dir1")
        dir2 = Dir.curr_dir.find_dir("dir2")
        self.assertEqual("dir1", dir1.path)
        self.assertEqual("dir2", dir2.path)
        self.assertNotEqual(dir1, dir2)

        dir1_again = Dir.curr_dir.find_dir("dir1")
        self.assertEqual(dir1, dir1_again)

        # simple case in a subdirectory
        dir1 = Dir.curr_dir.find_dir("some/path1/dir1")
        dir2 = Dir.curr_dir.find_dir("some/path1/dir2")
        self.assertEqual("some/path1/dir1", dir1.path)
        self.assertEqual("some/path1/dir2", dir2.path)
        self.assertNotEqual(dir1, dir2)

        dir1_again = Dir.curr_dir.find_dir("some/path1/dir1")
        self.assertEqual(dir1, dir1_again)

        # same name in other dir
        path2_dir1 = Dir.curr_dir.find_dir("some/path2/dir1")
        self.assertNotEqual(dir1, path2_dir1)

        some_x = dir1.parent.parent
        some_y = path2_dir1.parent.parent
        self.assertEqual(some_x, some_y)

        # with "." and ".." in path
        dir = Dir.curr_dir.find_dir("x/././././y")
        self.assertEqual("x/y", dir.path)

        dir = Dir.curr_dir.find_dir("x/y/./.././z")
        self.assertEqual("x/z", dir.path)

        dir = Dir.curr_dir.find_dir("x/y/./../../z")
        self.assertEqual("z", dir.path)

        dir = Dir.curr_dir.find_dir("x/y/./../../z/..")
        self.assertEqual(".", dir.path)

        # above current dir
        dir = Dir.curr_dir.find_dir("x/y/./../../z/../..")
        self.assertEqual(os.path.dirname(os.getcwd()), dir.path)

        # two steps
        sub2 = Dir.curr_dir.find_dir("sub1/sub2")
        self.assertEqual("sub1/sub2", sub2.path)

        # two steps == 2 * one step
        sub2_b = Dir.curr_dir.find_dir("sub1").find_dir("sub2")
        self.assertEqual("sub1/sub2", sub2_b.path)
        self.assertEqual(sub2, sub2_b)

    #----------------------------------------------------------------------

    def test_path(self):
        # relative paths
        f = Dir.curr_dir.find_file("some/dir/f2.txt")
        self.assertEqual( f.path, "some/dir/f2.txt" )

        f = Dir.curr_dir.find_file("some/f2.txt")
        self.assertEqual( f.path, "some/f2.txt" )

        f = Dir.curr_dir.find_file("f2.txt")
        self.assertEqual( f.path, "f2.txt" )

        ## PC drive letter paths
        #f = Dir.curr_dir.find_file("c:/path/to/dir1/f1.txt")
        #self.assertEqual( f.path, "c:/path/to/dir1/f1.txt" )

        #f = Dir.curr_dir.find_file("c:/path/to/f1.txt")
        #self.assertEqual( f.path, "c:/path/to/f1.txt" )

        #f = Dir.curr_dir.find_file("c:/path/f1.txt")
        #self.assertEqual( f.path, "c:/path/f1.txt" )

        #f = Dir.curr_dir.find_file("c:/f1.txt")
        #self.assertEqual( f.path, "c:/f1.txt" )

        # UNIX paths
        f = Dir.curr_dir.find_file("/path/to/dir1/f1.txt")
        self.assertEqual( f.path, "/path/to/dir1/f1.txt" )

        f = Dir.curr_dir.find_file("/path/to/f1.txt")
        self.assertEqual( f.path, "/path/to/f1.txt" )

        f = Dir.curr_dir.find_file("/path/f1.txt")
        self.assertEqual( f.path, "/path/f1.txt" )

        f = Dir.curr_dir.find_file("/f1.txt")
        self.assertEqual( f.path, "/f1.txt" )


    #----------------------------------------------------------------------

    def test_parent(self):
        # relative
        dir1 = Dir.curr_dir.find_dir("x/y/z/dir1")
        dir = dir1.parent
        self.assertEqual( Dir.curr_dir.find_dir("x/y/z"), dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("x/y"),   dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("x"),     dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("."),     dir )

        # absolute
        dir1 = Dir.curr_dir.find_dir("/x/y/z/dir1")
        dir = dir1.parent
        self.assertEqual( Dir.curr_dir.find_dir("/x/y/z"), dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("/x/y"),   dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("/x"),     dir )
        dir = dir.parent
        self.assertEqual( Dir.curr_dir.find_dir("/"),     dir )

        dir = dir.parent
        self.assertEqual( None, dir )

#         # Windows drive letter path
#         dir1 = Dir.curr_dir.find_dir("c:/x/y/z/dir1")
#         dir = dir1.parent
#         self.assertEqual( Dir.curr_dir.find_dir("c:/x/y/z"), dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("c:/x/y"),   dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("c:/x"),     dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("c:/"),     dir )

#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("c:/"),     dir )

#         # Windows unc path
#         dir1 = Dir.curr_dir.find_dir("#server/share/x/y/z/dir1")
#         dir = dir1.parent
#         self.assertEqual( Dir.curr_dir.find_dir("#server/share/x/y/z"), dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("#server/share/x/y"),   dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("#server/share/x"),     dir )
#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("#server/share/"),     dir )

#         dir = dir.parent
#         self.assertEqual( Dir.curr_dir.find_dir("#server/share/"),     dir )

    #----------------------------------------------------------------------

    def test_absolute_paths_unix(self):
        # dirs
        dir1 = Dir.curr_dir.find_dir("x/y/z/dir1")
        dir2 = Dir.curr_dir.find_dir("x/y/z/dir2")
        self.assertEqual("x/y/z/dir1", dir1.path)
        self.assertEqual("x/y/z/dir2", dir2.path)

        root = Dir.curr_dir.find_dir("/")
        etc = Dir.curr_dir.find_dir("/etc")
        self.assertEqual("/", root.path)
        self.assertEqual("/etc", etc.path)

        # files
        file1 = Dir.curr_dir.find_file("x/y/z/file1")
        file2 = Dir.curr_dir.find_file("x/y/z/file2")
        self.assertEqual("x/y/z/file1", file1.path)
        self.assertEqual("x/y/z/file2", file2.path)

        root_file = Dir.curr_dir.find_file("/root.txt")
        etc_file = Dir.curr_dir.find_file("/etc/etc.txt")
        self.assertEqual("/root.txt", root_file.path)
        self.assertEqual("/etc/etc.txt", etc_file.path)


    #----------------------------------------------------------------------


    def test_file_md5(self):
        import binascii
        a_file = Dir.curr_dir.find_file("a-file")
        self.assertEqual(b'865a4186e753d260ecbb01ebadc4ffa7',
                         binascii.hexlify(a_file.file_md5()))
        examples = [
            [    10,    b'6dc9a1aba993afc49b4898a7e48c21cc'],
            [    20,    b'a547325e91ee22388fb308c7c60ef3ed'],
            [  1000,    b'4ebc14649e431377793b4284e9d67a6e'],
            [ 10000,    b'ff6919a1709bb32d4000f79a5c137243'],
            [ 20000,    b'97befb436b7830dea954481cd831751f'],
            [ 50000,    b'c7895765a0d11eaddd2db88da9f62254'],
            [100000,    b'77eede168b102bf039741a6dfd9fa379'],
            [200000,    b'b0cc4fc8cc4d9c7912c65c0015c984bb'],
            [     2,    b'8cfa2282b17de0a598c010f5f0109e7d'],
            [     0,    b'd41d8cd98f00b204e9800998ecf8427e'],
            ]
        tmp_file = Dir.curr_dir.find_file("tmp-file")

        for n, hexdigest in examples:
            with open("tmp-file", "wb") as f:
                f.write(b'12345' * n)

            actual_hexdigest = binascii.hexlify(tmp_file.file_md5())
            self.assertEqual(hexdigest, actual_hexdigest)


#----------------------------------------------------------------------

if __name__ == '__main__':
    unittest.main()

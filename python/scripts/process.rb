#!/usr/bin/ruby

def inc_indent(str)
    if str.length - str.lstrip.length < 12
        "  " + str
    else
        str
    end
end

if File.exist?("JCONS_PROCESS_ENV")
    File.open("JCONS_PROCESS_ENV") do |f|
        while line = f.gets
            if line =~ /^(.*?)=(.*)/
                ENV[$1] = $2
            end
        end
    end
end

Signal.trap("INT") do
    puts "Ignoring SIGINT ..."
end


$opt_verbose = false
$opt_no_tgt = false

srcs = []
tgts = []
slow = nil
err = nil

cmdline_part = 0

while ARGV.size > 0 && ARGV[0] =~ /^-/
    arg = ARGV.shift
    case arg
    when "-v"
        $opt_verbose = true
    when "--no-tgt"
        $opt_no_tgt = true
    else
        puts "Error: unknown option: #{arg}"
        exit 1
    end
end

for arg in ARGV
    case
    when arg == "--"
        cmdline_part += 1
    when cmdline_part == 0
        srcs << arg
    when cmdline_part == 1
        tgts << arg
    when arg =~ /^SLEEP=(\d)$/
        slow = $1.to_i
    when arg =~ /^EXIT=(\d+)$/
        err = $1.to_i
    when arg =~ /^COMMENT=/
        # ignore
    else
        raise RuntimeError, "illegal argument"
    end
end

if ENV["JCONS_PROCESS_SHOW_EXISTING_FILES"]
    files = ENV["JCONS_PROCESS_SHOW_EXISTING_FILES"].split(" ")
    existing_files = files.select {|file| File.exist?(file) }
    puts "process: existing files: #{existing_files.join(',')}"
end

content = []
for src in srcs
    content << "This is from #{src}:"
    File.open(src) do |f|
        while line = f.gets
            content << inc_indent(line)
        end
    end
end

#system "env"
write_output = ! ENV["JCONS_PROCESS_NO_TGT_WRITTEN"] && ! $opt_no_tgt

if write_output
    for tgt in tgts
        File.open(tgt, "w") do |f|
            f.puts "Written to #{tgt}:"
            for line in content
                f.puts inc_indent(line)
            end
        end
    end
end

if slow
    puts "beginning of SLOW program ..." if $opt_verbose
    sleep slow
    puts "end of SLOW program ..." if $opt_verbose
end

if err
    exit err
end

if ENV["JCONS_PROCESS_SLEEP"]
    sleep ENV["JCONS_PROCESS_SLEEP"].to_f
end

if ENV["JCONS_PROCESS_FAIL_AT_END"]
    exit(1)
end

exit 0

t1 = Time.new
loop do
    t2 = Time.now
    break if t2 - t1 > 5
end

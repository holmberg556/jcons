#!/usr/bin/env python3
#----------------------------------------------------------------------
# jcons_cmds_bridge.py
#----------------------------------------------------------------------
# Copyright 2015 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

from os.path import dirname, normpath, join
import os
import sys

import jcons
import jconslib.script

from jconslib.engine import Engine
from jconslib import Global
from jconslib.filesystem import File

class MyMain(jcons.Main):

    def __init__(self, jcons_to_test):
        self.jcons_to_test = jcons_to_test

    def main(self):
        super().main()

    def update_top_files(self, args):
        self.call_d(args)

    def _print_deps(self, cmd, f):
        for src in cmd.srcs:
            print("# INPUT: %s" % src.path, file=f)
        for tgt in cmd.tgts:
            print("# OUTPUT: %s" % tgt.path, file=f)

    def call_d(self,targets):
        from jconslib.cmd import Cmd
        import subprocess
        import itertools
        import sys
        sys.stdout.flush()
        sys.stderr.flush()
        tmp_file = "/tmp/tmp.cmds-%d" % os.getpid()
        with open(tmp_file, "w") as f:
            for tgt in File.exe_deps_tgts:
                for src in tgt.exe_deps:
                    print("# INPUT: %s" % src.path, file=f)
                print("# EXE_DEPEND: %s" % tgt.path, file=f)
                print(":", file=f)

            for c in Cmd.all:
                if c.extra_deps:
                    for dep in c.extra_deps:
                        print("# INPUT: %s" % dep.path, file=f)
                    print("# DEPEND: %s" % c.tgts[0].path, file=f)
                    print(":", file=f)

            for c in Cmd.all:
                line = c.get_cmdline2().split(' ')

                build_cache = c.cons.build_cache()
                if build_cache is not None:
                    print("# CACHEDIR: %s" % build_cache.cache_dir.path, file=f)

                if line[0] == 'false':
                    self._print_deps(c, f)

                elif line == ['foo.rb', 'bla', 'bla']:
                    self._print_deps(c, f)

                elif line[0:2] == ['failing.rb', 'cp'] and len(line) == 4:
                    self._print_deps(c, f)

                elif line[0:4] == ['sleep', '1', '&&', 'cp'] and len(line) == 6:
                    self._print_deps(c, f)
                elif line[0:4] == ['sleep', '3', '&&', 'cp'] and len(line) == 6:
                    self._print_deps(c, f)

                elif line[0:2] == ['cp', '-f'] and len(line) == 4:
                    self._print_deps(c, f)

                elif line[0] == 'process.rb':
                    self._print_deps(c, f)

                elif len(line) == 5 and line[0] == 'cp' and line[3] == '&&':
                    self._print_deps(c, f)

                elif len(line) == 3 and line[0] == './cp-copy':
                    self._print_deps(c, f)

                    # line = line[1:]
                    # while len(line) > 1 and line[0][0:2] == '--':
                    #     line = line[1:]
                    # parts = [list(vs) for k,vs in itertools.groupby(line, lambda x: x == '--') if not k]
                    # for fname in parts[0]:
                    #     print("# INPUT: %s" % fname, file=f)
                    # for fname in parts[1]:
                    #     print("# OUTPUT: %s" % fname, file=f)

                print(c.get_cmdline2(), file=f)
        opts = []
        if Engine.njobs != 1: opts.extend(["--parallel", str(Engine.njobs)])
        if Global.always_make: opts.append("-B")
        if Global.keep_going: opts.append("-k")
        if Global.list_targets: opts.append("-p")
        if Global.remove: opts.append("-r")
        if Global.quiet: opts.append("-q")
        if Global.cache_force: opts.append("--cache-force")
        #if Global.build_cache: opts.extend(["--cache-dir", Global.build_cache.cache_dir.path])
        opts_str = ' '.join(opts)
        cmdline = self.jcons_to_test + " < " + tmp_file + " " + opts_str + " " + ' '.join(targets)
        #err = subprocess.call("echo > /dev/tty " + cmdline, shell=True)
        err = subprocess.call(cmdline, shell=True)
        os.unlink(tmp_file)
        exit(err)

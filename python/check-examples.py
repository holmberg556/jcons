#!/usr/bin/env python3

import os
import re
import sys

def skip(lines, i, pred):
    while i < len(lines) and pred(lines[i]):
        i += 1
    return i

def indent(line):
    return len(re.match(r'(\s*)', line).group(1))

def empty(line):
    return line.strip() == ''

def code_block(lines, i):
    indent1 = indent(lines[i])
    a = skip(lines, i+1, empty)
    b = skip(lines, a, lambda x: empty(x) or indent(x) > indent1)
    return a,b

def block_filename(block):
    m = re.match(r'\s+# (\S+.py)', block[0])
    if m: return m.group(1)
    m = re.match(r'\s+/\* (\S+) \*/', block[0])
    if m: return m.group(1)
    return None

def write_code_block(block):
    fname = block_filename(block)
    if fname:
        os.system("cd tmp-manual && mkdir -p $(dirname %s)" % fname)
        with open("tmp-manual/" + fname, "w") as f:
            for line in block:
                line = re.sub(r'^    ', "", line)
                f.write(line + "\n")

def cmd_output(cmd):
    with open("tmp-manual/command.sh", "w") as f:
        f.write(cmd + "\n")
    os.system("cd tmp-manual && sh ./command.sh > stdout.log 2>&1")
    lines = open("tmp-manual/stdout.log").readlines()
    return [ line.replace("\n", "") for line in lines ]

def write_shell_block(block):
    in_output = False
    indent = re.match(r'(\s+)', block[0]).group(1)
    for line in block:
        line = re.sub(r'^' + indent, "", line)
        m = re.match(r'\$ (.*)', line)
        if m:
            cmd = m.group(1)
            output = cmd_output(cmd)
            print(indent + "$ " + cmd)
            for line in output:
                print(indent + line)
            in_output = True
            continue
        m = re.match(r'\s*$', line)
        if m:
            print()
            in_output = False
            continue
        if not in_output:
            print(indent + line)


def run_command(cmd):
    #print("=== " + cmd)
    if cmd == "REMOVE .jcons":
        os.system("cd tmp-manual && rm -rf .jcons")
        return
    m = re.match(r'COPY (\S+) (\S+)$', cmd)
    if m:
        src, tgt = m.groups()
        os.system("cd tmp-manual && mkdir -p $(dirname %s)" % tgt)
        os.system("cd tmp-manual && cp %s %s" % (src,tgt))
        return
    raise RuntimeError("cmd=" + cmd)

def main():
    lines = [ line.replace("\n", "") for line in sys.stdin ]

    i = 0
    while i < len(lines):
        if re.match(r'.. FILE:', lines[i]):
            i1 = i
            a,b = code_block(lines, i)
            i = b
            write_code_block(lines[a:b])
            for line in lines[i1:i]:
                print(line)
            continue
        if re.match(r'.. code-block:: ', lines[i]):
            i1 = i
            a,b = code_block(lines, i)
            i = b
            write_code_block(lines[a:b])
            for line in lines[i1:i]:
                print(line)
            continue
        if re.match(r'.*::$', lines[i]):
            print(lines[i])
            print()
            a,b = code_block(lines, i)
            i = b
            write_shell_block(lines[a:b])
            continue
        m  = re.match(r'.. COMMAND: (.*)', lines[i])
        if m:
            print(lines[i])
            i += 1
            run_command(m.group(1))
            continue

        print(lines[i])
        i += 1

main()





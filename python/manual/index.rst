.. JCons Manual documentation master file, created by
   sphinx-quickstart on Fri Mar 22 00:47:41 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

JCons Manual
============

.. toctree::
   :maxdepth: 2

   intro
   tour
   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


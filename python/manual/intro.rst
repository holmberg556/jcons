
Introduction
------------

JCons is a Make_ replacement, i.e. a program for building other
programs. JCons is designed to build C/C++ programs, but
any files that are produced in a well defined manner can be
created under the control of JCons. As can be guessed from the name,
JCons' main inspiration is Cons_, the make tool written in Perl_
(described in the Spring 1998 issue of "The Perl Journal").
Cons_ implemented a number of interesting ideas:

- The build description files can be *programs* written in an
  existing language (e.g. Perl_), instead of using a description
  language that is unique to the make tool (e.g. Makefile-syntax or Jam-files).
  A `Domain Specific Language`_ often start out as quite a simple language,
  but gradually acquire more and more "programming featues".
  By instead using an existing programming language, one gets the benefits
  of a familiar and welldefined programming environment.

- Cryptographic checksums can be used to get more reliable dependency
  checking than that provided by Make_, where file timestamps are used
  to decide if a file should be rebuilt.
  For each target file built, a cryptographic checksum is stored that includes
  everything the file depends on, e.g. the source files and the command
  line used to build the target file. *Any* change to a dependency (transitively too)
  will result in a changed checksum, and trigger a rebuild.

- Automatic detection of ``#include`` file dependencies makes building
  of C/C++ programs much more reliable.

JCons tries to steal most of the good ideas of Cons_.
But there are important differences too, for example:

- JCons uses Python_ as its primary language for description files.

- JCons can build in parallel.

- JCons is much faster than Cons when doing an up-to-date check.

The ideas implemented by Cons_ are not so special today.  A number of
similar tools exist, most notably SCons_ (also inspired by Cons_).
JCons is quite minimalistic and lacks many features available in other
tools. There is for example no "autoconf like" functionality.

So why develop JCons?
Firstly, because it was fun. Writing a make tool seems quite easy in the
beginning. But to get a reliable, fast and useful tool requires attention to
many small details. This has been an interesting challenge.
Secondly, because I believe JCons does some things better than both Cons_ and
SCons_. JCons is for example much faster at up-to-date checks than
both Cons_ and SCons_.

About this document
+++++++++++++++++++

The rest of this document tries to describe how to use JCons.
First a simple example will be given to illustrate the overall way
JCons works. Then a guide to the different JCons features follows. And
finally a reference part will describe all the details.

If you have used Cons_ before, you should recognize many
things. The biggest difference from Cons is of course that JCons uses Python
as scripting language instead of Perl.

The examples in the text have actually been executed as real
commands. This should ensure that they stay correct at all times.

How JCons works
+++++++++++++++

1) JCons looks for a file ``construct.py``, and loads it as Python_ code.
   The file has the same role for JCons as the ``Makefile`` has for Make_.

2) The code in ``construct.py`` should call methods in the :py:class:`Cons`
   class. These methods are used to inform JCons what things there are
   to build.

3) Once the ``construct.py`` has been read, JCons uses the collected
   information to start its build engine.

A minimal example of using JCons is given below:

.. code-block:: python

    # construct.py
    e = Cons()
    e.command("readme.pdf", "readme.ps",  "ps2pdf %INPUT %OUTPUT")
    e.command("readme.ps",  "readme.txt", "enscript -q %INPUT -o %OUTPUT")

The calls to :py:meth:`Cons.command` tells JCons how the output files
can be produced from the input files::

    $ echo a line of text > readme.txt   # create source file
    $ jcons
    enscript -q readme.txt -o readme.ps
    ps2pdf readme.ps readme.pdf
    $ jcons                             # nothing to do 2nd time
    jcons: up-to-date: .
    $ echo one more line >> readme.txt
    $ jcons                             # input file has changed
    enscript -q readme.txt -o readme.ps
    ps2pdf readme.ps readme.pdf

All methods of the :py:class:`Cons` class are of this type. Each
method is useful in a different situation: :py:meth:`Cons.program` to build a
program, :py:meth:`Cons.static_library` to create a library, etc. But all methods
could in principle have been implemented on top of the :py:meth:`Cons.command`
method.

.. include:: pointers.txt

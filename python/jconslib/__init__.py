#----------------------------------------------------------------------
# jcons/__init__.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------
# TODO: move + test

import os

class Global:
    list_targets = False
    always_make = False
    trust_mtime = True
    keep_going = False
    dry_run = False
    accept_existing_target = False
    cache_force = False
    build_cache = None
    remove = False
    list_targets = False
    quiet = False
    verbose = False
    case_sensitive = False
    case_insensitive = False

    logging = False

    got_sigint = False
    should_terminate = False

from .engine import Engine
from .cons import Cons
from .buildcache import BuildCache

from .filesystem import Dir
from .cons import Cons

def init(opts):
    Global.quiet                  = opts['quiet']
    Global.always_make            = opts['always_make']
    Global.keep_going             = opts['keep_going']
    Global.dry_run                = opts['dry_run']
    Global.list_targets           = opts['list_targets']
    Engine.njobs                  = opts['parallel']
    Global.remove                 = opts['remove']
    Global.accept_existing_target = opts['accept_existing_target']
    Global.trust_mtime            = opts['trust_mtime']
    Global.case_sensitive         = opts['case_sensitive']
    Global.case_insensitive       = opts['case_insensitive']

    Dir.init()
    Cons.set_conscript_dir(os.getcwd())

    if opts['cache_force'] is not None:
        Global.cache_force = opts['cache_force']
    if opts['cache_dir']:
        set_cache_dir(opts['cache_dir'])

def init_out_of_source(source_top):
    tgtdir = Cons.conscript_dir
    _conscript_dir_stack.append(Cons.conscript_dir)
    Cons.conscript_dir = Cons.conscript_dir.find_dir(source_top)
    Cons.conscript_dir.init_out_of_source(source_top, tgtdir)

def update_top_files(targets):
    engine = Engine()
    engine.update_top_files(targets)
    return engine.nerrors

def got_sigint():
    Global.got_sigint = True
    Global.should_terminate = True

def set_cache_dir(cache_dir):
    d = Cons.conscript_dir
    Global.build_cache = BuildCache(d, cache_dir)

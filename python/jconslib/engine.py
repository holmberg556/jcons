#----------------------------------------------------------------------
# jcons/engine.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""with Engine class running the build state machines"""

import os

from .builder import FUN_base, FUN_top_level, FUN_update_tgt, set_engine
from .filesystem import Dir, File
from . import Global
from .process import process_start, process_wait

#----------------------------------------------------------------------

class Engine:

    __slots__ = ("nerrors", "executing",
                 "ready")

    njobs = 1           # TODO: instance variable???

    terminate_warning_given = False

    def __init__(self):
        self.nerrors = 0
        self.executing = {}
        self.ready = None

    def add_error(self):
        self.nerrors += 1
        if not Global.keep_going:
            Global.should_terminate = True

    def execute_cmd(self, fun, cmdline, noop_command, exitstatus_ptr):
        pid = process_start( cmdline, noop_command )
        self.executing[pid] = (fun, exitstatus_ptr)
        fun.sem_acquire()


    def return_to_caller(self, fun):
        if fun.caller:
            if not fun.status_ok:
                fun.caller.status_ok = False
            fun.caller.sem_release()
            fun.caller = None

    def _show_queue(self, str1):
        print("QUEUE--%s: ------------------------------ [%d / %d]" % (
            str1,
            len(self.executing),
            Engine.njobs
            ))
        for q in self.ready:
            print("QUEUE--%s: %s" % (str1, q))

    def run(self):
        ready = self.ready
        executing = self.executing
        njobs = Engine.njobs
        while ready or executing:
            while ready and len(executing) < njobs:
                fun = ready.pop()
                if Global.should_terminate and not fun.finish_after_cmd:
                    # unwind call stack
                    fun.release_semaphores()
                    self.return_to_caller(fun)
                else:
                    # normal case
                    fun.call_state()

            if executing:
                if Global.should_terminate and not Engine.terminate_warning_given:
                    Engine.terminate_warning_given = True
                    print("jcons: *** waiting for commands to finish ...")
                pid, exitstatus = process_wait(True)
                if pid != 0:
                    sem_fun, exitstatus_ptr = executing.pop(pid)
                    exitstatus_ptr(exitstatus)
                    sem_fun.sem_release()
                    if Global.should_terminate:
                        sem_fun.finish_after_cmd = True

                    #if (WIFSIGNALED(exitstatus)) {
                    #    sem_fun->signalled_command(exitstatus);
                    #}
                # always fall through


    def lookup_targets(self, targets, entries, tgts):
        errors = 0
        for target in targets:
            tgt = Dir.curr_dir.lookup_entry(target)
            if not tgt:
                print("jcons: error: don't know how to build '" + target + "'")
                errors += 1
            elif isinstance(tgt, File):
                tgts.append(tgt)
                tgt.top_target = True
                entries.append(tgt)
            elif isinstance(tgt, Dir):
                tgt.append_files_under(tgts)
                entries.append(tgt)
            else:
                print("jcons: error: internal error")
                exit(1)
        return errors


    def update_top_files(self, targets):
        if len(targets) == 0:
            targets.append(".")

        if Global.list_targets:
            for target in targets:
                tgt = Dir.curr_dir.lookup_entry(target)
                if tgt:
                    if isinstance(tgt, File):
                        print(tgt.path)
                    elif isinstance(tgt, Dir):
                        tgts = []
                        tgt.append_files_under(tgts)
                        for f in tgts:
                            print(f.path)
            exit(0)

        entries = []
        tgts = []
        errors = self.lookup_targets(targets, entries, tgts)
        if errors > 0:
            exit(1)

        if Global.remove:
            for tgt in tgts:
                if tgt.file_exist_FORCED():
                    path = tgt.path
                    print("Removed " + path)
                    os.unlink(path)
                    tgt.st_invalid_error()
                else:
                    # skip silently
                    pass
        else:
            self.ready = [FUN_top_level(tgts)]
            set_engine(self, self.ready)
            self.run()

        Dir.terminate()
        if Global.got_sigint:
            print("jcons: *** got interrupt, terminating")
            exit(1)

        paths = ','.join(sorted(x.caller.tgt.path
                                for x in FUN_base.all_fun_list
                                if isinstance(x, FUN_update_tgt)
                                and x.caller
                                and isinstance(x.caller, FUN_update_tgt)
                               ))
        if paths:
            print("jcons: error: circular dependency for '%s'" % paths)
            exit(1)

        for entry in entries:
            if isinstance(entry, Dir):
                if not entry.dirty:
                    if Global.remove:
                        print("jcons: already removed: " + entry.path)
                    else:
                        print("jcons: up-to-date: " + entry.path)

#----------------------------------------------------------------------
# jcons/script.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""handle reading of construct.py files"""

import copy
import os
import sys

from . import set_cache_dir
from . import cons

#----------------------------------------------------------------------

def _map_cons_element(x):
    if isinstance(x, (list,tuple)):
        return _map_cons_list(x)
    else:
        return x._cons if isinstance(x, Cons) else x

def _map_cons_list(xs):
    return [_map_cons_element(x) for x in xs]

#----------------------------------------------------------------------

_cmdline_overrides = None

class Cons:
    def __init__(self, **kwargs):
        self._cons = cons.Cons(**kwargs)
        for key, value in _cmdline_overrides:
            self._cons.setenv(key, value)

    def clone(self, **kwargs):  # TODO: kwargs
        obj = copy.copy(self)
        obj._cons = obj._cons.clone(**kwargs)
        for key, value in _cmdline_overrides:
            obj._cons.setenv(key, value)
        return obj

    def command(self, tgts, srcs, command, **kwargs):
        return self._cons.command(tgts, srcs, command, **kwargs)

    def object(self, obj, src):
        return self._cons.object(obj, src)

    def objects(self, *srcs):
        srcs2 = _map_cons_list(srcs)
        return self._cons.objects(*srcs2)

    def program(self, prog, *srcs):
        srcs2 = _map_cons_list(srcs)
        return self._cons.program(prog, *srcs2)

    def static_library(self, lib, *srcs):
        srcs2 = _map_cons_list(srcs)
        return self._cons.static_library(lib, *srcs2)

    def exe_depends(self, tgt, src):
        return self._cons.exe_depends(tgt, src)

    def depends(self, tgt, src):
        return self._cons.depends(tgt, src)

    def install(self, tgtdir, src):
        return self._cons.install(tgtdir, src)

    def install_as(self, tgt, src):
        return self._cons.install_as(tgt, src)

    @classmethod
    def include(cls, fname):
        saved_dir = os.getcwd()
        os.chdir(os.path.dirname(fname) or ".")
        cons.Cons.set_conscript_dir(os.getcwd())
        try:
            _read_conscript(os.path.basename(fname))
        finally:
            os.chdir(saved_dir)
            cons.Cons.set_conscript_dir(saved_dir)


    @classmethod
    def global_cache_dir(cls, path):
        set_cache_dir(path)

    def cache_dir(self, dir1):
        self._cons.cache_dir(dir1)

#----------------------------------------------------------------------

_exports_stack = []

class Exports():
    pass

_exports_stack.append(dict())

def read_construct(filename, overrides):
    global _cmdline_overrides
    _cmdline_overrides = overrides
    return _read_conscript(filename)

def _read_conscript(filename):
    """Read construct/conscript files"""

    exports = copy.copy(_exports_stack[-1])

    globals_ = dict()
    globals_["Cons"] = Cons
    globals_["exports"] = exports

    try:
        with open(filename) as f:
            co = compile(f.read(), filename, "exec")
    except IOError as e:
        print("jcons: error: failed to read %s" % filename,
              file=sys.stderr)
        sys.exit(1)
    except SyntaxError as e:
        print("jcons: error: syntax error reading %s: %s" % (filename, e),
              file=sys.stderr)
        sys.exit(1)

    _exports_stack.append(exports)
    exec(co, globals_)
    _exports_stack.pop()

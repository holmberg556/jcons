#----------------------------------------------------------------------
# jcons/buildcache.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""handle cache of built files (eg. *.o), enabled by command line option"""

from binascii import hexlify
from os.path import exists
import os

#----------------------------------------------------------------------

class BuildCache:

    __slots__ = ("cache_dir",)

    def __init__(self, d, cache_dir):
        self.cache_dir = d.find_dir(cache_dir)
        self.cache_dir._cache_dir_p = True

    def put(self, dep_sig, file):
        dep_sig_str = hexlify(dep_sig).decode()
        prefix = dep_sig_str[0]
        cache_dir = self.cache_dir.find_dir(prefix)
        ok = cache_dir.mkdir_p()
        if not ok: return

        cache_file = cache_dir.find_file(dep_sig_str)
        if not exists(cache_file.path):
            ok = self._link_or_copy(file.path, cache_file.path)
            if not ok: return

        cache_file.st_updated(dep_sig)


    def get(self, dep_sig, file):
        dep_sig_str = hexlify(dep_sig).decode()
        prefix = dep_sig_str[0]
        cache_dir = self.cache_dir.find_dir(prefix)
        ok = cache_dir.mkdir_p()
        if not ok: return False

        cache_file = cache_dir.find_file(dep_sig_str)
        if not cache_file.file_exist():
            return False

        if cache_file.db_dep_sig() != dep_sig:
            os.unlink(cache_file.path)
            return False

        ok = self._link_or_copy(cache_file.path, file.path)
        if not ok:
            os.unlink(file.path)
            return False

        file.st_updated(dep_sig)
        # TODO: quiet sometimes
        print("Updated from cache:", file.path)
        return True

    def _link_or_copy(self, src, tgt):
        try:
            os.link(src, tgt)
            return True
        except OSError:
            return False

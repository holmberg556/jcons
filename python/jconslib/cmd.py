#----------------------------------------------------------------------
# jcons/cmd.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""with Cmd class representing each possible command to run"""

import re

def _quoted(str1):
    if re.search(r'[ \t"]', str1):
        return '"' + str1.replace('"', '\\"') + '"'
    else:
        return str1

MODE_RAW     = 1
MODE_COOKING = 2
MODE_DONE    = 3

class Cmd:
    __slots__ = ( "cons", "srcs", "tgts", "cmdline", "uses_cpp", "mode",
                  "extra_deps",
                  "uses_libpath", "exitstatus", "includes_digest",
                  "f_cmdname",
                  "_cmdline1")

    all = []

    def __init__(self, cons):
        self.cons = cons
        self.srcs = []
        self.tgts = []
        self.mode = MODE_RAW
        self.extra_deps = None
        self.uses_cpp = False
        self.uses_libpath = None
        self.exitstatus = None
        self.f_cmdname = None
        self.cmdline = None
        self._cmdline1 = None
        Cmd.all.append(self)

    #-----------------------------------
    # pass on 'invalid' call to all targets

    def st_invalid_error(self):
        for tgt in self.tgts:
            tgt.st_invalid_error()

    #-----------------------------------
    # pass on 'propagate' call to all targets

    def st_propagate_error(self):
        for tgt in self.tgts:
            tgt.st_propagate_error()

    #-----------------------------------

    def get_extra_deps(self):
        if self.extra_deps is None:
            self.extra_deps = set()
        return self.extra_deps

    def set_exitstatus(self, status):
        self.exitstatus = status

    def find_program(self):
        if self.f_cmdname is None:
            local_cmdline = self.get_cmdline1()
            i = local_cmdline.find(' ')
            cmdname = local_cmdline[0:i]
            self.f_cmdname = self.cons.env.program_path().find_program(cmdname)
        return  self.f_cmdname

    def get_cmdline1(self):
        if self._cmdline1 is None:
            self._cmdline1 = self.cons.env.expand(self.cmdline)
        return self._cmdline1

    def get_cmdline2(self):
        env = self.cons.env
        env.set("INPUT",  ' '.join(_quoted(x.path) for x in self.srcs))
        env.set("OUTPUT", ' '.join(_quoted(x.path) for x in self.tgts))
        cmdline = env.expand_all(self.cmdline)
        return cmdline

    def get_targets_str(self):
        return ' '.join(tgt.path for tgt in self.tgts)

    def append_sig(self, md5):
        local_cmdline = self.get_cmdline1()
        md5.update(local_cmdline.encode())

        f = self.find_program()
        if f:
            md5.update( f.path.encode() )
            md5.update( f.db_content_sig() )
            if f.exe_deps:
                for exe_dep in f.exe_deps:
                    md5.update( exe_dep.path.encode() )
                    md5.update( exe_dep.db_content_sig() )

#----------------------------------------------------------------------
# jcons/builder.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""the central state machines building files"""

from collections import defaultdict
from hashlib import md5 as hashlib_md5
import os
import stat

from .cmd import MODE_RAW, MODE_COOKING, MODE_DONE
from . import Global

WHITE = 1
GREY  = 2
BLACK = 3

#======================================================================

def log_method_calls(cls):
    def log_one(cls, name, f):
        def logger(*args, **kwargs):
            print((cls.__name__, name))
            return f(*args, **kwargs)
        setattr(cls, name, logger)
    if os.getenv("JCONS_LOG_METHODS"):
        for k, v in vars(cls).items():
            if callable(v):
                log_one(cls, k, v)
    return cls

@log_method_calls
class IncludesTree:

    __slots__ = ("deps", "inverted_deps", "finished", "visited")

    def __init__(self):
        self.deps = defaultdict(list)
        self.inverted_deps = None
        self.finished = []
        self.visited = set()

    def add_edge(self, from_, to):
        self.deps[from_].append(to)

    def add_finished(self, node):
        self.finished.append(node)

    def gen_inverted_deps(self):
        inverted_deps = defaultdict(list)
        for from_, tos in self.deps.items():
            for to in tos:
                inverted_deps[to].append(from_)
        self.inverted_deps = inverted_deps

    def set_visited(self, node):
        self.visited.add(node)

    def visited_p(self, node):
        return node in self.visited

    def calculate_finished(self):
        for from_, tos in self.deps.items():
            from_.color = WHITE
            for to in tos:
                to.color = WHITE
        self.finished = []

        for node in self.deps:
            if node.color == WHITE:
                self._calculate_finished_dfs(node)

    def _calculate_finished_dfs(self, node):
        node.color = GREY
        if node in self.deps:
            for node2 in self.deps[node]:
                if node2.color == WHITE:
                    self._calculate_finished_dfs(node2)

        node.color = BLACK
        self.finished.append(node)

    def sccs_prepare(self):
        for node in self.finished:
            node.color = WHITE

    def sccs_dfs(self, node, group_elements):
        node.color = GREY
        group_elements.append(node)
        for node2 in self.inverted_deps[node]:
            if node2.color == WHITE:
                self.sccs_dfs(node2, group_elements)
        node.color = BLACK

    def show(self):
        print("IncludesTree...")
        for from_, tos in self.deps.items():
            for to in tos:
                print("deps: %s --> %s" % (
                    from_.path,
                    to.path
                    ))
        for from_, tos in self.inverted_deps.items():
            for to in tos:
                print("inverted_deps: %s --> %s" % (
                    from_.path,
                    to.path
                    ))
        for node in self.finished:
            print("finished: %s" % node.path)
        print()


#======================================================================
# FUN_base
#
# Base class for all "functions".
#

_engine = None
_engine_ready = None

def set_engine(engine, ready):
    global _engine, _engine_ready
    _engine = engine
    _engine_ready = ready

_current_FUN = None

_new_FUN_list = []

@log_method_calls
class FUN_base:

    all_fun_list = set()

    __slots__ = ("next", "status_ok", "sem_count", "caller",
                 "finish_after_cmd")

    def __init__(self):
        self.next = self.STATE_start
        self.status_ok = True
        self.finish_after_cmd = False # TODO: per object?
        self.sem_count = 0
        self.caller = _current_FUN
        if self.caller:
            self.caller.sem_acquire()

        _new_FUN_list.append(self)
        FUN_base.all_fun_list.add(self)

    def _display_next(self):
        next_ = self.next
        return "%s[%s] --- .%s" % (
            next_.im_class.__name__,
            self.name(),
            next_.im_func.func_name
            )

    def STATE_start(self):
        raise RuntimeError("subclass responsibility")

    def name(self):
        raise RuntimeError("subclass responsibility")

    def maybe_FUN_update_tgt(self, node):
        if node.build_ok is None:
            FUN_update_tgt(node)
        else:
            if not node.build_ok:
                self.status_ok = False

    def call_state(self):
        self.sem_count = 1

        global _current_FUN
        _current_FUN = self
        del _new_FUN_list[:]
        while self.sem_count == 1 and self.next:
            self.next = self.next()
        _new_FUN_list.reverse()
        _engine_ready.extend(_new_FUN_list)

        if self.next:
            self.sem_count -= 1          # will NOT be == 0 here
        else:
            FUN_base.all_fun_list.remove(self)
            caller = self.caller
            if caller:
                if not self.status_ok:
                    caller.status_ok = False
                caller.sem_release()
                self.caller = None

    def release_semaphores(self):
        pass

    def sem_set(self, count):
        self.sem_count = count

    def sem_acquire(self):
        self.sem_count += 1

    def sem_release(self):
        self.sem_count -= 1
        if self.sem_count == 0:
            _engine_ready.append(self)
        elif self.sem_count < 0:
            raise RuntimeError("sem_release error: " + self.name)

    def execute_cmd(self, cmdline, noop_command, exitstatus_ptr):
        _engine.execute_cmd(self, cmdline, noop_command, exitstatus_ptr)

#======================================================================
# FUN_get_includes_tree
#
# Collect the include tree of a file.
# While traversing the tree, "update" files that can be built.
# Each node is processed in parallel, so files that take a long time
# to generate, will not stall the collecting in other parts of the
# include tree.
#

@log_method_calls
class FUN_get_includes_tree(FUN_base):

    __slots__ = ("level", "cpp_path_map", "node", "includes_tree")

    def __init__(self, cpp_path_map, node, includes_tree, level):
        if Global.logging:
            print("LOG: FUN_get_includes_tree CREATE %s level=%d" % (node.path, level))
        FUN_base.__init__(self)
        self.level = level
        self.cpp_path_map   = cpp_path_map
        self.node  = node
        self.includes_tree = includes_tree

    def __repr__(self):
        return "FUN_get_includes_tree[%s]" % self.name()

    def name(self):
        return self.node.path

    #-----------------------------------

    def STATE_start(self):
        self.maybe_FUN_update_tgt(self.node)
        return self.STATE_node_updated

    #-----------------------------------

    def STATE_node_updated(self):
        node = self.node
        if not node.build_ok:
            # stop after failure. Caller will check for this.
            self.status_ok = False
            return

        cpp_path_map = self.cpp_path_map
        cached_md5 = cpp_path_map.cached_md5

        includes_tree = self.includes_tree
        for inc in cpp_path_map.find_node_includes(node):
            includes_tree.add_edge(node, inc)
            if inc not in includes_tree.visited:
                includes_tree.visited.add(inc)
                if not cached_md5.get(inc):
                    FUN_get_includes_tree(cpp_path_map, inc, includes_tree,
                                          self.level+1)

        return self.STATE_finish

    #-----------------------------------

    def STATE_finish(self):
        # self.status_ok already set
        return


#======================================================================
# FUN_includes_md5
#
# Calculate MD5 of include tree
#

@log_method_calls
class FUN_includes_md5(FUN_base):

    __slots__ = ("cpp_path_map", "node", "includes_tree")

    def __init__(self, cpp_path_map, node):
        if Global.logging:
            print("LOG: FUN_includes_md5 CREATE " + node.path)
        FUN_base.__init__(self)
        self.cpp_path_map = cpp_path_map
        self.node = node
        self.includes_tree = IncludesTree()

    def name(self):
        return self.node.path

    def __repr__(self):
        return "FUN_includes_md5[%s][%d]" % (
            self.node.path,
            self.cpp_path_map.nnn
            )

    #-----------------------------------

    def STATE_start(self):
        node = self.node
        includes_tree = self.includes_tree
        includes_tree.visited.add(node)
        if not self.cpp_path_map.cached_md5.get(node):
            FUN_get_includes_tree(self.cpp_path_map, node, includes_tree, 0)
        return self.STATE_finish

    #-----------------------------------

    def STATE_finish(self):
        if not self.status_ok: return

        includes_tree = self.includes_tree

        includes_tree.calculate_finished()
        includes_tree.gen_inverted_deps()

        if not includes_tree.finished:
            includes_tree.finished.append(self.node)

        includes_tree.sccs_prepare()

        # Find strongly connected components. The second part of the algorithm
        # visiting the nodes in the order given by the "finishing times" from
        # the previous part.

        group_elements = []
        group_offsets = []
        x1 = 0
        for node in reversed(includes_tree.finished):
            if node.color == WHITE:
                includes_tree.sccs_dfs(node, group_elements)
                x2 = len(group_elements)
                group_offsets.append( (x1, x2) )
                x1 = x2

        # Calculate MD5 for each of the "nodes". The value depend on the
        # recursively included files, so the order of traversal of
        # group_offset1/group_offset2 is significant.

        cached_md5 = self.cpp_path_map.cached_md5
        for j1, j2 in reversed(group_offsets):
            if j2 == j1 + 1:
                # *one* file without cycles
                node = group_elements[j1]

                if cached_md5.get(node) is None:
                    # no cached value
                    cached_md5[node] = self.node_calc_md5(node)
            else:
                # a cycle with more than one file
                digest = self.node_calc_md5_cycle(group_elements, j1, j2)
                for k in range(j1, j2):
                    cached_md5[group_elements[k]] = digest
        return

    #-----------------------------------

    def node_calc_md5(self, node):
        deps = self.includes_tree.deps[node]
        if not deps:
            # no dependencies ==> use content
            return node.db_content_sig()

        cached_md5 = self.cpp_path_map.cached_md5
        md5 = hashlib_md5()
        md5.update(node.db_content_sig())
        for dep in deps:
            md5.update(cached_md5[dep])
        return md5.digest()

    #-----------------------------------

    def node_calc_md5_cycle(self, group_elements, j1, j2):
        # set temporarily to make rest of code simpler
        cached_md5 = self.cpp_path_map.cached_md5
        for i in range(j1, j2):
            cached_md5[group_elements[i]] = b"temporary-md5-signature"

        # collect common digest for all files in cycle
        digests = []
        for i in range(j1, j2):
            digests.append( group_elements[i].path.encode() )
            digests.append( self.node_calc_md5(group_elements[i]) )
        digests.sort()
        md5 = hashlib_md5()
        for digest in digests:
            md5.update(digest)
        return md5.digest()

#======================================================================
# FUN_update_tgt

@log_method_calls
class FUN_update_tgt(FUN_base):

    __slots__ = ("tgt", "next_waiting_sem", "info")

    def __init__(self, tgt):
        if Global.logging:
            print("LOG: FUN_update_tgt CREATE " + tgt.path)
        FUN_base.__init__(self)
        self.tgt = tgt
        self.next_waiting_sem = None
        self.info = None

    def __repr__(self):
        return "FUN_update_tgt[%s]" % self.tgt.path

    def name(self):
        return self.tgt.path

    #-----------------------------------

    def STATE_start(self):
        tgt = self.tgt
        cmd = tgt.cmd
        if cmd is None:
            if tgt.file_exist():
                if tgt.top_target:
                    print("jcons: already up-to-date: '%s' (source file)" % (
                        tgt.path
                        ))
                tgt.st_source()
                return self.STATE_finish()
            else:
                self.report_error("don't know how to build", tgt.path)
                tgt.st_propagate_error()
                return self.STATE_finish()
        else:
            if cmd.mode == MODE_RAW:
                cmd.mode = MODE_COOKING
                for src in cmd.srcs:
                    self.maybe_FUN_update_tgt(src)
                extra_deps = cmd.extra_deps
                if extra_deps:
                    for extra_dep in extra_deps:
                        self.maybe_FUN_update_tgt(extra_dep)
                if cmd.uses_libpath:
                    fs_libs = cmd.cons.libpath_map().fs_libs()
                    for fs_lib in fs_libs:
                        self.maybe_FUN_update_tgt(fs_lib)
                return self.STATE_srcs_updated
            elif cmd.mode == MODE_DONE:
                self.status_ok = tgt.build_ok
                return
            elif cmd.mode == MODE_COOKING:
                self.sem_acquire()
                # add 'self' to 'waiting sem' list of tgt
                self.next_waiting_sem = tgt.waiting_sem
                tgt.waiting_sem = self
                return self.STATE_updated_by_other

    #-----------------------------------

    def STATE_updated_by_other(self):
        self.status_ok = self.tgt.build_ok
        return

    #-----------------------------------

    def STATE_srcs_updated(self):
        cmd = self.tgt.cmd
        if not self.status_ok:
            cmd.st_propagate_error()
            return self.STATE_finish() # TODO: correct ???

        if cmd.uses_cpp:
            self.info = info = []
            cpp_path_map = cmd.cons.cpp_path()
            for src in cmd.srcs:
                incs = cpp_path_map.find_node_includes(src)
                for inc in incs:
                    if not cpp_path_map.cached_md5.get(inc):
                        FUN_includes_md5(cpp_path_map, inc)
                info.extend(incs)

        f = cmd.find_program()
        if f:
            self.maybe_FUN_update_tgt(f)
            if f.exe_deps:
                for exe_dep in f.exe_deps:
                    self.maybe_FUN_update_tgt(exe_dep)
        return self.STATE_maybe_execute_cmd

    #-----------------------------------

    def STATE_maybe_execute_cmd(self):
        cmd = self.tgt.cmd
        if not self.status_ok:
            cmd.st_propagate_error()
            return self.STATE_finish() # TODO: correct ???

        if cmd.uses_cpp:
            md5 = hashlib_md5()
            cached_md5 = cmd.cons.cpp_path().cached_md5
            for inc in self.info:
                md5.update(cached_md5[inc])
            cmd.includes_digest = md5.digest()

        tgts_need_update = False
        for tgt_nr, tgt in enumerate(cmd.tgts):
            tgt.new_dep_sig = self.node_new_dep_sig(tgt, tgt_nr)
            if self.need_update_p(tgt):
                tgts_need_update = True
        if tgts_need_update:
            if not Global.accept_existing_target:
                for tgt in cmd.tgts:
                    try:
                        os.unlink(tgt.path)
                    except OSError:
                        # OK to fail removing file
                        pass
            for tgt in cmd.tgts:
                ok = self.mkdir_p(tgt.parent)
                if not ok:
                    cmd.st_propagate_error()
                    return self.STATE_finish()
            build_cache = cmd.cons.build_cache()
            if build_cache:
                if all(build_cache.get(tgt.new_dep_sig, tgt) for tgt in cmd.tgts):
                    # TODO: do anything more ???
                    return self.STATE_finish()

            noop_command = (Global.accept_existing_target and
                            all(tgt.file_exist() for tgt in cmd.tgts))
            cmdline = cmd.get_cmdline2()
            if Global.quiet:
                print("Build " + cmd.get_targets_str())
            else:
                print(cmdline)
            if Global.dry_run:
                for tgt in cmd.tgts:
                    tgt.st_fake_updated()
                return self.STATE_finish()
            else:
                self.execute_cmd(cmdline, noop_command, cmd.set_exitstatus)
                return self.STATE_cmd_executed
        else:
            # ! tgts_need_update
            for tgt in cmd.tgts:
                tgt.st_propagate_ok()
                build_cache = cmd.cons.build_cache()
                if build_cache and Global.cache_force:
                    build_cache.put(tgt.db_dep_sig(), tgt)
                if tgt.top_target:
                    print("jcons: already up-to-date: '" + tgt.path + "'")
            return self.STATE_finish()

    #-----------------------------------

    def STATE_cmd_executed(self):
        cmd = self.tgt.cmd
        if cmd.exitstatus != 0:
            self.report_error("error building", self.tgt.path)
            cmd.st_propagate_error()
            return self.STATE_finish()

        missing_names = ""
        for tgt in cmd.tgts:
            if not tgt.file_exist_FORCED():
                if missing_names:
                    missing_names += " "
                missing_names += tgt.path
        if missing_names:
            self.report_error("tgts not created", missing_names)
            cmd.st_propagate_error()
            return self.STATE_finish()

        # update tgt sigs
        for tgt in cmd.tgts:
            dep_sig = tgt.new_dep_sig
            tgt.st_updated(dep_sig)
            build_cache = cmd.cons.build_cache()
            if build_cache:
                build_cache.put(dep_sig, tgt)
        return self.STATE_finish()

    #-----------------------------------

    def STATE_finish(self):
        self.release_semaphores()
        self.status_ok = self.tgt.build_ok
        return

    #-----------------------------------
    # Release other FUN_update_tgt objects also looking at this node.

    def release_semaphores(self):
        cmd = self.tgt.cmd
        if cmd:
            if cmd.mode == MODE_COOKING:
                for tgt in cmd.tgts:
                    waiting_fun = tgt.waiting_sem
                    while waiting_fun:
                        waiting_fun.sem_release()
                        waiting_fun = waiting_fun.next_waiting_sem
                    tgt.waiting_sem = None
            cmd.mode = MODE_DONE

    #-----------------------------------
    # An error that may halt 'jcons', unless the -k option was given.

    def report_error(self, err, arg):
        print("jcons: error: " + err + " '" + arg + "'")
        _engine.add_error()

    #-----------------------------------
    # Make sure a directory exists.
    # Called for each target of a command, to make sure that the command
    # will be able to write its target files.

    def mkdir_p(self, d):
        try:
            mode = os.stat(d.path).st_mode
            if stat.S_ISDIR(mode):
                return True            # dir already exist, OK
            else:
                self.report_error("expected directory", d.path)
                return False
        except OSError:
            mkdir_ok = self.mkdir_p(d.parent)
            if not mkdir_ok:
                return False
            try:
                os.mkdir(d.path)
                return True
            except OSError:
                self.report_error("could not create dir", d.path)
                return False

    #-----------------------------------
    # Tell if a target file needs to be updated.

    def need_update_p(self, tgt):
        if Global.always_make:
            return True
        if not tgt.file_exist():
            return True
        return tgt.db_dep_sig() != tgt.new_dep_sig

    #-----------------------------------
    # The digest a target file should have to be considered up-to-date.

    def node_new_dep_sig(self, tgt, tgt_nr):
        # The part of the digest all targets of a command have in common.
        # TODO: only do this once (when there are several targets)
        # TODO: maybe move 'md5' or whole method to 'Cmd' class.

        md5 = hashlib_md5()
        cmd = tgt.cmd
        for src in cmd.srcs:
            md5.update(src.db_content_sig())
            md5.update(src.path.encode())
        if cmd.extra_deps:
            digests = sorted(dep.db_content_sig() for dep in cmd.extra_deps)
            for digest in digests:
                md5.update(digest)
        if cmd.uses_cpp:
            md5.update(cmd.includes_digest)
        if cmd.uses_libpath:
            for fs_lib in cmd.cons.libpath_map().fs_libs():
                md5.update(fs_lib.db_content_sig())
        cmd.append_sig(md5)

        # target specific
        md5.update(str(tgt_nr).encode())
        md5.update(tgt.name.encode())
        return md5.digest()

#======================================================================
# FUN_top_level

@log_method_calls
class FUN_top_level(FUN_base):

    __slots__ = ("tgts",)

    def __init__(self, tgts):
        FUN_base.__init__(self)
        self.tgts = tgts

    def name(self):
        return ",".join(tgt.path for tgt in self.tgts)

    def STATE_start(self):
        for tgt in self.tgts:
            self.maybe_FUN_update_tgt(tgt)
        return self.STATE_finish

    def STATE_finish(self):
        return

#----------------------------------------------------------------------
# commands.py
#----------------------------------------------------------------------
# Copyright 2016-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""handle reading of construct.cmds files"""

import copy
import os
import sys
from contextlib import contextmanager

import jconslib.filesystem as filesystem
from jconslib.filesystem import File, Dir
from jconslib.cons import Cons
from jconslib.engine import Engine
from jconslib.cmd import Cmd

g_CHDIR = None
g_INPUT = []
g_OUTPUT = []
g_INCDIR = []
g_DEPEND = []
g_EXE_DEPEND = []

g_CACHEDIR = None

#----------------------------------------------------------------------

@contextmanager
def change_conscript_dir(dirpath):
    Cons.push_conscript_dir(dirpath)
    yield
    Cons.pop_conscript_dir()

#----------------------------------------------------------------------

class ConsCache:

    def __init__(self):
        self.mIncsConsMap = {}

    def get(self, cache_dir, incs):
        if incs:
            idx = (cache_dir, tuple(incs))
        else:
            idx = (cache_dir, None)

        try:
            return self.mIncsConsMap[idx]
        except KeyError:
            cons = Cons() # TODO: change
            if incs:
                cons.set_cpp_path(incs)
            if cache_dir is not None:
                cons.cache_dir(cache_dir)
            self.mIncsConsMap[idx] = cons
            return cons

#----------------------------------------------------------------------

x_arg0 = None
x_args = None
x_prog = None

x_infiles = None
x_infile = None
x_outfile = None

def process(cmdline, cons_cache, subdir):
    global x_args, x_prog
    global x_infiles, x_infile, x_outfile

    x_infiles = []
    x_infile = None
    x_outfile = None

    used_cmdline = (cmdline if subdir == "."
                    else "cd " + subdir + " && " + cmdline)

    # for all but gcc ... -I ...
    cons = cons_cache.get(g_CACHEDIR, None)

    #--------------------
    if len(g_INPUT) == 1 and len(g_OUTPUT) == 1 and g_INCDIR:
        cons = cons_cache.get(g_CACHEDIR, g_INCDIR)
        cons.command(g_OUTPUT, g_INPUT, used_cmdline, scanner="cpp")

    #--------------------
    elif g_INPUT and g_OUTPUT:
        cons.command(g_OUTPUT, g_INPUT, used_cmdline)

    #--------------------
    elif g_INPUT and g_EXE_DEPEND and cmdline == ":":
        for tgt in g_EXE_DEPEND:
            for src in g_INPUT:
                cons.exe_depends(tgt, src)

    #--------------------
    elif g_INPUT and g_DEPEND and cmdline == ":":
        for tgt in g_DEPEND:
            for src in g_INPUT:
                cons.depends(tgt, src)

    #--------------------
    elif (is_cmd("gcc") or is_cmd("g++") or is_cmd("c++")) and opt_c() and find_src() and find_opt_o():
        digest_cmdline_arr = [x_arg0]

        incs = []
        next_i = False
        for arg in x_args:
            if next_i:
                incs.append(arg)
                next_i = False
            elif arg == '-I':
                next_i = True
            elif arg.startswith('-I'):
                incs.append(arg[2:])
            else:
                digest_cmdline_arr.append(arg)
        digest_cmdline = ' '.join(digest_cmdline_arr)

        cons = cons_cache.get(g_CACHEDIR, incs)
        cons.command(x_outfile, x_infile, used_cmdline, "cpp", digest_cmdline=digest_cmdline)

    #--------------------
    elif (is_cmd("gcc") or is_cmd("g++") or is_cmd("c++")) and not opt_c() and find_opt_o():
        for arg in x_args:
            if arg.endswith(".o"):
                x_infiles.append(arg)
        for arg in x_args:
            if arg.endswith(".a"):
                x_infiles.append(arg)

        cons.command(x_outfile, x_infiles, used_cmdline)

    #--------------------
    elif is_cmd("ar") and ar_option(x_args[0]) and nargs() >= 3:
        x_outfile = x_args[1]
        x_infiles.extend(x_args[2:])

        cons.command(x_outfile, x_infiles, used_cmdline)

    #--------------------
    elif is_cmd("cp") and nargs() == 2 and not is_option(x_args[0]):
        x_infile  = x_args[0]
        x_outfile = x_args[1]

        cons.command(x_outfile, x_infile, used_cmdline)

    #--------------------
    elif has_input() and has_output():
        cons.command(x_outfile, x_infile, used_cmdline)

    #--------------------
    else:
        print("                                   ------ Unknown command: ", used_cmdline)
        exit(1)


def is_option(arg):
    return arg.startswith('-')

def nargs():
    return len(x_args)

def is_cmd(name):
    flag = x_prog == name or x_prog.endswith(name) and x_prog[- len(name) - 1] == '-' # TODO: +-1
    return flag

def opt_c():
    return "-c" in x_args

def find_opt_o():
    global x_outfile
    try:
        i = x_args.index("-o")
    except KeyError:
        return False
    else:
        if i > len(x_args) - 2: return False
        x_outfile = x_args[i + 1]
        return True

def ar_option(arg):
    return arg == "rc" or arg == "cq" or arg == "qc"

def find_src():
    global x_infile
    for arg in x_args:
        if arg.endswith(".c") or arg.endswith(".cpp") or arg.endswith(".cc"):
            if x_infile:
                return None
            else:
                x_infile = arg
    return x_infile

def find_objs():
    for arg in x_args:
        if arg.endswith(".o"):
            x_infiles.append(arg)
    return x_infiles or None


def find_archives(clear=False):
    if clear:
        x_infiles[:] = []
    for arg in x_args:
        if arg.endswith(".a"):
            x_infiles.append(arg)

    return x_infiles or None

def has_input():
    global x_infile
    count = 0
    for i, arg in enumerate(x_args):
        if arg == "<" and i < len(x_args) - 1:
            x_infile = x_args[i+1]
            count += 1
    return count == 1

def has_output():
    global x_outfile
    count = 0
    for i, arg in enumerate(x_args):
        if arg == ">" and i < len(x_args) - 1:
            x_outfile = x_args[i+1]
            count += 1
    return count == 1

#----------------------------------------------------------------------

def read_commands(f, cons_cache_by_dir, subdir):
    global g_CHDIR
    global g_INPUT
    global g_OUTPUT
    global g_INCDIR
    global g_DEPEND
    global g_EXE_DEPEND
    global g_CACHEDIR

    cwd = "."
    for line in f:
        line2 = line.strip()
        if len(line2) == 0: continue

        arg0, *args = line2.split()

        if arg0 == "#" and len(args) == 2 and args[0] == "CHDIR:":
            g_CHDIR = args[1]
            continue

        elif line2.startswith("# INPUT: "):
            g_INPUT.append(line2[9:])
            continue

        elif line2.startswith("# OUTPUT: "):
            g_OUTPUT.append(line2[10:])
            continue

        elif line2.startswith("# INCDIR: "):
            g_INCDIR.append(line2[10:])
            continue

        elif line2.startswith("# DEPEND: "):
            g_DEPEND.append(line2[10:])
            continue

        elif line2.startswith("# EXE_DEPEND: "):
            g_EXE_DEPEND.append(line2[14:])
            continue

        elif line2.startswith("# CACHEDIR: "):
            g_CACHEDIR = line2[12:]
            continue

        if line2[0] == '#': continue

        if arg0 == "cd" and len(args) > 2 and args[1] == "&&":
            cwd = args[0]
            arg0 = args[2]
            args = args[3:]

        global x_arg0
        global x_args
        global x_prog

        x_arg0 = arg0
        x_args = args
        x_prog = os.path.basename(arg0)

        if cwd not in cons_cache_by_dir:
            cons_cache_by_dir[cwd] = ConsCache()

        if cwd != ".":
            with change_conscript_dir(cwd):
                process(line2, cons_cache_by_dir[cwd], subdir)

        elif g_CHDIR is None:
            process(line2, cons_cache_by_dir[cwd], subdir)

        else:
            with change_conscript_dir(g_CHDIR):
                line_with_chdir = "cd " + g_CHDIR + " && " + line2
                process(line_with_chdir, cons_cache_by_dir[cwd], subdir)

        cwd = "."
        g_CHDIR = None
        g_INPUT = []
        g_OUTPUT = []
        g_INCDIR = []
        g_DEPEND = []
        g_EXE_DEPEND = []
        g_CACHEDIR = None

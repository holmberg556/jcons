#----------------------------------------------------------------------
# jcons/env.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""with VarEnv class implementing expansion of %CFLAGS variables"""

import os
import re

from .filesystem import Dir

#----------------------------------------------------------------------

class ProgramPath:

    obj_by_path = {}

    __slots__ = ("file_by_cmdname", "dir_arr")

    def __init__(self, program_path):
        self.file_by_cmdname = {}
        self.dir_arr = []
        for dir1 in program_path.split(":"):
            d = Dir.curr_dir.lookup_or_fs_dir(dir1)
            if d:
                self.dir_arr.append(d)

    #-----------------------------------

    @classmethod
    def find(cls, path):
        it = ProgramPath.obj_by_path.get(path)
        if it:
            return it
        else:
            res = ProgramPath.obj_by_path[path] = ProgramPath(path)
            return res

    #-----------------------------------

    def find_program(self, cmdname):
        try:
            return self.file_by_cmdname[cmdname]
        except KeyError:
            if cmdname.find("/") == -1:
                # simple name
                for it2 in self.dir_arr:
                    f = it2.lookup_or_fs_file(cmdname)
                    if f:
                        self.file_by_cmdname[cmdname] = f
                        return f
            else:
                # relative path
                f = Dir.curr_dir.lookup_or_fs_file(cmdname)
                if f:
                    self.file_by_cmdname[cmdname] = f
                    return f
            self.file_by_cmdname[cmdname] = None
            return None

#----------------------------------------------------------------------

_special_names = set(["INPUT", "OUTPUT", "_CPP_INC_OPTS"])

def _clean_spaces(str1):
    res = re.sub(r'\s+', " ", str1)
    res = re.sub(r'\s+$', "", res)
    return res

class VarEnv:

    __slots__ = ("env", "_program_path", "_all", "_expand_cache")

    def __init__(self, **settings):
        self.env = dict()
        for key, val in settings.items():
            self.set(key, val)
        self._program_path = None
        self._expand_cache = dict()
        self._all = None

    def __copy__(self):
        clone = VarEnv.__new__(VarEnv)
        clone.env = dict(self.env)
        clone._program_path = None # ???
        clone._expand_cache = dict()
        return clone

    def program_path(self):
        if self._program_path is None:
            path = os.getenv("PATH")
            if not path:
                raise RuntimeError("PATH not set")
            self._program_path = ProgramPath.find(path)
        return self._program_path

    def _sequence(self, str1):
        parts1 = str1.split('%%')
        parts2 = [re.split(r'%([A-Z_]\w*)', part1) for part1 in parts1]
        parts = parts2[0]
        for part2 in parts2[1:]:
            parts[-1] += "%" + part2[0]
            parts.extend(part2[1:])
        return parts

    def set(self, key, val):
        self.env[key] = self._sequence(val)

    def get(self, key):
        seq = self.env.get(key)
        if seq:
            return self._eval(seq)
        else:
            return ""

    def expand(self, str1):
        try:
            return self._expand_cache[str1]
        except KeyError:
            self._all = False
            seq = self._sequence(str1)
            res = _clean_spaces(self._eval(seq))
            self._expand_cache[str1] = res
            return res

    def expand_all(self, str1):
        self._all = True
        seq = self._sequence(str1)
        return _clean_spaces(self._eval(seq))

    def _eval(self, seq):
        acc = []
        self._eval_acc(seq, acc)
        return ''.join(acc)

    def _eval_acc(self, seq, acc):
        acc.append(seq[0])
        i = 1
        while i < len(seq):
            name = seq[i]
            if name in _special_names and not self._all:
                acc.append("%" + name)
            else:
                seq2 = self.env.get(name)
                if seq2:
                    self._eval_acc(seq2, acc)
            acc.append(seq[i+1])
            i += 2

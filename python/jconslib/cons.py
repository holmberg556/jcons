#----------------------------------------------------------------------
# jcons/cons.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""with Cons class containing common build settings"""

import copy
from os.path import splitext
from collections import defaultdict

from .buildcache import BuildCache
from .cmd import Cmd
from .env import VarEnv
from .filesystem import Dir
from . import Global

#----------------------------------------------------------------------

def _to_array(arg):
    if isinstance(arg, list):
        return arg
    else:
        return [arg]

def _flatten_with_cons(arr, cons):
    curr_cons = cons
    for x in arr:
        if isinstance(x, (list, tuple)):
            for item in _flatten_with_cons(x, curr_cons):
                yield item
        elif isinstance(x, Cons):
            curr_cons = x
        else:
            expanded_src = curr_cons._expand(x)
            yield (expanded_src, curr_cons)

def _file_ext(path):
    return splitext(path)[1]

def _file_remove_ext(path):
    return splitext(path)[0]

#----------------------------------------------------------------------

class CppPathMap:
    __slots__ = ("_cpp_path", "_as_str", "_file_to_rfile", "cached_md5",
                 "_bydir",
                 "_cpp_path_uniq")

    s_arr_to_obj = {}


    def __init__(self, cpp_path):
        self._cpp_path = cpp_path
        self._as_str = ':'.join(x.path for x in self._cpp_path)
        self._file_to_rfile = {}
        self.cached_md5 = {}
        self._bydir = defaultdict(dict)

        seen = set()
        self._cpp_path_uniq = []
        for x in self._cpp_path:
            if x not in seen:
                self._cpp_path_uniq.append(x)
                seen.add(x)

    #-----------------------------------

    @classmethod
    def find(cls, cpp_path):
        key = tuple(cpp_path)
        try:
            return cls.s_arr_to_obj[key]
        except KeyError:
            obj = CppPathMap(key)
            cls.s_arr_to_obj[key] = obj
            return obj

    #-----------------------------------

    def find_node_includes(self, node):
        d = node.parent
        bydir = self._bydir[d]
        incs = []
        for include in node.db_includes():
            inc = bydir.get(include)
            if inc is None:
                quotes, fname = include
                if quotes:
                    # handle #include "..."
                    inc = d.lookup_or_fs_file(fname)
                if inc is None:
                    # handle #include <...>
                    inc = self._file_to_rfile.get(fname)
                    if inc is None:
                        for d2 in self._cpp_path_uniq:
                            inc = d2.lookup_or_fs_file(fname)
                            if inc is not None: break
                        else:
                            inc = False
                        self._file_to_rfile[fname] = inc
                bydir[include] = inc
            if inc and inc != node:
                if Global.logging:
                    print("INCFOUND: %s - %s" % (node.path, inc.path))
                incs.append(inc)
            else:
                # didn't find include file, so we ignore it
                pass
        return incs

#----------------------------------------------------------------------

class LibPathMap:

    __slots__ = ("_libpath_dirs", "_libs", "_fs_libs")

    def __init__(self, libpath_dirs, libs):
        self._libpath_dirs = libpath_dirs
        self._libs = libs
        self._fs_libs = None

    #-----------------------------------


    @classmethod
    def find(cls, libpath_dirs, libs):
        return LibPathMap(libpath_dirs, libs)

    #-----------------------------------

    def fs_libs(self):
        if self._fs_libs is None:
            self._fs_libs = []
            for libname in self._libs:
                for d in self._libpath_dirs:
                    f = d.lookup_or_fs_file(libname)
                    if f:
                        self._fs_libs.append(f)
                        break
        return self._fs_libs

#----------------------------------------------------------------------

def _apply_defaults(p):
    p.set("AR",           "ar")
    p.set("AR_CMD",       "%AR %AR_FLAGS %OUTPUT %INPUT")
    p.set("AR_FLAGS",     "rc")
    p.set("CC",           "gcc")
    p.set("CC_CMD",
          "%CC %_CPP_INC_OPTS %CFLAGS %CCFLAGS -c %INPUT -o %OUTPUT")
    p.set("CC_LINK",
          "%CC -o %OUTPUT %LINKFLAGS %INPUT %_LIBPATH_OPTS %_LIB_OPTS")
    p.set("CFLAGS",       "")
    p.set("CCFLAGS",      "")
    p.set("CXX",          "g++")
    p.set("CXXFLAGS",     "%CFLAGS")
    p.set("CXX_CMD",
          "%CXX %_CPP_INC_OPTS %CXXFLAGS %CCFLAGS -c %INPUT -o %OUTPUT")
    p.set("CXX_LINK",
          "%CXX -o %OUTPUT %LINKFLAGS %INPUT %_LIBPATH_OPTS %_LIB_OPTS")
    p.set("EXE_EXT",      "")
    p.set("LIB_EXT",      ".a")
    p.set("LINKFLAGS",    "")
    p.set("OBJ_EXT",      ".o")


class Cons:
    __slots__ = ("_build_cache", "_obj_ext", "_exe_ext", "_lib_ext",
                 "_cpp_path", "_cpp_path_arg",
                 "_libs", "_libs_arg", "_libs_arg",
                 "_libpath", "_libpath_dirs", "_libpath_map",
                 "env"
                )

    conscript_dir = None
    conscript_dir_stack = []

    @classmethod
    def set_conscript_dir(cls, dir1):
        Cons.conscript_dir = Dir.curr_dir.find_dir(dir1)

    @classmethod
    def push_conscript_dir(cls, path):
        Cons.conscript_dir_stack.append(Cons.conscript_dir)
        Cons.conscript_dir = Cons.conscript_dir.find_dir(path)

    @classmethod
    def pop_conscript_dir(cls):
        Cons.conscript_dir = Cons.conscript_dir_stack.pop()


    def __init__(self, **kwargs):
        self._build_cache = None

        self._obj_ext = None
        self._exe_ext = None
        self._lib_ext = None

        self._cpp_path = None
        self._cpp_path_arg = []

        self._libs = None
        self._libs_arg = None

        self._libpath = []
        self._libpath_dirs = None
        self._libpath_map = None

        self.env = VarEnv()
        _apply_defaults(self.env)

        self._apply_kwargs(**kwargs)

    def _apply_kwargs(self, **kwargs):
        for k, v in kwargs.items():
            if k == "CPPPATH":
                arr = self._path_to_array(v)
                self.set_cpp_path(arr)
            elif k == "LIBS":
                self.set_libs(v)
            elif k == "LIBDIRS":
                arr = _to_array(v)
                self.set_libpath(arr)
            else:
                self.setenv(k, v)

    #-----------------------------------

    def clone(self, **kwargs):
        obj = copy.copy(self)
        obj.env = copy.copy(obj.env)
        obj._apply_kwargs(**kwargs)
        return obj

    #-----------------------------------

    def build_cache(self):
        return self._build_cache or Global.build_cache

    #-----------------------------------

    def cache_dir(self, dir1):
        self._build_cache = BuildCache(Cons.conscript_dir, dir1)

    #-----------------------------------

    def _join_string_ary(self, acc, level, arg):
        if level < 5:
            if isinstance(arg, list):
                for x in arg:
                    self._join_string_ary(acc, level+1, x)
            else:
                acc.append(arg)

    #-----------------------------------

    def setenv(self, key, value):
        if isinstance(value, list):
            acc = []
            self._join_string_ary(acc, 0, value)
            self.env.set(key, ' '.join(acc))
        else:
            # simple string
            self.env.set(key, value)

    def _path_to_array(self, arg):
        if isinstance(arg, list):
            return arg
        else:
            return arg.split(":")

    def _expand(self, str1):
        if str1.find("%") == -1:
            res = str1
        else:
            res = self.env.expand(str1)
        if res[0] == "#":
            prefix, slash, rest = res.partition("/")
            if slash:
                if prefix == "#BUILD_CURR_DIR":
                    build_top = self.env.get("BUILD_TOP")
                    if build_top:
                        res = '/'.join((build_top,
                                        Cons.conscript_dir.path,
                                        rest))
                elif prefix == "#":
                    pass
                else:
                    RuntimeError("illegal #-path", str1)

        return res

    #-----------------------------------

    # TODO: avoid exposing to users
    def cpp_path(self):
        if self._cpp_path is None:
            self._cpp_path = CppPathMap.find(self._cpp_path_arg)
        return self._cpp_path

    #-----------------------------------

    def set_cpp_path(self, arg):
        self._cpp_path_arg = []
        for it in arg:
            d = Cons.conscript_dir.find_dir(self._expand(it))
            if d.tgtdir:
                self._cpp_path_arg.append(d.tgtdir)
            self._cpp_path_arg.append(d)
        self.env_update_cpp_path()

    #-----------------------------------

    def env_update_cpp_path(self):
        self.env.set("_CPP_INC_OPTS",
                     ' '.join("-I " + x.path for x in self._cpp_path_arg))

    #-----------------------------------

    def _calc_libname(self, name):
        return self._maybe_add_ext("lib" + name, self.lib_ext())

    #-----------------------------------

    def set_libs(self, libs):
        self._libs_arg = libs

    #-----------------------------------

    def _calc_libs(self, libs_arr):
        lib_opts = []
        self._libs = []

        # TODO: delay this update, and take care of Windows syntax too
        for it in libs_arr:
            if it[0:2] == "-l":
                # -lFOO
                lib_opts.append(it)
                self._libs.append(self._calc_libname(it[2:]))
            else:
                # FOO
                lib_opts.append("-l" + it)
                self._libs.append(self._calc_libname(it))

        self.env.set("_LIB_OPTS", ' '.join(lib_opts))

    #-----------------------------------

    def libs(self):
        if self._libs is None:
            if isinstance(self._libs_arg, list):
                expanded_libs_arr = [self._expand(it) for it in self._libs_arg]
                self._calc_libs(expanded_libs_arr)
            elif isinstance(self._libs_arg, str):       # TODO: eliminate test?
                expanded_libs_arr = self._expand(self._libs_arg).split()
                self._calc_libs(expanded_libs_arr)
            else:
                self._libs = []
        return self._libs

    #-----------------------------------

    def set_libpath(self, libpath):
        self._libpath = libpath
        self.env_update_libpath_opts()

    #-----------------------------------
    # TODO: delay this update, and take care of Windows syntax too

    def env_update_libpath_opts(self):
        _libpath_opts_str = ' '.join("-L" + x for x in self._libpath)
        self.env.set("_LIBPATH_OPTS", _libpath_opts_str)

    #-----------------------------------

    def libpath_dirs(self):
        if self._libpath_dirs is None:
            self._libpath_dirs = []
            for it in self._libpath:
                d = Cons.conscript_dir.lookup_or_fs_dir(it)
                if d:
                    self._libpath_dirs.append(d)
        return self._libpath_dirs

    #-----------------------------------

    def libpath_map(self):
        if self._libpath_map is None:
            self._libpath_map = LibPathMap.find(self.libpath_dirs(),
                                                self.libs())
        return self._libpath_map

    #-----------------------------------

    def _connect_tgt(self, tgt, cmd):
        existing_cmd = tgt.cmd
        if existing_cmd:
            print("jcons: error: same target twice: ", tgt.path)
            exit(1)
        cmd.tgts.append(tgt)
        tgt.cmd = cmd
        if tgt.extra_deps:
            if cmd.extra_deps:
                # merge sets
                cmd.extra_deps.extend(tgt.extra_deps)
                tgt.extra_deps = None
            else:
                # move set
                cmd.extra_deps = tgt.extra_deps
                tgt.extra_deps = None


    def _connect_src(self, src, cmd):
        cmd.srcs.append(src)


    def exe_depends(self, tgt, src):
        t = Cons.conscript_dir.find_file(self._expand(tgt))
        s = Cons.conscript_dir.find_file(self._expand(src))
        exe_deps = t.get_exe_deps()
        exe_deps.add(s)


    def depends(self, tgt, src):
        t = Cons.conscript_dir.find_file(self._expand(tgt))
        s = Cons.conscript_dir.find_file(self._expand(src))

        if t.cmd:
            extra_deps = t.cmd.get_extra_deps()
            extra_deps.add(s)
        else:
            extra_deps = t.get_extra_deps()
            extra_deps.add(s)

    def install(self, tgtdir, src):
        s = Cons.conscript_dir.find_file(self._expand(src))
        td = Cons.conscript_dir.find_dir(self._expand(tgtdir))
        t = td.find_file(s.name)

        cmd = Cmd(self)
        self._connect_src(s, cmd)
        self._connect_tgt(t, cmd)
        cmd.cmdline = "cp %INPUT %OUTPUT"


    def install_as(self, tgt, src):
        s = Cons.conscript_dir.find_file(self._expand(src))
        t = Cons.conscript_dir.find_file(self._expand(tgt))

        cmd = Cmd(self)
        self._connect_src(s, cmd)
        self._connect_tgt(t, cmd)
        cmd.cmdline = "cp %INPUT %OUTPUT"


    def command(self, tgt_arg, src_arg, command, scanner=None, digest_cmdline=None):
        tgts = _to_array(tgt_arg)
        srcs = _to_array(src_arg)

        cmd = Cmd(self)
        for tgt in tgts:
            t = Cons.conscript_dir.find_file(self._expand(tgt))
            self._connect_tgt(t, cmd)
        for src in srcs:
            s = Cons.conscript_dir.find_file(self._expand(src))
            self._connect_src(s, cmd)

        cmd.cmdline = command
        cmd.uses_cpp = (scanner == "cpp")
        if digest_cmdline is not None:
            cmd._cmdline1 = digest_cmdline


    def _object1_file(self, src):
        obj = _file_remove_ext(src) + self.obj_ext()
        return self._get_mapped_obj(obj)

    def _object1(self, src):
        mapped_obj = self._object1_file(src)
        self.object(mapped_obj, src)
        return mapped_obj

    def objects(self, *srcs):
        objs = []
        for src, cons in _flatten_with_cons(srcs, self):
            expanded_src = cons._expand(src)
            obj = cons._object1(expanded_src)
            objs.append(obj)
        return objs

    def object(self, obj, src):
        t = Cons.conscript_dir.find_file(self._expand(obj))
        s = Cons.conscript_dir.find_file(self._expand(src))
        return self._fs_object(t, s, self.lang_by_ext(src))

    def _fs_object(self, t, s, lang):
        x_cmd = self.compile_symbol_by_lang(lang)

        existing_cmd = t.cmd
        if existing_cmd:
            same = (x_cmd  == existing_cmd.cmdline and
                    self == existing_cmd.cons and
                    existing_cmd.uses_cpp)
            if same:
                # same command again ==> OK, and nothing new to do
                return t.path
            else:
                print ("jcons: error: same target twice, different command:" +
                       "'" + t.path + "'")
                exit(1)

        cmd = Cmd(self)
        self._connect_src(s, cmd)
        self._connect_tgt(t, cmd)
        cmd.uses_cpp = True

        cmd.cmdline = x_cmd
        return t.path


    def static_library(self, lib, *srcs):
        items = []
        d = Cons.conscript_dir
        for src, cons in _flatten_with_cons(srcs, self):
            if self.is_obj_ext(src):
                items.append( (None, d.find_file(src), None) )
            else:
                obj = cons._object1_file(src)
                items.append( (cons, d.find_file(obj), src) )

        lib_cmd = Cmd(self)
        for _, o, _ in items:
            self._connect_src(o, lib_cmd)

        for cons, o, src in items:
            if src:
                cons._fs_object(o, d.find_file(src), cons.lang_by_ext(src))

        lib_a = self._maybe_add_ext(self._expand(lib), self.lib_ext())
        mapped_lib = self._get_mapped_lib(lib_a)

        t = Cons.conscript_dir.find_file(mapped_lib)
        self._connect_tgt(t, lib_cmd)

        lib_cmd.cmdline = "%AR_CMD"


    def program(self, prog, *srcs):
        link_lang = None
        objs = []
        for src, cons in _flatten_with_cons(srcs, self):
            if self.is_obj_ext(src):
                objs.append(src)
            elif self.is_lib_ext(src):
                objs.append(self._get_mapped_lib(src))
            else:
                obj = cons._object1(src)
                objs.append(obj)
                link_lang = self._update_lang(link_lang,
                                              self.lang_by_ext(src))
        if link_lang is None:
            link_lang = "C++" # best guess

        link_cmd = Cmd(self)
        for obj in objs:
            o = Cons.conscript_dir.find_file(obj)
            self._connect_src(o, link_cmd)

        prog_exe = self._maybe_add_ext(self._expand(prog), self.exe_ext())
        mapped_prog = self._get_mapped_exe(prog_exe)

        t = Cons.conscript_dir.find_file(mapped_prog)
        self._connect_tgt(t, link_cmd)

        x_cmd = self.link_symbol_by_lang(link_lang)
        link_cmd.cmdline = x_cmd
        link_cmd.uses_libpath = True


    #-----------------------------------

    def _maybe_add_ext(self, path, ext):
        if _file_ext(path) == "":
            return path + ext
        else:
            return path

    def is_obj_ext(self, file):
        ext = _file_ext(file)
        return ext == self.obj_ext()


    def is_exe_ext(self, file):
        ext = _file_ext(file)
        return ext == self.exe_ext()

    def is_lib_ext(self, file):
        ext = _file_ext(file)
        return ext == self.lib_ext()

    #-----------------------------------

    def lang_by_ext(self, file):
        ext = _file_ext(file)
        if ext == ".c":
            return "C"
        elif ext == ".cpp":
            return "C++"
        elif ext == ".cc":
            return "C++"
        elif ext == ".cxx":
            return "C++"
        else:
            print("Error: unknown extension for: '" + file + "'")
            exit(1)

    def compile_symbol_by_lang(self, lang):
        if lang == "C":
            return "%CC_CMD"
        elif lang == "C++":
            return "%CXX_CMD"
        else:
            print("Error: unknown language: " + lang)
            exit(1)

    def link_symbol_by_lang(self, lang):
        if lang == "C":
            return "%CC_LINK"
        elif lang == "C++":
            return "%CXX_LINK"
        else:
            print("Error: unknown language: " + lang)
            exit(1)

    def _update_lang(self, link_lang, lang):
        if link_lang is None:
            return lang
        elif link_lang == "C" and lang == "C++":
            return lang
        else:
            # already set
            return link_lang

    #-----------------------------------

    def obj_ext(self):
        if not self._obj_ext:
            #m_env.get("OBJ_EXT", m_obj_ext, true)
            self._obj_ext = ".o"
        return self._obj_ext

    def exe_ext(self):
        if not self._exe_ext:
            #m_env.get("EXE_EXT", m_exe_ext, true)
            self._exe_ext = ""
        return self._exe_ext

    def lib_ext(self):
        if not self._lib_ext:
            #m_env.get("LIB_EXT", m_lib_ext, true)
            self._lib_ext = ".a"
        return self._lib_ext

    #-----------------------------------
    # Replace ".." with "__" in paths.

    def _replace_dot_dot(self, path):
        parts = path.split("/")
        return '/'.join( "__" if x == ".." else x for x in parts)

    def _prepend_build_top(self, path):
        mapped_path = path

        build_top = self.env.get("BUILD_TOP")
        if build_top:
            if mapped_path.find("#/") == 0:
                f = Cons.conscript_dir.find_file(mapped_path)
                mapped_path = '/'.join((build_top, f.path))
            else:
                mapped_path = self._replace_dot_dot(mapped_path)
                mapped_path = '/'.join((build_top,
                                        Cons.conscript_dir.path,
                                        mapped_path))

        build_subdir = self.env.get("BUILD_SUBDIR")
        if build_subdir:
            slash_i = mapped_path.rfind('/')
            if slash_i == -1:
                mapped_path = build_subdir + "/" + mapped_path
            else:
                mapped_path = '/'.join((mapped_path[:slash_i], build_subdir,
                                        mapped_path[slash_i+1:]))

        build_suffix = self.env.get("BUILD_SUFFIX")
        if build_suffix:
            slash_i = mapped_path.rfind('/')
            dot_i   = mapped_path.rfind('.')
            if dot_i == -1:
                mapped_path += "-" + build_suffix
            elif slash_i == -1 or dot_i > slash_i:
                mapped_path = ''.join((mapped_path[:dot_i],
                                       "-",
                                       build_suffix,
                                       mapped_path[dot_i:]))
            else:
                mapped_path += "-" + build_suffix

        return mapped_path

    #-----------------------------------

    def _get_mapped_exe(self, path):
        exe_dir = self.env.get("EXE_DIR")
        if exe_dir:
            return exe_dir + "/" + path
        return self._prepend_build_top(path)

    def _get_mapped_lib(self, path):
        lib_dir = self.env.get("LIB_DIR")
        if lib_dir:
            return lib_dir + "/" + path
        return self._prepend_build_top(path)

    def _get_mapped_obj(self, path):
        return self._prepend_build_top(path)

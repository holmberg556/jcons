#----------------------------------------------------------------------
# jcons/filesystem.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""filesystem tree module with Dir and File classes"""

from collections import namedtuple, defaultdict
from hashlib import md5 as hashlib_md5
import functools
import io
import os
import os.path
import re
import sys
import time

# from fast_stat import classify_file

import pickle

from os      import stat    as os_stat
from os.path import join    as path_join, getmtime
from stat    import S_ISDIR

from . import Global


_VERSION_STR = '_'.join(str(x) for x in sys.version_info)
_DATABASE_DIR = ".jcons/python." + _VERSION_STR

s_case_sensitive = None # set in Dir.init()

#======================================================================

class Entries:

    def __init__(self):
        self._value = dict()
        self._case = dict()

    def __setitem__(self, k ,v):
        low_k = k.lower()
        actual_k = self._case.get(low_k)
        if actual_k:
            self._value[actual_k] = v
        else:
            self._case[low_k] = k
            self._value[k] = v

    def get(self, k):
        low_k = k.lower()
        actual_k = self._case.get(low_k)
        if actual_k:
            return self._value[actual_k]
        else:
            return None

    def items(self):
        return self._value.items()

    @staticmethod
    def create():
        if s_case_sensitive:
            return dict()
        else:
            return Entries()



#======================================================================

class NotDirError(Exception):
    pass

class NotFileError(Exception):
    pass

Include = namedtuple('Include', ["quotes", "file"])

#======================================================================
# Information saved for each file

class ConsignEntry:

    __slots__ = ("mtime", "content_sig", "dep_sig", "includes")

    def __init__(self):
        self.mtime = 0          # the value returned by stat(2)
        self.content_sig = None # a cryptographic checksum of the file content
        self.dep_sig = None     # the dependency checksum, used to decide
                                # if a file need to be updated
        self.includes = None

#======================================================================
# Base class for Dir / File

class Entry:

    __slots__ = ("name", "_path", "parent", "dirty")

    def __init__(self, name, parent=None):
        if parent:
            # Constructor for normal entry
            self.name = name
            self._path = None
            self.parent = parent
            self.dirty = False
        else:
            self.name = name
            self._path = name
            self.parent = None
            self.dirty = False

    #------------------------------
    # Set dirty flag and propagate upwards in tree.

    def set_dirty_upwards(self):
        d = self
        while d and not d.dirty:
            d.dirty = True
            d = d.parent

    #------------------------------
    # Return the path of a file/directory.
    # The path may be absolute/relative depending on where in the filesystem
    # the file/directory is located.

    @property
    def path(self):
        if not self._path:
            parent_path = self.parent.path
            if parent_path == ".":
                self._path = self.name
            elif parent_path[-1] == '/':
                self._path = parent_path + self.name
            else:
                self._path = parent_path + '/' + self.name
        return self._path

#======================================================================

class Dir(Entry):

    curr_dir = None
    s_dir_arr = []
    g_tops = {}

    __slots__ = ("_consign_dirty", "tgtdir", "_cache_dir_p",
                 "_local_dir_p", "_entries", "_fs_entries")

    def __init__(self, name, parent=None):
        Entry.__init__(self, name, parent)
        self._consign_dirty = False
        self.tgtdir = None

        self._cache_dir_p = (parent != None and parent._cache_dir_p)
        self._local_dir_p = (parent != None and parent._local_dir_p)

        self._entries = Entries.create()
        self._entries["."] = self
        self._entries[".."] = (parent or self)
        self._fs_entries = None


    def write_consign(self, curr_mtime, consign_file):
        for entry in self.consign().values():
            if entry.mtime == curr_mtime:
                entry.mtime = None
        with open(consign_file, "wb") as f:
            pickle.dump(self.consign(), f, pickle.HIGHEST_PROTOCOL)
        self._consign_dirty = False

    def read_consign(self, consign_file):
        if os.path.isfile(consign_file):
            with open(consign_file, "rb") as f:
                return pickle.load(f)
        else:
            return defaultdict(ConsignEntry)

    @functools.lru_cache(None)
    def consign(self):
        res = self.read_consign(self._consign_path())
        self._consign_dirty = False
        Dir.s_dir_arr.append(self)
        return res

    # Initialize the class.
    # The method can be called several times, and will then "reset"
    # the state. This can be useful for testing the code.

    @classmethod
    def init(cls):
        global s_case_sensitive
        if not os.path.isdir(_DATABASE_DIR):
            os.makedirs(_DATABASE_DIR)

        if Global.case_sensitive:
            s_case_sensitive = True
        elif Global.case_insensitive:
            s_case_sensitive = False
        else:
            s_case_sensitive = cls.case_sensitive_filesystem(_DATABASE_DIR);
        if Global.verbose:
            print(f'### s_case_sensitive = {s_case_sensitive}')

        cls.curr_dir = None
        cls.g_tops.clear()

        cls.s_dir_arr = []

        top = Dir("/")
        cls.g_tops["/"] = top
        cls.curr_dir = top.find_dir(os.getcwd()) # argument absolute
        cls.curr_dir._path = "."
        cls.curr_dir._local_dir_p = True

    @classmethod
    def case_sensitive_filesystem(cls, fname):
        if not os.path.isdir(fname):
            print(f"ERROR: called with nonexisting file: {fname}")
            exit(1)
        try:
            return not os.path.samefile(fname.lower(), fname.upper())
        except FileNotFoundError:
            return True

    #----------------------------------------------------------------------
    # Flush information to .consign files.
    # Should be called before the process is terminated.

    @classmethod
    def terminate(cls):
        curr_mtime = int(time.time())
        for it in cls.s_dir_arr:
            if it._consign_dirty:
                it.write_consign(curr_mtime, it._consign_path())

    #----------------------------------------------------------------------

    def init_out_of_source(self, relpath, tgtdir):
        self._path = relpath
        self.tgtdir = tgtdir

    #----------------------------------------------------------------------

    @classmethod
    def top_print_tree(cls):
        for it in cls.g_tops:
            cls.g_tops[it].print_tree()



    def _consign_path(self):
        if self._cache_dir_p:
            return self.path + "/.consign"

        name = self.path
        #
        # Transform a path with directory delimiters ('/') to a filename:
        #
        #   - use '=' as quote character
        #   - use '+' instead of '/'
        #
        name = name.replace('=', '==')
        name = name.replace('+', '=+')
        name = name.replace('/', '+')
        return _DATABASE_DIR + "/__" + name

    #----------------------------------------------------------------------

    def print_tree(self, level):
        print("    " * level, end=" ")
        print("'" + self.name + "'")
        for it in sorted(self._entries.values()):
            it.print_tree(level + 1)

    #----------------------------------------------------------------------
    # find/create a "file" in the programs directory tree
    # interpret "#" at start of path
    # fast if successive calls are in same directory

    def find_file(self, path):
        dirpath, slash, name = path.rpartition("/")
        if slash:
            if dirpath == '':
                d = Dir.g_tops["/"]
            else:
                d = self.find_dir(dirpath)
        else:
            d = self

        f = d._entries.get(name)
        if f:
            if not isinstance(f, File):
                raise NotFileError()
        else:
            f = d._entries[name] = File(name, d)
        return f

    #----------------------------------------------------------------------
    # look for an "existing" file or directory in the programs directory tree
    # no interpretation of "#"
    # path is expected to be well-formed

    def lookup_entry(self, path):
        parts = path.split("/")
        e = self
        for part in parts:
            if part == "":
                e = Dir.g_tops["/"]
            elif isinstance(e, File):
                raise NotDirError()
            else:
                e = e._entries.get(part)
                if e is None:
                    return None
        return e

    #----------------------------------------------------------------------

    def find_dir(self, path):
        d = self
        for i, name in enumerate(path.split("/")):
            if name == "#":
                d = Dir.curr_dir
            elif not name:
                if i == 0:
                    d = Dir.g_tops["/"]
                else:
                    pass
            else:
                d2 = d._entries.get(name)
                if d2:
                    if not isinstance(d2, Dir):
                        raise NotDirError()
                else:
                    d2 = d._entries[name] = Dir(name, d)
                d = d2
        return d

    #----------------------------------------------------------------------

    def lookup_or_fs_dir(self, path):
        first, slash, rest = path.partition("/")
        if not first:
            d = Dir.g_tops["/"]
            first, slash, rest = rest.partition("/")
        else:
            d = self

        while slash:
            e = d._entries.get(first)
            if e is None:
                try:
                    st = os_stat(path_join(d.path, first))
                except OSError:
                    d._entries[first] = -111
                    return None
                else:
                    if S_ISDIR(st.st_mode):
                        d2 = Dir(first, d)
                        d._entries[first] = d2
                        d = d2
                    else:
                        f = File(first, d)
                        d._entries[first] = f
                        f.set_mtime(st.st_mtime)
                        raise NotDirError()
            elif e == -111:
                return None
            else:
                if not isinstance(e, Dir):
                    raise NotDirError()
                d = e
            first, slash, rest = rest.partition("/")

        e = d._entries.get(first)
        if e is None:
            try:
                st = os_stat(path_join(d.path, first))
            except OSError:
                d._entries[first] = -111
                return None
            else:
                if S_ISDIR(st.st_mode):
                    d2 = Dir(first, d)
                    d._entries[first] = d2
                    return d2
                else:
                    f = File(first, d)
                    d._entries[first] = f
                    f.set_mtime(st.st_mtime)
                    raise NotDirError()
        elif e == -111:
            return None
        else:
            if not isinstance(e, Dir):
                raise NotDirError()
            return e

    #----------------------------------------------------------------------

    def lookup_or_fs_file(self, path):
        if Global.logging:
            print("LOOKUP_OR_FS_FILE: " + path)
        first, slash, rest = path.partition("/")
        if not first:
            d = Dir.g_tops["/"]
            first, slash, rest = rest.partition("/")
        else:
            d = self

        while slash:
            e = d._entries.get(first)
            if e is None:
                try:
                    st = os_stat(path_join(d.path, first))
                except OSError:
                    d._entries[first] = -111
                    return None
                else:
                    if S_ISDIR(st.st_mode):
                        d2 = Dir(first, d)
                        d._entries[first] = d2
                        d = d2
                    else:
                        f = File(first, d)
                        d._entries[first] = f
                        f.set_mtime(st.st_mtime)
                        raise NotDirError()
            elif e == -111:
                return None
            else:
                if not isinstance(e, Dir):
                    raise NotDirError()
                d = e
            first, slash, rest = rest.partition("/")

        e = d._entries.get(first)
        if e is None:
            if d._fs_entries is None:
                try:
                    d._fs_entries = set(os.listdir(d.path))
                except OSError:
                    d._fs_entries = set()
            if first not in d._fs_entries:
                d._entries[first] = -111
                return None
            else:
                try:
                    st = os_stat(path_join(d.path, first))
                except OSError:
                    d._entries[first] = -111
                    return None
                else:
                    if S_ISDIR(st.st_mode):
                        d2 = Dir(first, d)
                        d._entries[first] = d2
                        raise NotFileError()
                    else:
                        f = File(first, d)
                        d._entries[first] = f
                        f.set_mtime(st.st_mtime)
                        return f
        elif e == -111:
            return None
        else:
            if not isinstance(e, File):
                raise NotFileError()
            return e

    #----------------------------------------------------------------------

    def mkdir_p(self):
        path = self.path
        if not os.path.isdir(path):
            try:
                os.makedirs(path)
            except OSError:
                return False
        return True

  #----------------------------------------------------------------------

    def append_files_under(self, tgts):
        for name, e in sorted(self._entries.items()):
            if name in (".", ".."): continue
            if isinstance(e, Dir):
                e.append_files_under(tgts)
            elif isinstance(e, File):
                if e.cmd:
                    tgts.append(e)
            else:
                print("jcons: error: internal error")
                exit(1)

#======================================================================

class File(Entry):

    __slots__ = ("_mtime", "_file_exist",
                 "_tgtfile", "extra_deps", "exe_deps", "top_target",
                 "cmd", "waiting_sem", "build_ok", "new_dep_sig",
                 "_consign_entry",
                 "color")

    exe_deps_tgts = []

    def __init__(self, name, parent):
        Entry.__init__(self, name, parent)
        self._mtime = 0
        self._file_exist = None
        self._tgtfile = None
        self.extra_deps = None
        self.exe_deps = None
        self.top_target = False
        self.cmd = None
        self.waiting_sem = None
        self.build_ok = None
        self.new_dep_sig = None
        self.color = None
        self._consign_entry = None

    def __repr__(self):
        return "File[" + self.path + "]"

    def _consign_get_entry(self):
        self._consign_entry = self.parent.consign()[self.name]
        return self._consign_entry

    def _consign_remove_entry(self):
        self.parent.consign().pop(self.name, None)

    def _consign_set_dirty(self):
        self.parent._consign_dirty = True

    def set_mtime(self, mtime):
        self._mtime = mtime
        self._file_exist = True

    def get_extra_deps(self):
        if self.extra_deps is None:
            self.extra_deps = set()
        return self.extra_deps

    def get_exe_deps(self):
        File.exe_deps_tgts.append(self)
        if self.exe_deps is None:
            self.exe_deps = set()
        return self.exe_deps

    #--------------------------------------------------
    #--------------------------------------------------
    # Methods for managing the .consign information ...

    #----------------------------------------------------------------------
    # Return the "raw" content_sig value.
    # It will always exist when this function is called.

    def db_content_sig(self):
        consign_entry = self._consign_entry or self._consign_get_entry()
        return consign_entry.content_sig

    #----------------------------------------------------------------------
    # Return dep_sig if it is valid.
    # Called when we consider rebuilding an existing file.

    def db_dep_sig(self):
        consign_entry = self._consign_entry or self._consign_get_entry()
        if consign_entry.dep_sig == "INVALID":
            return "MD5:INVALID"
        mtime = self.file_mtime()
        if mtime == consign_entry.mtime and Global.trust_mtime:
            return consign_entry.dep_sig

        content_sig = self.file_md5()
        if content_sig == consign_entry.content_sig:
            consign_entry.mtime = mtime
            self._consign_set_dirty()
            return consign_entry.dep_sig

        consign_entry.includes = None
        self._consign_set_dirty()
        return "MD5:UNDEFINED"

    #----------------------------------------------------------------------
    # The file exists and is a "source".
    # Make sure it has an accurate .consign entry.

    def st_source(self):
        consign_entry = self._consign_entry or self._consign_get_entry()
        mtime = self.file_mtime()
        if not (mtime == consign_entry.mtime and Global.trust_mtime):
            consign_entry.mtime = mtime
            consign_entry.content_sig = self.file_md5()
            consign_entry.includes = None
            self._consign_set_dirty()
        if consign_entry.dep_sig != consign_entry.content_sig:
            consign_entry.dep_sig = consign_entry.content_sig
            self._consign_set_dirty()
        self.build_ok = True
        #print(("st", "source", self.path, self.db_content_sig()))

    #----------------------------------------------------------------------
    # The file is invalid: the file does not exist, or a build step failed.
    # Forget about the file.

    def st_invalid_error(self):
        self.set_dirty_upwards()
        self._consign_remove_entry()
        self._consign_set_dirty()
        self.build_ok = False
        #print(("st", "invalid_error", self.path))

    #----------------------------------------------------------------------
    # Propagate the fact that there has been an error "upstream".
    # No change of .consign info.

    def st_propagate_error(self):
        self.set_dirty_upwards()
        self.build_ok = False
        #print(("st", "propagate_error", self.path))

    #----------------------------------------------------------------------
    # The file is already up-to-date.
    # No change of .consign info.

    def st_propagate_ok(self):
        self.build_ok = True
        #print(("st", "propagate_ok", self.path))

    #----------------------------------------------------------------------
    # The file has been updated by running a command.
    # The new 'dep_sig' is stored, and 'mtime' and 'content_sig' are updated.

    def st_updated(self, dep_sig):
        self.set_dirty_upwards()
        consign_entry = self._consign_entry or self._consign_get_entry()
        self._consign_set_dirty()
        consign_entry.mtime       = self.file_mtime_FORCED()
        consign_entry.content_sig = self.file_md5()
        consign_entry.dep_sig     = dep_sig

        consign_entry.includes = None

        self.build_ok = True
        #print(("st", "updated", self.path, consign_entry))

    #----------------------------------------------------------------------

    def st_fake_updated(self):
        consign_entry = self._consign_entry or self._consign_get_entry()
        consign_entry.content_sig = b''
        self.build_ok = True

    #----------------------------------------------------------------------

    def file_exist(self):
        if self._file_exist is None:
            try:
                self._mtime = getmtime(self.path)
                self._file_exist = True
            except OSError:
                self._file_exist = False

        return self._file_exist

    #----------------------------------------------------------------------
    # like 'file_exist', but with forced call

    def file_exist_FORCED(self):
        try:
            self._mtime = getmtime(self.path)
            self._file_exist = True
        except OSError:
            self._file_exist = False

        return self._file_exist

    #----------------------------------------------------------------------

    static_buff = bytearray(65536)

    def file_md5(self):
        buff = File.static_buff
        buff_size = len(buff)

        m = hashlib_md5()
        with io.FileIO(self.path, "rb") as f:
            while True:
                n = f.readinto(buff)
                if not n: break
                if n == buff_size:
                    m.update(buff)
                else:
                    m.update(memoryview(buff)[0:n])

        return m.digest()

    #----------------------------------------------------------------------

    def file_mtime(self):
        if not self._mtime:
            self._mtime = getmtime(self.path)
        return self._mtime

    #----------------------------------------------------------------------
    # like 'file_mtime', but with forced call

    def file_mtime_FORCED(self):
        self._mtime = getmtime(self.path)
        return self._mtime

    #----------------------------------------------------------------------
    # Called when file exists and has current info in .consign.
    # We may either get value from .consign or compute it now.

    def db_includes(self):
        consign_entry = self._consign_entry or self._consign_get_entry()
        if consign_entry.includes is None:
            consign_entry.includes = self.calc_includes()
            self._consign_set_dirty()
        return consign_entry.includes

    #----------------------------------------------------------------------

    def calc_includes(self):
        includes = []
        include_re = re.compile(r'\s*#\s*include\s+([<"])(.*?)[>"]')
        end_re = re.compile(r'#end\b')
        with open(self.path, "r", errors="replace") as f:
            for line in f:
                if line.find("#") == -1: continue
                m = include_re.match(line)
                if m:
                    quotes = m.group(1) == '"'
                    filename = m.group(2)
                    includes.append(Include(quotes, filename))
                elif end_re.match(line):
                    break

        return includes

    #----------------------------------------------------------------------

    def print_tree(self, level):
        print("    " * level, end=" ")
        print('"' + self.name + '"')

    #----------------------------------------------------------------------

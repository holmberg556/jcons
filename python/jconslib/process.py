#----------------------------------------------------------------------
# jcons/process.py
#----------------------------------------------------------------------
# Copyright 2012-2017 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "jcons".
#
# "jcons" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "jcons" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "jcons".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

"""handle process creation & waiting"""

import os
import re
import shlex
import sys
import subprocess

_fake_processes = []
_fake_pid = 1

_p_by_pid = dict()

def process_start(cmdline, noop_command):
    args = shlex.split(cmdline)
    meta = re.search(r'[&<>;]', cmdline)
    sys.stdout.flush()
    sys.stderr.flush()

    if noop_command:
        global _fake_pid
        _fake_pid += 1
        pid = str(_fake_pid)
        _fake_processes.append(pid)
        return pid

    if meta:
        cmd = ('sh', '-c', cmdline)
    else:
        cmd = args
    p = subprocess.Popen(cmd)
    _p_by_pid[p.pid] = p
    return p.pid

def process_wait(blocking=True):
    if _fake_processes:
        return _fake_processes.pop(), 0
    options = 0 if blocking else os.WNOHANG
    pid, status = os.waitpid(0, options)
    del _p_by_pid[pid]
    return pid, status
